<?php

class ReportedsuggestedController extends Zend_Controller_Action {

    public $user = null;

    public function init() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userInfo) {
            $this->_redirect('/index');
        } else {
            $this->view->email = $userInfo->email;
            if ($userInfo->id_type == 1) {
                $this->view->id_type = "Admin";
            } elseif ($userInfo->id_type == 2) {
                $this->view->id_type = "Usuario";
            } elseif ($userInfo->id_type == 3) {
                $this->view->id_type = "Solo lectura";
            }
            $this->user = $userInfo->user;
        }
        $this->config = Zend_Registry::get('config');
        $this->view->imgsUrl = $this->config->mediaConverterFrontEnd . 'media/contdin/fixo/questions/';
    }

    public function dropAction() {
        $id = $this->getRequest()->getParam('question_id');
        if ($id) {
            $dbh = new Application_Model_DbTable_Suggested();
            $dbh->deleteQ($id);
            Zend_Registry::get('log')->info("[$this->user] DELETE_QUESTION:$id");

            die('OK');
        }
    }

    public function indexAction() {
        $search = $this->getRequest()->getParam('search') ? $this->getRequest()->getParam('search') : null;

        $this->view->search_txt = !empty($search) ? $search : "";
        $selected_lang = $this->getRequest()->getParam('lang') ? $this->getRequest()->getParam('lang') : null;
        if (!$selected_lang) {
            return null;
        }
        $selected_country = $this->getRequest()->getParam('country') ? $this->getRequest()->getParam('country') : null;
        $selected_level = $this->getRequest()->getParam('level') ? $this->getRequest()->getParam('level') : null;
        $selected_category = $this->getRequest()->getParam('category') ? $this->getRequest()->getParam('category') : null;
        $cant = $this->getRequest()->getParam('cant') ? $this->getRequest()->getParam('cant') : CANT_DEFAULT;
        $page = $this->getRequest()->getParam('page') ? $this->getRequest()->getParam('page') : PAGE_DEFAULT;


        $this->view->selected_lang = $selected_lang;
        $this->view->selected_country = $selected_country;
        $this->view->selected_level = $selected_level;
        $this->view->selected_category = $selected_category;
        $dbh = new Application_Model_DbTable_Suggested();
        $count = $dbh->getall('count', $page, $cant, $selected_lang, $selected_country, $selected_category, $selected_level, $search);
        $rows = $dbh->getall('query', $page, $cant, $selected_lang, $selected_country, $selected_category, $selected_level, $search);

        $this->view->questions = $rows;

#paginado :
        $cmb_paginado = null;
        if ($count) {
            $paginas = ($count / $cant) + 1;
            $cmb_paginado = "<select id='filter_page'>";
            for ($i = 1; $i < $paginas; $i++) {
                if ($page == $i) {
                    $cmb_paginado.= "<option selected='selected' value='$i'>$i</option>";
                } else {
                    $cmb_paginado.= "<option value='$i'>$i</option>";
                }
            }
            $cmb_paginado.= "</select>";
        }
        $this->view->cmb_paginado = $cmb_paginado;
    }

    public function editAction() {
        $qlid = $this->getRequest()->getParam('qlid');
        $dbh = new Application_Model_DbTable_Suggested();
        $rows = $dbh->getqlid($qlid);
        #print_r($rows);exit;
        #  shuffle($return['questions']);
        #$this->view->data = $return['questions'];
        $this->view->data = $rows;
    }

    public function addAction() {
        
    }

    public function updateAction() {
        $qlid = $this->getRequest()->getParam('qlid');
        $question = $this->getRequest()->getParam('question');
        $level = $this->getRequest()->getParam('level');
        $cat = $this->getRequest()->getParam('category_id');
        $country_id = $this->getRequest()->getParam('country_id');
        $answer0 = $this->getRequest()->getParam('answer0');
        $answer1 = $this->getRequest()->getParam('answer1');
        $answer2 = $this->getRequest()->getParam('answer2');
        $answer3 = $this->getRequest()->getParam('answer3');
        $dbh = new Application_Model_DbTable_Suggested();
        $rows = $dbh->getqlid($qlid);
        $bad = array(chr(130), chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(173), chr(160));
        $good = array(chr(44), chr(39), chr(39), chr(34), chr(34), chr(45), chr(45), chr(45), chr(32));
        $question = str_replace("\n", " ", $question);
        $question = str_replace("  ", " ", $question);
        #$question =  str_replace("\"","\", $question); 
        $this->view->data = $rows;
        try {
            $dbh->updateQ($qlid, $question, $level, $country_id, $cat, $answer0, $answer1, $answer2, $answer3);
            $this->_redirect('/reportedsuggested/index/page/1/lang/*/country/*/category/*/level/*/search/' . urlencode($question));
        } catch (\Exception $e) {
            $this->_helper->viewRenderer('edit');
            $this->view->error = 'Error cambiando la pregunta: ' . $e->getMessage();
        }
    }

    public function saveAction() {
        $user_id = $this->getRequest()->getParam('user_id');
        #$pp = $this->getRequest()->getParams();
        #print_r($pp);exit;
        #print_r($user_id);exit;
        $suggested_id = $this->getRequest()->getParam('suggested_id');
        $lang = $this->getRequest()->getParam('lang');
        $level = $this->getRequest()->getParam('level');
        $cat = $this->getRequest()->getParam('category_id');
        $country_id = $this->getRequest()->getParam('country_id');
        $question = $this->getRequest()->getParam('question');
        $answer0 = $this->getRequest()->getParam('answer0');
        $answer1 = $this->getRequest()->getParam('answer1');
        $answer2 = $this->getRequest()->getParam('answer2');
        $answer3 = $this->getRequest()->getParam('answer3');
        $dbh = new Application_Model_DbTable_Questions();
        try {
            $dbh->getDefaultAdapter()->beginTransaction();
            $question_id = $dbh->addQ(0, $level, $country_id, $lang, $cat, $question, $user_id);

            $dbh->addA($answer0, 1, $question_id, $lang);
            $dbh->addA($answer1, 0, $question_id, $lang);
            $dbh->addA($answer2, 0, $question_id, $lang);
            $dbh->addA($answer3, 0, $question_id, $lang);

            if (is_uploaded_file($_FILES['squared']['tmp_name'])) {
                $this->updateImage($_FILES['squared']['tmp_name'], 'squared', $question_id);
            }
            if (is_uploaded_file($_FILES['rectangular']['tmp_name'])) {
                $this->updateImage($_FILES['rectangular']['tmp_name'], 'rectangular', $question_id);
            }
            $dbs = new Application_Model_DbTable_Suggested();
            $dbs->updateStatus($suggested_id);
            $dbh->getDefaultAdapter()->commit();
            $this->_redirect('/questions/index/page/1/lang/*/country/*/category/*/level/*/search/' . urlencode($question));
        } catch (\Exception $e) {
            $dbh->getDefaultAdapter()->rollBack();
            $this->_helper->viewRenderer('add');
            $this->view->error = 'Error registrando la pregunta: ' . $e->getMessage();
        }
    }

}
