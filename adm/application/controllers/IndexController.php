<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if ($userInfo) {
            $this->view->email = $userInfo->email;            
            if ($userInfo->id_type == 1) {
                $this->view->id_type = "Admin";
            } elseif ($userInfo->id_type == 2) {
                $this->view->id_type = "Usuario";
            } elseif ($userInfo->id_type == 3) {
                $this->view->id_type = "Solo lectura";
            }
            $this->_redirect("/questions/");
        }else{
            $this->_redirect('/useradmin/');
        }
    }

}
