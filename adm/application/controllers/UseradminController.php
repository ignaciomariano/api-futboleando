<?php

require_once 'Zend/Auth.php';
require_once 'Zend/Auth/Adapter/DbTable.php';

class UseradminController extends Zend_Controller_Action {

    public $user = null;

    public function init() {

        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if ($userInfo) {
            $this->user = $userInfo->user;
        } else {
            $this->user = "anon";
        }
    }

    public function indexAction() {
        
    }

    public function authAction() {
        $this->_helper->layout->disableLayout();    //disable layout
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance();

        $authAdapter = new Zend_Auth_Adapter_DbTable();
        $authAdapter->setTableName('admin_user')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password');
        $uname = $request->getParam('username');
        $paswd = md5($request->getParam('password'));
        if (!empty($uname) && !empty($paswd)) {
            $authAdapter->setIdentity($uname);
            $authAdapter->setCredential($paswd);
            $result = $auth->authenticate($authAdapter);
            if ($result->isValid()) {
                $data = $authAdapter->getResultRowObject(null, 'password');
                $auth->getStorage()->write($data);
                $sess = new Zend_Session_Namespace('Zend_Auth');
                $sess->storage->user = "$uname";
                $sess->setExpirationSeconds(172800); //DOS DIAS
                Zend_Session::rememberMe(172800);
                Zend_Registry::get('log')->info("[$uname] USER/AUTH OK :$paswd");
                die('OK');
            } else {
                echo 'ERROR';
            }
        } else {
            echo 'ERROR';
        }
        Zend_Registry::get('log')->info("USER/AUTH FAIL ");
    }

    public function logoutAction() {

        $auth = Zend_Auth::getInstance(); 
        Zend_Registry::get('log')->info("[$this->user] USER/LOGOUT"); 
        $auth->clearIdentity();
        $this->_redirect('/useradmin/');
    }

}

?>
