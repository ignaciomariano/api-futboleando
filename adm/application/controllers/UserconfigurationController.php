<?php

class UserconfigurationController extends Zend_Controller_Action {

    public function init() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userInfo) {
            $this->_redirect('/index');
        }
        /* Initialize action controller here */
    }

    public function indexAction() {
        
    }

    public function managementAction() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if ($userInfo) {
            $this->view->email = $userInfo->email;
            if ($userInfo->id_type == 1) {
                $this->view->id_type = "Admin";
            } elseif ($userInfo->id_type == 2) {
                $this->view->id_type = "Usuario";
            } elseif ($userInfo->id_type == 3) {
                $this->view->id_type = "Solo lectura";
            }
        }
        $user_configuration = new Application_Model_DbTable_Userconfiguration();
        $this->view->configs = $user_configuration->getUserConfiguration();
    }

    public function updateAction() {
        $this->_helper->layout->disableLayout();    //disable layout
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $id = $request->getParam('id');
        $initial_budget = $request->getParam('initial_budget');
        #$maxium_transfer_allowed = $request->getParam('max_transfer_allowed');
        #$email_days_confirm = $request->getParam('email_days_confirm');
        $user_config = new Application_Model_DbTable_Userconfiguration();
        $update_status = $user_config->update($id, $initial_budget);
        #, $maxium_transfer_allowed, 
        #$email_days_confirm
        return $update_status;
    }

}
