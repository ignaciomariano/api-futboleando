<?php

# http://local.api.futboleando.com/adm/upload/filename/file2/limit/200/token/pmovil

class AdmController extends Zend_Controller_Action {
    public $json = false;
    public $error = false;
    public $errorn = 0;
    public $user = null;
    public $arr_parse = array('SA' => 3,
        'CM' => 3,
        'FI' => 2,
        'FN' => 1,
        'FA' => 1,
        'FO' => 4,
        'LOCAL' => 1,
        'CAMPEONATOS' => 1,
        'INTERNACIONAL' => 2,
        'MUNDIALES' => 3,
        'HISTORIA' => 4,
        'JUGADORES' => 5,
        'EQUIPOS' => 6);

    public function init() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userInfo) {
            $this->_redirect('/index');
        } else {
            $this->user = $userInfo->user;
        }
    }

    private function initLogUpload() {
        try {
            $hoy = date("d-m-y");
            $stream = @fopen(APPLICATION_PATH . "/../logs/upload_" . $hoy . ".log", "a", false);
            if (!$stream) {
                
            }
            $writer = new Zend_Log_Writer_Stream($stream);
            $log = new Zend_Log($writer);
            Zend_Registry::set('logu', $log);
        } catch (Exception $e) {
            Zend_Registry::set('logu', null);
        }
    }

    private function parseCategory($c) {
        $ret = (isset($this->arr_parse[strtoupper($c)])) ? $this->arr_parse[strtoupper($c)] : 2;
        #       echo "CAT:" . $ret;
        return (isset($this->arr_parse[strtoupper($c)])) ? $this->arr_parse[strtoupper($c)] : 2;
    }

    public function uploadAction() {

        $this->_helper->layout->disableLayout();    //disable layout
        $this->_helper->viewRenderer->setNoRender();
        try {

            #delete


            $bad = array(chr(130), chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(173), chr(160));
            $good = array(chr(44), chr(39), chr(39), chr(34), chr(34), chr(45), chr(45), chr(45), chr(32));

            $this->initLogUpload();
            # globales
            #header('Content-type: text/html;');
            ini_set('error_reporting', 'E_ALL');
            set_time_limit(0);
            $admH = new Application_Model_DbTable_Adm();
            # config params
            $limitRows = $this->getRequest()->getParam('limit');

            if (!intval($limitRows)) {
                $limitRows = 10000;
            }
            #    $this->log->log("limitRows: $limitRows", Zend_Log::INFO);
            # 

            $n = $this->getRequest()->getParam('n');
            $arr = array();
            $row = exec('ls -t ../files/', $output, $error);
            while (list(, $row) = each($output)) {
                $arr[] = $row;
            }
            $i = 0;
            foreach ($arr as $fl) {
                $i++;
                if ($i == $n) {
                    $filename = $fl;
                    continue;
                }
            }
            $i = 0;
            #      die($filename);
            #  $filename = $this->getRequest()->getParam('filename');
            if (empty($filename)) {
                throw new Exception('filename is invalid', 0);
            }
            echo "Procesando $filename\n;";
            $country_id = '*';
            $level = '1';
            #   if (strlen($lang)==0) { $lang = 'ES'; };
#### 
            $file = __dir__ . "/../../files/" . $filename;
            require_once 'excel_reader2.php';
            $data = new Spreadsheet_Excel_Reader($file, false, 'UTF-16');
            if ($data === NULL) {
                throw new Exception('File error', 101);
            }
            $row = 1; // begins at row2
            $add = 0;
            $a1 = $a2 = $a3 = $a4 = null;
            $row_empty = 0;
            do {
                #      echo "$row.";
                $lang = $data->val(1, 9);
                if (empty($lang)) {
                    $lang = 'ES';
                }
                # Inicio pregunta
                $row++;

                #break ?
                if ($row > $limitRows) {
                    break;
                }
                $level = 1;
                $cat = strtoupper($data->val($row, 1));
                $question = $data->val($row, 2);
                $is_correct = intval($data->val($row, 3));
                $country_id = strtoupper($data->val($row, 7));
                $r = array();
                $r[] = $a1 = $data->val($row, 3);
                $r[] = $a2 = $data->val($row, 4);
                $r[] = $a3 = $data->val($row, 5);
                $r[] = $a4 = $data->val($row, 6);
                if (intval($data->val($row, 8)) > 0) {
                    $level = intval($data->val($row, 8));
                }
                #     $this->log->log("Row $row / Empty: $row_empty", Zend_Log::INFO);
                if (
                        strlen($cat) < 1 ||
                        empty($question) || strlen($question)<2
                ) {

                    $row_empty++;
                } else {

                    $row_empty = 0;
                    $add++;

                    $category_id = $this->parseCategory($cat);

                    # cargar pregunta

                    $info = array('level' => $level, 'country_id' => $country_id, 'category_id' => $category_id);
                    $question_id = $admH->addQuestion($info);
                    $question = str_replace("\n", " ", $question);
                    $question = str_replace("  ", " ", $question);


                    $question = str_replace($bad, $good, $question);
                    $info = array('question_id' => $question_id, 'question' => /*mysql_real_escape_string(*/utf8_encode($question)/*)*/, 'lang' => $lang);
                    $question_langs = $admH->addQuestionLang($info);

                    echo "<br />" . utf8_encode($question . "<br />Answers: $a1|$a2|$a3|$a4");
                    Zend_Registry::get('logu')->log("[$this->user][$filename]Question: " . utf8_encode($question . "\nAnswers: $a1|$a2|$a3|$a4"), Zend_Log::INFO);
                    $is_correct = 1;
                    # cargar respuestas
                    foreach ($r as $answer) {
                        $info = array('is_correct' => $is_correct, 'question_id' => $question_id);
                        $answer_id = $admH->addAnswer($info);
                        $info = array('answer_id' => $answer_id, 'answer' => /*mysql_real_escape_string(*/utf8_encode($answer)/*)*/, 'lang' => $lang);
                        $answer_langs = $admH->addAnswerLang($info);
                        $is_correct = 0;
                    }
                }
            } while ($row_empty < 14);
            
            Zend_Registry::get('log')->log("[$this->user][$filename]add-questions:$add",6);
#        $this->json = array('status' => 'OK', 'data' => $add);
        } catch (Exception $ex) {

            echo $ex->getMessage();
        }
    }

}
