<?php

class QuestionsController extends Zend_Controller_Action {

    public $user = null;

    public function init() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userInfo) {
            $this->_redirect('/index');
        } else {
            $this->view->email = $userInfo->email;
            if ($userInfo->id_type == 1) {
                $this->view->id_type = "Admin";
            } elseif ($userInfo->id_type == 2) {
                $this->view->id_type = "Usuario";
            } elseif ($userInfo->id_type == 3) {
                $this->view->id_type = "Solo lectura";
            }
            $this->user = $userInfo->user;
        }
        $this->config = Zend_Registry::get('config');
        $this->view->imgsUrl = $this->config->mediaConverterFrontEnd.'media/contdin/fixo/questions/';
    }

    public function dropAction() {
        $id = $this->getRequest()->getParam('question_id');
        if ($id) {

            $dbh = new Application_Model_DbTable_Questions();
            $dbh->deleteQ($id);
            Zend_Registry::get('log')->info("[$this->user] DELETE_QUESTION:$id");

            die('OK');
        }
    }

    public function indexAction() {


        $search = $this->getRequest()->getParam('search') ? $this->getRequest()->getParam('search') : null;

        $this->view->search_txt = !empty($search) ? $search : "";
        $selected_lang = $this->getRequest()->getParam('lang') ? $this->getRequest()->getParam('lang') : null;
        if (!$selected_lang) {
            return null;
        }
        $selected_country = $this->getRequest()->getParam('country') ? $this->getRequest()->getParam('country') : null;
        $selected_level = $this->getRequest()->getParam('level') ? $this->getRequest()->getParam('level') : null;
        $selected_category = $this->getRequest()->getParam('category') ? $this->getRequest()->getParam('category') : null;
        $cant = $this->getRequest()->getParam('cant') ? $this->getRequest()->getParam('cant') : CANT_DEFAULT;
        $page = $this->getRequest()->getParam('page') ? $this->getRequest()->getParam('page') : PAGE_DEFAULT;


        $this->view->selected_lang = $selected_lang;
        $this->view->selected_country = $selected_country;
        $this->view->selected_level = $selected_level;
        $this->view->selected_category = $selected_category;
        $dbh = new Application_Model_DbTable_Questions();
        $count = $dbh->getall('count', $page, $cant, $selected_lang, $selected_country, $selected_category, $selected_level, $search);
        $rows = $dbh->getall('query', $page, $cant, $selected_lang, $selected_country, $selected_category, $selected_level, $search);

        $return['questions'] = array();
        if ($rows) {
            foreach ($rows as $answer) {
                $respuesta = Array();
                $respuesta['answer'] = $answer['answer'];
                $respuesta['answer_id'] = $answer['answer_id'];
                $respuesta['al_id'] = $answer['al_id'];
                $respuesta['is_correct'] = $answer['is_correct'];
                $pregunta = $answer;
                unset($pregunta['answer']);
                unset($pregunta['is_correct']);
                unset($pregunta['answer_id']);

# si existe la pregunta guardamos la respuesta
                if (isset($return['questions'][$answer['question_id']])) {
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
# si no existe creamos la pregunta y guardamos la respuesta
                } else {
                    $return['questions'][$answer['question_id']] = $pregunta;
                    $return['questions'][$answer['question_id']]['answers'] = Array();
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                }
            }
        }

        $this->view->questions = $return['questions'];




#paginado :
        $cmb_paginado = null;
        if ($count) {
            $paginas = ($count / $cant) + 1;
            $cmb_paginado = "<select id='filter_page'>";
            for ($i = 1; $i < $paginas; $i++) {
                if ($page == $i) {
                    $cmb_paginado.= "<option selected='selected' value='$i'>$i</option>";
                } else {
                    $cmb_paginado.= "<option value='$i'>$i</option>";
                }
            }
            $cmb_paginado.= "</select>";
        }
        $this->view->cmb_paginado = $cmb_paginado;
    }

    public function editAction() {



        $qlid = $this->getRequest()->getParam('qlid');


        $dbh = new Application_Model_DbTable_Questions();
        $rows = $dbh->getqlid($qlid);
        $return['questions'] = array();
        if ($rows) {

            foreach ($rows as $answer) {
                $respuesta = Array();
                $respuesta['answer'] = $answer['answer'];
                $respuesta['answer_id'] = $answer['answer_id'];

                $respuesta['al_id'] = $answer['al_id'];
                $respuesta['is_correct'] = $answer['is_correct'];
                $pregunta = $answer;
                unset($pregunta['answer']);
                unset($pregunta['is_correct']);
                unset($pregunta['answer_id']);

# si existe la pregunta guardamos la respuesta
                if (isset($return['questions'][$answer['question_id']])) {
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
# si no existe creamos la pregunta y guardamos la respuesta
                } else {
                    $return['questions'][$answer['question_id']] = $pregunta;
                    $return['questions'][$answer['question_id']]['answers'] = Array();
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                }
            }
        }
        #  shuffle($return['questions']);
        $this->view->data = $return['questions'];
    }

    public function upexcelAction() {

        $n = $this->getRequest()->getParam('delete');
        if ($n > 0) {
            $arr = array();
            $row = exec('ls -t ../files/', $output, $error);
            while (list(, $row) = each($output)) {
                $arr[] = $row;
            }
            $i = 0;
            foreach ($arr as $fl) {
                $i++;
                if ($i == $n) {
                    $filename = $fl;
                    continue;
                }
            }
            $i = 0;
            exec("rm -rf ../files/" . str_replace(" ", "\ ", "$filename"));
            Zend_Registry::get('log')->info("[$this->user] DELETE_FILE $filename");
        }


        $adapter = new Zend_File_Transfer_Adapter_Http();
        $adapter->setDestination('../files/');

        $files = $adapter->getFileInfo();
        $msg = null;
        if ($files) {
            foreach ($files as $file => $info) {
                // file uploaded ?
                if (!$adapter->isUploaded($file)) {
                    $msg = "Why havn't you uploaded the file ?";
                    continue;
                }
                if ($info['type'] != 'application/vnd.ms-excel' && $info['type'] != 'application/vnd.ms-office' && 0 > strpos($info['type'], 'ms-excel')) {
                    $msg = "Invalid content type:" . $info['type'];
                }
                // validators are ok ? 
                if (!$adapter->isValid($file)) {
                    $msg = "Sorry but " . $info['name'] . " is not what we wanted. " . var_export($info);
                }
                $names = $info['name'];
                if (strpos($names, ".xls") == -1) {
                    $msg = "Invalid format. Must have .xls at end.";
                }


                if (!$msg) {
                    if (!$adapter->receive()) {
                        $messages = $adapter->getMessages();
                        $msg = implode("\n", $messages);
                    } else {
                        Zend_Registry::get('log')->info("[$this->user] UPLOAD_FILE OK $names");
                        $msg = null;
                    }
                }
            }
            if ($msg) {
                Zend_Registry::get('log')->info("[$this->user] UPLOAD_FILE ERROR $msg");
                echo $msg;
            }
        }
        $this->view->lista = "";
        $arr = array();
        $row = exec('ls -t ../files/', $output, $error);
        while (list(, $row) = each($output)) {
            $arr[] = $row;
        }
        $this->view->lista = $arr;
    }

    public function addAction() {
        
    }

    public function updateAction() {
        $qlid = $this->getRequest()->getParam('qlid');
        $question = $this->getRequest()->getParam('question');
        $level = $this->getRequest()->getParam('level');
        $cat = $this->getRequest()->getParam('category_id');
        $country_id = $this->getRequest()->getParam('country_id');
        $answer0 = $this->getRequest()->getParam('answer0');
        $answer1 = $this->getRequest()->getParam('answer1');
        $answer2 = $this->getRequest()->getParam('answer2');
        $answer3 = $this->getRequest()->getParam('answer3');

        $question_id = 0;
        $dbh = new Application_Model_DbTable_Questions();
        $rows = $dbh->getqlid($qlid);
        $return['questions'] = array();
        if ($rows) {

            foreach ($rows as $answer) {
                $respuesta = Array();
                $respuesta['answer'] = $answer['answer'];
                $respuesta['answer_id'] = $answer['answer_id'];
                $respuesta['al_id'] = $answer['al_id'];
                $respuesta['is_correct'] = $answer['is_correct'];
                $pregunta = $answer;
                unset($pregunta['answer']);
                unset($pregunta['is_correct']);
                unset($pregunta['answer_id']);
                $question_id = $pregunta['question_id'];
# si existe la pregunta guardamos la respuesta
                if (isset($return['questions'][$answer['question_id']])) {
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
# si no existe creamos la pregunta y guardamos la respuesta
                } else {
                    $return['questions'][$answer['question_id']] = $pregunta;
                    $return['questions'][$answer['question_id']]['answers'] = Array();
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                }
            }
        }
        #     shuffle($return['questions']);




        $bad = array(chr(130), chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(173), chr(160));
        $good = array(chr(44), chr(39), chr(39), chr(34), chr(34), chr(45), chr(45), chr(45), chr(32));

        $question = str_replace("\n", " ", $question);
        $question = str_replace("  ", " ", $question); 
        
        #$question =  str_replace("\"","\", $question); 
     
        $this->view->data = $return['questions'];

        try{
            $dbh->getDefaultAdapter()->beginTransaction();

            $this->updateQuestion($return['questions'][$question_id]['question_id'], $level, $country_id, $cat);
            $this->updateQuestionLang($question, $qlid);
            $this->updateAnswerLang($answer0, $return['questions'][$question_id]['answers'][0]['al_id']);
            $this->updateAnswerLang($answer1, $return['questions'][$question_id]['answers'][1]['al_id']);
            $this->updateAnswerLang($answer2, $return['questions'][$question_id]['answers'][2]['al_id']);
            $this->updateAnswerLang($answer3, $return['questions'][$question_id]['answers'][3]['al_id']);
            if(is_uploaded_file($_FILES['squared']['tmp_name'])){
                $this->updateImage($_FILES['squared']['tmp_name'], 'squared', $question_id);
            }
            if(is_uploaded_file($_FILES['rectangular']['tmp_name'])){
                $this->updateImage($_FILES['rectangular']['tmp_name'], 'rectangular', $question_id);
            }
            $dbh->getDefaultAdapter()->commit();
            $this->_redirect('/questions/index/page/1/lang/*/country/*/category/*/level/*/search/'.  urlencode($question));
            
        } catch (\Exception $e){
            $dbh->getDefaultAdapter()->rollBack();
            $this->_helper->viewRenderer('edit');
            $this->view->error = 'Error cambiando la pregunta: '.$e->getMessage();
        }
    }

    private function updateQuestion($question_id, $level, $country_id, $cat) {
        $dbh = new Application_Model_DbTable_Questions();
        $dbh->updateQ($question_id, $level, $country_id, $cat);
    }

    private function updateQuestionLang($question, $qlid) {
        $dbh = new Application_Model_DbTable_Questions();
        $dbh->updateQL($question, $qlid);
    }

    private function updateAnswerLang($answer, $alid) {
        $dbh = new Application_Model_DbTable_Questions();
        $dbh->updateAL($answer, $alid);
    }

    public function saveAction() {
        $question_id = $this->getRequest()->getParam('question_id');
        $lang = $this->getRequest()->getParam('lang');
        $level = $this->getRequest()->getParam('level');
        $cat = $this->getRequest()->getParam('category_id');
        $country_id = $this->getRequest()->getParam('country_id');
        $question = $this->getRequest()->getParam('question');
        $answer0 = $this->getRequest()->getParam('answer0');
        $answer1 = $this->getRequest()->getParam('answer1');
        $answer2 = $this->getRequest()->getParam('answer2');
        $answer3 = $this->getRequest()->getParam('answer3');


        $dbh = new Application_Model_DbTable_Questions();
        try{
            $dbh->getDefaultAdapter()->beginTransaction();
            $question_id = $dbh->addQ(0, $level, $country_id, $lang, $cat, $question);

            $dbh->addA($answer0, 1, $question_id, $lang);
            $dbh->addA($answer1, 0, $question_id, $lang);
            $dbh->addA($answer2, 0, $question_id, $lang);
            $dbh->addA($answer3, 0, $question_id, $lang);

            if(is_uploaded_file($_FILES['squared']['tmp_name'])){
                $this->updateImage($_FILES['squared']['tmp_name'], 'squared', $question_id);
            }
            if(is_uploaded_file($_FILES['rectangular']['tmp_name'])){
                $this->updateImage($_FILES['rectangular']['tmp_name'], 'rectangular', $question_id);
            }
            $dbh->getDefaultAdapter()->commit();
            $this->_redirect('/questions/index/page/1/lang/*/country/*/category/*/level/*/search/'.  urlencode($question));
        }  catch (\Exception $e){
            $dbh->getDefaultAdapter()->rollBack();
            $this->_helper->viewRenderer('add');
            $this->view->error = 'Error registrando la pregunta: '.$e->getMessage();
        }
    }
    
    public function updateImage($arq, $type, $question_id){
        $time = time();
        $dbh = new Application_Model_DbTable_Questions();
        $uploader = new Pmovil2_ConversorDeMidia_Uploader($this->config->mediaConverterUrl);
        $uploader->repassaUpload($_FILES[$type], "questions/$question_id/$type", $time);
        $dbh->update(array("${type}_image_at" => new Zend_Db_Expr("FROM_UNIXTIME($time)")), "question_id = $question_id");
    }

}
