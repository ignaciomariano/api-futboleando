<?php

class ReportedController extends Zend_Controller_Action {

    public $user = null;

    public function init() {
        $userInfo = Zend_Auth::getInstance()->getStorage()->read();
        if (!$userInfo) {
            $this->_redirect('/index');
        } else {
            $this->view->email = $userInfo->email;
            if ($userInfo->id_type == 1) {
                $this->view->id_type = "Admin";
            } elseif ($userInfo->id_type == 2) {
                $this->view->id_type = "Usuario";
            } elseif ($userInfo->id_type == 3) {
                $this->view->id_type = "Solo lectura";
            }
            $this->user = $userInfo->user;
        }
        $this->config = Zend_Registry::get('config');
        $this->view->imgsUrl = $this->config->mediaConverterFrontEnd . 'media/contdin/fixo/questions/';
    }

    public function indexAction() {
        $search = $this->getRequest()->getParam('search') ? $this->getRequest()->getParam('search') : null;

        $this->view->search_txt = !empty($search) ? $search : "";
        $selected_lang = $this->getRequest()->getParam('lang') ? $this->getRequest()->getParam('lang') : null;
        if (!$selected_lang) {
            return null;
        }
        $selected_country = $this->getRequest()->getParam('country') ? $this->getRequest()->getParam('country') : null;
        $selected_level = $this->getRequest()->getParam('level') ? $this->getRequest()->getParam('level') : null;
        $selected_category = $this->getRequest()->getParam('category') ? $this->getRequest()->getParam('category') : null;
        $cant = $this->getRequest()->getParam('cant') ? $this->getRequest()->getParam('cant') : CANT_DEFAULT;
        $page = $this->getRequest()->getParam('page') ? $this->getRequest()->getParam('page') : PAGE_DEFAULT;


        $this->view->selected_lang = $selected_lang;
        $this->view->selected_country = $selected_country;
        $this->view->selected_level = $selected_level;
        $this->view->selected_category = $selected_category;
        $dbh = new Application_Model_DbTable_Reported();
        $count = $dbh->getall('count', $page, $cant, $selected_lang, $selected_country, $selected_category, $selected_level, $search);
        $rows = $dbh->getall('query', $page, $cant, $selected_lang, $selected_country, $selected_category, $selected_level, $search);
        
        $this->view->questions = $rows;
        #
#paginado :
        $cmb_paginado = null;
        if ($count) {
            $paginas = ($count / $cant) + 1;
            $cmb_paginado = "<select id='filter_page'>";
            for ($i = 1; $i < $paginas; $i++) {
                if ($page == $i) {
                    $cmb_paginado.= "<option selected='selected' value='$i'>$i</option>";
                } else {
                    $cmb_paginado.= "<option value='$i'>$i</option>";
                }
            }
            $cmb_paginado.= "</select>";
        }
        $this->view->cmb_paginado = $cmb_paginado;
    }

    public function updateAction() {
        $this->_helper->layout->disableLayout();    //disable layout
        $this->_helper->viewRenderer->setNoRender();
        $question_id = $this->getRequest()->getParam('question_id');
        $reported_reasons_id = $this->getRequest()->getParam('reported_reasons_id');
        $db = new Application_Model_DbTable_Reported();
        try {
            $db->update($question_id,$reported_reasons_id);
            echo "OK";
        } catch (\Exception $e) {
            echo "Error al corregir: " . $e->getMessage();
        }
    }

}
