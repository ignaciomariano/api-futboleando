<?php

$config = include APPLICATION_PATH.'/../../config.php';

$host = preg_replace('/:.*/', '', $_SERVER['HTTP_HOST']);
$host_parts = explode('.',$host);
array_shift($host_parts);
$domain = implode('.', $host_parts);

return  array(
    'phpSettings' => array(
        'display_startup_errors' => (APPLICATION_ENV == 'production'? 0 : 1),
        'display_errors' => (APPLICATION_ENV == 'production'? 0 : 1)
    ),
    'includePaths' => array(
        'library' => APPLICATION_PATH."/../library"
    ),
    'bootstrap' => array(
        'path' => APPLICATION_PATH . "/Bootstrap.php",
        'class' => "Bootstrap"
    ),
    'appnamespace' => "Application",
    'autoloaderNamespaces' => array(
        "ST_",
        "Pmovil_"
    ),
    'resources' => array(
        'frontController' => array(
            'controllerDirectory' => APPLICATION_PATH . "/controllers",
            'actionhelperpaths' => array(
                'Zend_Controller_Action_Helper' => APPLICATION_PATH . "/controllers/helpers"
            ),
            'params' => array(
                'displayExceptions' => (APPLICATION_ENV == 'production'? 0 : 1)
            )
        ),
        'db' => array(
            'adapter' => "PDO_MYSQL",
            'params' => array_merge($config['database']['params'], 
                array(
                    'profiler' => array(
                        'enabled' => true,
                        'class' => 'Pmovil_Db_Profiler_Log'
                    )
                )
            ),
            'isDefaultTableAdapter' => true
        ),
        'layout' => array(
            'layoutPath' => APPLICATION_PATH . "/layouts/scripts"
        ),
        'session' => array(
            'cookie_lifetime' => 864000,
            'gc_maxlifetime' => 864000,
            'remember_me_seconds' => 864000,
            'cookie_domain' => ".$domain"
        ),
        'view' => array(
            'helperPath' => APPLICATION_PATH."/views/helpers"
        )
    ),
    'constants' => array(
        'CATEGORY_ID_OFFLINE' => 1,
        'CATEGORY_ID_ONLINE' => 2
    ),
    'mediaConverterUrl' => $config['mediaConverterUrl'],
    'mediaConverterFrontEnd' => $config['mediaConverterFrontEnd'],
    'mediaConverter' => $config['mediaConverter'],
    'log4php' => $config['log4php']
);


