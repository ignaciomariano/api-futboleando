<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initVars() {
        
    }

    protected function _initLog() {
        try {
            $config = $this->getOptions();
            Logger::configure($config['log4php']);
            $logger = Logger::getRootLogger();
            Zend_Registry::set('log', $logger);
        } catch (Exception $e) {
            Zend_Registry::set('log', null);
        }
    }

    protected function _initSessions() {
        define('CANT_DEFAULT', 50);
        define('MAX_QUESTION_LENGTH', 140);
        define('MAX_ANSWER_LENGTH', 30);
        define('PAGE_DEFAULT', 1);
        define('CANTIDAD_RESPUESTAS', 4);
        header('Content-Type: text/html; charset=UTF-8');
        $this->bootstrap('session');
        # session_start();
    }
    
    protected function _initConfig() {
        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);
        return $config;
    }    

}
