<?php

class Application_Model_DbTable_Adm extends Zend_Db_Table_Abstract {

    protected $_name = 'questions';

    public function addQuestion($info) { 
        $id = $this->insert($info);
        if ($id) {
            return $id;
        } else {
            throw new Exception('No se puede agregar la pregunta', '801');
        }
    }

    public function addQuestionLang($info) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();

        $select = "INSERT INTO question_lang VALUES (null, "
                . $info['question_id'] . ", '" . $info['question'] . "', '" . $info['lang'] . "') ON DUPLICATE KEY UPDATE question_id=question_id;";
        $db->query($select);
        $id = $db->getConnection()->lastInsertId();
        if ($id) {
            return $id;
        } else {
            echo '<br />####<br />No se puede agregar la pregunta ('.$info["question"].')<br />####<br />';
        }
    }
  public function addAnswer($info) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();

        $select = "INSERT INTO answers VALUES (null, ".$info['is_correct'].", ".$info['question_id'].",null);";
        $db->query($select);
        $id = $db->getConnection()->lastInsertId();
        if ($id) {
            return $id;
        } else {
            throw new Exception('No se puede agregar la respuesta', '801');
        }
    }
  public function addAnswerLang($info) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();

        $select = "INSERT INTO answer_lang VALUES (null, ".$info['answer_id'].", '".$info['answer']."', '".$info['lang']."');";
        $db->query($select);
        $id = $db->getConnection()->lastInsertId();
        if ($id) {
            return $id;
        } else {
            throw new Exception('No se puede agregar la respuesta (lang)', '801');
        }
    } 
}
