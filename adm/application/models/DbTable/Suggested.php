<?php

class Application_Model_DbTable_Suggested extends Zend_Db_Table_Abstract {

    protected $_name = 'suggested';

    public function addQ($question_id, $level, $country_id, $lang, $cat, $question, $square = null, $rectangular = null) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "INSERT INTO questions VALUES (null, $level, $cat, '$country_id',";
        $sql .= empty($square) ? 'null,' : "FROM_UNIXTIME($square),";
        $sql .= empty($rectangular) ? 'null)' : "FROM_UNIXTIME($rectangular))";
        $desc = $sql;
        $db->query($sql);
        $id = $db->lastInsertId();
        $sql = "INSERT INTO question_lang VALUES (null, $id, '$question', '$lang');";
        $desc.=$sql;
        #die($desc);
        $db->query($sql);
        return $id;
    }

    public function addA($answer, $correct, $question_id, $lang) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "INSERT INTO answers VALUES (null, $correct, $question_id, null);";
        $desc = $sql;
        $db->query($sql);
        $id = $db->lastInsertId();

        $sql = "INSERT INTO answer_lang VALUES (null, $id, '$answer', '$lang');";
        $desc.=$sql;
#       die($desc);
        $db->query($sql);
    }

    public function cleanBase() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "DELETE FROM answers WHERE question_id NOT IN (select question_id from questions);";
        $db->query($sql);
        $sql = "DELETE FROM answer_lang WHERE answer_id NOT IN (select answer_id from answers);";
        $db->query($sql);
        $sql = "DELETE FROM question_lang WHERE question_id NOT IN (select question_id from questions);";
        $db->query($sql);
    }

    public function deleteQ($question_id) {
        if ($question_id) {
            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            try {
                $sql = "DELETE FROM suggested WHERE id = $question_id;";
                $db->query($sql);
            } catch (\Exception $e) {
                print_r($e);
            }

            return true;
        }
        return false;
    }

    #public function updateQ($question_id, $level, $country_id, $cat) {

    public function updateQ($qlid, $question, $level, $country_id, $cat, $answer0, $answer1, $answer2, $answer3) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update suggested set category_id = $cat, country_id = '$country_id', question = '$question',  "
                . "answer_correct = '$answer0' , answer_incorrect1 = '$answer1'"
                . ", answer_incorrect2 = '$answer2', answer_incorrect3 = '$answer3' "
                . "WHERE id = $qlid;";
        #level = $level,
        #level = '$level',
        #die($sql);
        $db->query($sql);
    }

    public function updateStatus($suggested_id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update suggested set production = 1 where id = $suggested_id";
        $db->query($sql);
    }

    public function updateQL($question, $qlid) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update question_lang set question = ? WHERE id = ?;";
        #  die($sql);
        $stm = $db->prepare($sql);
        $stm->execute(array($question, $qlid));
    }

    public function updateAL($answer, $alid) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update answer_lang set answer = ? WHERE id = ?;";
        $stm = $db->prepare($sql);
        $stm->execute(array($answer, $alid));
    }

    public function getall($type = "query", $page = PAGE_DEFAULT, $cant = CANT_DEFAULT, $lang = null, $country = null, $category = null, $level = null, $search = null) {
        $filter_lang = $filter_category = $filter_country = $filter_level = $filter_search = "";
        if (!empty($search)) {
            $filter_search = " AND question like '%$search%' ";
        }
        #filters
        if (isset($lang)) {
            $filter_lang = ($lang != "*") ? " AND lang1 = '$lang' AND lang2 = '$lang' " : "";
        }
        if (isset($country)) {
            $filter_country = ($country != "*") ? " AND country_id like '%$country%' " : "";
        }
        if (isset($category)) {
            $filter_category = ($category != "*") ? " AND category_id = $category " : "";
        }
        if (isset($level)) {
            $filter_level = ($level != "*") ? " AND level = '$level' " : "";
        }
        $WHERE = " WHERE  id > 0 $filter_lang $filter_country $filter_category $filter_level $filter_search AND production = 0";

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $cant = $cant * CANTIDAD_RESPUESTAS;
        if ($type == 'count') {
            $sql = "SELECT COUNT(question) as count FROM suggested $WHERE;";
            $stmt = $db->query($sql);
            $rows = $stmt->fetch();
            $cant = 0;
            if (isset($rows['count'])) {
                $cant = intval(intval($rows['count']) / 4);
            }
            return intval($cant);
        }
        $limit1 = ($page - 1) * $cant;
        $sql = "SELECT * FROM suggested $WHERE ORDER BY id DESC LIMIT " . $limit1 . ", " . $cant . ";";
        #die($sql);
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        return $rows;
    }

    public function getqlid($qlid) {

        # 
        $WHERE = " WHERE id = $qlid";
        #
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();

        $sql = "SELECT COUNT(id) as count FROM suggested $WHERE;";
        $stmt = $db->query($sql);
        $rows = $stmt->fetch();
        $cant = CANTIDAD_RESPUESTAS;
        #$sql = "SELECT  *, UNIX_TIMESTAMP(squared_image_at) AS squared_image_id, UNIX_TIMESTAMP(rectangular_image_at) AS rectangular_image_id FROM admQuestList $WHERE ORDER BY question_id ASC, is_correct DESC, answer_id ASC LIMIT   $cant ;";
        $sql = "SELECT  * FROM suggested $WHERE ORDER BY id ASC  LIMIT   $cant ;";
#die($sql);
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        return $rows;
    }

}
