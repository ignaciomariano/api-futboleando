<?php

class Application_Model_DbTable_Teamdf extends Zend_Db_Table_Abstract {

    protected $_name = 'team_df';

    public function getTeam($id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT name FROM team_df where id = ' . $id  . '&& id_campeonato = ' .ID_CAMPEONATO;
        $stmt = $db->query($sql);
        $rows = $stmt->fetchObject();
        if ($rows != false) {
            return $rows->name;
        } else {
            return 0;
        }
    }

    public function getTeams() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ); 
        $sql = 'SELECT * FROM team_df where id_campeonato = ' .ID_CAMPEONATO;
 
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
