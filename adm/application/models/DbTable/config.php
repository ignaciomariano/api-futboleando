<?php

class Application_Model_DbTable_config extends Zend_Db_Table_Abstract {

    protected $_name = 'config';

    public function update($id_campeonato, $status) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $date = date("Y-m-d");
        $data = array('status' => $status , 'config_date' => $date);
        $where = array('id_campeonato = ?' => $id_campeonato);
        $update_status = $db->update($this->_name, $data, $where);
        return $update_status;
    }


    public function getConfig() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $sql = 'SELECT * FROM config where id_campeonato = ' . ID_CAMPEONATO;
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
