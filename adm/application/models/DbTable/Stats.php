<?php

class Application_Model_DbTable_Stats extends Zend_Db_Table_Abstract {

    protected $_name = '';

    public function getuseractives() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $sql = 'select count(*) as useractives '
                . 'from user '
                . 'where status = 1;';
        $stmt = $db->query($sql);
        $row = $stmt->fetch();
        if ($row != false && sizeof($row) == 1) {
            return $row;
        } else {
            return 0;
        }
    }

    public function getusernoactives() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'select count(*) as usernoactives '
                . 'from user '
                . 'where status = 0;';
        $stmt = $db->query($sql);
        $row = $stmt->fetch();
        if ($row != false && sizeof($row) == 1) {
            return $row;
        } else {
            return 0;
        }
    }
    
        public function getusers() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'select count(*) as users '
                . 'from user;';
        $stmt = $db->query($sql);
        $row = $stmt->fetch();
        if ($row != false && sizeof($row) == 1) {
            return $row;
        } else {
            return 0;
        }
    }

        public function getteamactives() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $sql = 'select count(*) as teamactives '
                . 'from team '
                . 'where status = 1;';
        $stmt = $db->query($sql);
        $row = $stmt->fetch();
        if ($row != false && sizeof($row) == 1) {
            return $row;
        } else {
            return 0;
        }
    }

    public function getteamnoactives() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'select count(*) as teamnoactives '
                . 'from team '
                . 'where status = 0;';
        $stmt = $db->query($sql);
        $row = $stmt->fetch();
        if ($row != false && sizeof($row) == 1) {
            return $row;
        } else {
            return 0;
        }
    }
    
        public function getteams() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'select count(*) as teams '
                . 'from team;';
        $stmt = $db->query($sql);
        $row = $stmt->fetch();
        if ($row != false && sizeof($row) == 1) {
            return $row;
        } else {
            return 0;
        }
    }

}
