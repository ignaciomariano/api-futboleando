<?php

class Application_Model_DbTable_Reported extends Zend_Db_Table_Abstract {

    protected $_name = 'reported';

    public function getall($type = "query", $page = PAGE_DEFAULT, $cant = CANT_DEFAULT, $lang = null, $country = null, $category = null, $level = null, $search = null) {
        $filter_lang = $filter_category = $filter_country = $filter_level = $filter_search = "";
        if (!empty($search)) {
            $filter_search = " AND question like '%$search%' ";
        }
#filters
        if (isset($lang)) {
            $filter_lang = ($lang != "*") ? " AND lang1 = '$lang' AND lang2 = '$lang' " : "";
        }
        if (isset($country)) {
            $filter_country = ($country != "*") ? " AND country_id like '%$country%' " : "";
        }
        if (isset($category)) {
            $filter_category = ($category != "*") ? " AND category_id = $category " : "";
        }
        if (isset($level)) {
            $filter_level = ($level != "*") ? " AND level = '$level' " : "";
        }
        $WHERE = " WHERE r.id > 0 $filter_lang $filter_country $filter_category $filter_level $filter_search AND checked = 0";

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $cant = $cant * CANTIDAD_RESPUESTAS;
        if ($type == 'count') {
            $sql = "SELECT count(DISTINCT reported_reasons_id, r.question_id) as count FROM reported r $WHERE;";
            $stmt = $db->query($sql);
            $rows = $stmt->fetch();
            $cant = 0;
            if (isset($rows['count'])) {
                $cant = intval(intval($rows['count']) / 4);
            }
            return intval($cant);
        }
        $limit1 = ($page - 1) * $cant;
        $sql = "SELECT r.id, q.question_id, q.user_id, r.date, r.reported_reasons_id, r.reasons, r.checked, q.category_id, q.country_id, ql.question, ql.lang,  count(*) as cantidad FROM reported r LEFT JOIN questions q ON q.question_id = r.question_id LEFT JOIN question_lang ql ON q.question_id = ql.question_id $WHERE GROUP BY reported_reasons_id, r.question_id ORDER BY cantidad DESC LIMIT " . $limit1 . ", " . $cant . ";";
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        return $rows;
    }

    public function getqlid($qlid) {

        $WHERE = " WHERE ql_id = $qlid";
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "SELECT COUNT(id) as count FROM reported $WHERE;";
        $stmt = $db->query($sql);
        $rows = $stmt->fetch();
        $cant = CANTIDAD_RESPUESTAS;
        $sql = "SELECT  * FROM reported $WHERE ORDER BY question_id ASC, is_correct DESC, answer_id ASC LIMIT   $cant ;";
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        return $rows;
    }

    public function update($question_id, $reported_reasons_id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'checked' => 1
        );
        $where["question_id  = ?"] = $question_id;
        $where["reported_reasons_id = ?"] = $reported_reasons_id;
        $res = $db->update('reported', $data, $where);
        return $res;
    }

}
