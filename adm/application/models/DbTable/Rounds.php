<?php

class Application_Model_DbTable_Rounds extends Zend_Db_Table_Abstract {

    protected $_name = 'round';

    public function get() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT id, number FROM round where id_campeonato = ' . ID_CAMPEONATO;
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

    public function getId($id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT distinct r.number as number '
                . 'FROM round as r inner join '
                . 'matchs as m on r.id = m.round_id '
                . 'where m.round_id='. $id;
        #print_r($sql);die();
        $stmt = $db->query($sql);
        $rows = $stmt->fetchObject();
        if ($rows != false) {
            return $rows->number;
        } else {
            return 0;
        }
    }

}
