<?php

class Application_Model_DbTable_Tactics extends Zend_Db_Table_Abstract {

    protected $_name = 'tactics';

    public function add($name, $defensodres, $mediocampistas, $delanteros, $status) {
        $tactic_id = $this->insert(array(
            'name' => $name,
            'defenders' => $defensodres,
            'midfielders' => $mediocampistas,
            'strikers' => $delanteros,
            'status' => $status
        ));
        return $tactic_id;
    }

    public function update($id, $status) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array('status' => $status);
        $where = array('id = ?' => $id);
        $update_status = $db->update($this->_name, $data, $where);
        return $update_status;
    }

    public function delete($id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $tactic_id = $db->delete('tactics', array(
            'id = ?' => $id
        ));
        return $tactic_id;
    }

    public function getTactics() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $sql = 'SELECT * FROM tactics';
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
