<?php

class Application_Model_DbTable_Userconfiguration extends Zend_Db_Table_Abstract {

    protected $_name = 'user_configuration';

    public function update($id, $initial_budget) {
        #, $maxium_transfer_allowed, $email_days_confirm
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array('initial_budget' => $initial_budget);
        #, 'max_transfer_allowed' => $maxium_transfer_allowed, 
        #'email_days_confirm' => $email_days_confirm
        $where = array('id = ?' => $id);
        $update_status = $db->update($this->_name, $data, $where);
        return $update_status;
    }

    public function getUserConfiguration() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        $sql = 'SELECT * FROM user_configuration';
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
