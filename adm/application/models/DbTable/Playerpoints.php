<?php

class Application_Model_DbTable_Playerpoints extends Zend_Db_Table_Abstract {

    protected $_name = 'player_points';

    public function getmatchs($fecha) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
 
        $sql = 'SELECT  distinct match_id FROM player_points where fecha = ' . $fecha . ' && id_campeonato = ' .ID_CAMPEONATO;
 
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

    public function getPlayersTeam($id, $match_id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT pp.id, pp.match_id, pp.player_id, pp.calif, pp.best_player, '
                . 'p.nickname, p.rol_id '
                . 'FROM player_total_points pp '
                . 'INNER JOIN player p on pp.player_id = p.id '
                . 'where p.team_df_id = ' . $id . ' && '
                . 'pp.computed != -99 && '
                . 'pp.match_id = ' .$match_id . ' && '
                . 'id_campeonato = ' . ID_CAMPEONATO 
                . ' order by p.rol_id asc';
        #print_r($sql);exit();
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
