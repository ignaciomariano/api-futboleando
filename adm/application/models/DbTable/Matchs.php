<?php

class Application_Model_DbTable_Matchs extends Zend_Db_Table_Abstract {

    protected $_name = 'matchs';

    public function get($round) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT m.id as id, m.home_team_id, m.away_team_id, '
                . 'tdf.name as home, tdf2.name as away'
                . ' FROM matchs as m '
                . 'inner join team_df as tdf on tdf.id = m.home_team_id '
                . 'inner join team_df as tdf2 on tdf2.id = m.away_team_id'
                . ' where round_id = ' . $round;
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

    public function getMatch($id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT name FROM team_df where id = ' . $id;
        $stmt = $db->query($sql);
        $rows = $stmt->fetchObject();
        if ($rows != false) {
            return $rows->name;
        } else {
            return 0;
        }
    }

}
