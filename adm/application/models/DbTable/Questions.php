<?php

class Application_Model_DbTable_Questions extends Zend_Db_Table_Abstract {

    protected $_name = 'questions';

    public function addQ($question_id, $level, $country_id, $lang, $cat, $question, $user_id, $square = null, $rectangular = null) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "INSERT INTO questions VALUES (null, $level, $cat, '$country_id',";
        $sql .= empty($square) ? 'null,' : "FROM_UNIXTIME($square),";
        $sql .= empty($rectangular) ? 'null,' : "FROM_UNIXTIME($rectangular), ";
        $sql .= empty($user_id) ? 'null);' : "$user_id);";
        #print_r($sql);exit;
        $desc = $sql;
        $db->query($sql);
        $id = $db->lastInsertId();
        $sql = "INSERT INTO question_lang VALUES (null, $id, '$question', '$lang');";
        $desc.=$sql;
        #die($desc);
        $db->query($sql);
        return $id;
    }

    public function addA($answer, $correct, $question_id, $lang) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "INSERT INTO answers VALUES (null, $correct, $question_id, null);";
        $desc = $sql;
        $db->query($sql);
        $id = $db->lastInsertId();

        $sql = "INSERT INTO answer_lang VALUES (null, $id, '$answer', '$lang');";
        $desc.=$sql;
#       die($desc);
        $db->query($sql);
    }

    public function cleanBase() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "DELETE FROM answers WHERE question_id NOT IN (select question_id from questions);";
        $db->query($sql);
        $sql = "DELETE FROM answer_lang WHERE answer_id NOT IN (select answer_id from answers);";
        $db->query($sql);
        $sql = "DELETE FROM question_lang WHERE question_id NOT IN (select question_id from questions);";
        $db->query($sql);
    }

    public function deleteQ($question_id) {
        if ($question_id) {

            $db = Zend_Db_Table_Abstract::getDefaultAdapter();

            $db->beginTransaction();

            try {
                $sql = "DELETE answer_lang FROM answer_lang INNER JOIN answers USING (answer_id) WHERE question_id = $question_id;";
                $db->query($sql);

                $sql = "DELETE FROM answers WHERE question_id = $question_id;";
                $db->query($sql);

                $sql = "DELETE FROM question_lang WHERE question_id = $question_id;";
                $db->query($sql);

                $sql = "DELETE FROM questions WHERE question_id = $question_id;";
                $db->query($sql);

                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                print_r($e);
            }

            return true;
        }
        return false;
    }

    public function updateQ($question_id, $level, $country_id, $cat) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update questions set level = $level,category_id = $cat, country_id = '$country_id' WHERE question_id = $question_id;";
        # die($sql);
        $db->query($sql);
    }

    public function updateQL($question, $qlid) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update question_lang set question = ? WHERE id = ?;";
        #  die($sql);
        $stm = $db->prepare($sql);
        $stm->execute(array($question, $qlid));
    }

    public function updateAL($answer, $alid) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = " update answer_lang set answer = ? WHERE id = ?;";
        $stm = $db->prepare($sql);
        $stm->execute(array($answer, $alid));
    }

    public function getall($type = "query", $page = PAGE_DEFAULT, $cant = CANT_DEFAULT, $lang = null, $country = null, $category = null, $level = null, $search = null) {
        $filter_lang = $filter_category = $filter_country = $filter_level = $filter_search = "";

        if (!empty($search)) {
            $filter_search = " AND question like '%$search%' ";
        }
        #filters
        if (isset($lang)) {
            $filter_lang = ($lang != "*") ? " AND lang1 = '$lang' AND lang2 = '$lang' " : "";
        }
        if (isset($country)) {
            $filter_country = ($country != "*") ? " AND country_id like '%$country%' " : "";
        }
        if (isset($category)) {
            $filter_category = ($category != "*") ? " AND category_id = $category " : "";
        }
        if (isset($level)) {
            $filter_level = ($level != "*") ? " AND level = '$level' " : "";
        }
        $WHERE = " WHERE question_id > 0 $filter_lang $filter_country $filter_category $filter_level $filter_search";

        #
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $cant = $cant * CANTIDAD_RESPUESTAS;
        if ($type == 'count') {

            $sql = "SELECT COUNT(question_id) as count FROM admQuestList $WHERE;";
            $stmt = $db->query($sql);
            $rows = $stmt->fetch();
            $cant = 0;
            if (isset($rows['count'])) {
                $cant = intval(intval($rows['count']) / 4);
            }
            return intval($cant);
        }
        $limit1 = ($page - 1) * $cant;
        $sql = "SELECT *, UNIX_TIMESTAMP(squared_image_at) AS squared_image_id, UNIX_TIMESTAMP(rectangular_image_at) AS rectangular_image_id FROM admQuestList $WHERE ORDER BY question_id DESC, answer_id ASC LIMIT " . $limit1 . ", " . $cant . ";";

        # die($sql);
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        return $rows;
    }

    public function getqlid($qlid) {

        # 
        $WHERE = " WHERE ql_id = $qlid";
        #
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();

        $sql = "SELECT COUNT(question_id) as count FROM admQuestList $WHERE;";
        $stmt = $db->query($sql);
        $rows = $stmt->fetch();
        $cant = CANTIDAD_RESPUESTAS;
        $sql = "SELECT  *, UNIX_TIMESTAMP(squared_image_at) AS squared_image_id, UNIX_TIMESTAMP(rectangular_image_at) AS rectangular_image_id FROM admQuestList $WHERE ORDER BY question_id ASC, is_correct DESC, answer_id ASC LIMIT   $cant ;";
#die($sql);
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        return $rows;
    }

}
