<?php

class Application_Model_DbTable_Playertotalpoints extends Zend_Db_Table_Abstract {

    protected $_name = 'player_total_points';

    public function update($id, $calif, $figura) {

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        /* 'plus_captain' => $capitan, */
        $data = array('calif' => $calif, 'best_player' => $figura);
        $where = array('id = ?' => $id);
        $update_status = $db->update($this->_name, $data, $where);
        return $update_status;
    }

    public function getFechasProcesadas() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
 
        $sql = 'SELECT distinct r.number, r.id, pts.id_campeonato '
                . 'FROM round r '
                . 'inner join player_total_points pts '
                . 'on pts.id_campeonato = r.id_campeonato AND pts.fecha = r.number '
                . 'WHERE pts.id_campeonato = ' . ID_CAMPEONATO . ' '
                . 'ORDER BY r.id ASC;';
 
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
 
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
