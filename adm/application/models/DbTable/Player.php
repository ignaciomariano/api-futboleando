<?php

class Application_Model_DbTable_Player extends Zend_Db_Table_Abstract {

    protected $_name = 'player';

    public function update($id, $value, $status) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array('value' => $value, 'status' => $status);
        $where = array('id = ?' => $id);
        $update_status = $db->update($this->_name, $data, $where);
        return $update_status;
    }

    public function getPlayers() {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT * FROM player';
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

    public function getPlayersTeam($id) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);
        $sql = 'SELECT * FROM player where team_df_id = ' . $id;
        $stmt = $db->query($sql);
        $rows = $stmt->fetchAll();
        if ($rows != false) {
            return $rows;
        } else {
            return 0;
        }
    }

}
