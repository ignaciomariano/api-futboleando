<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Upload extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->setName('contact_us');

        $title = new Zend_Form_Element_Select('title');
        $title->setLabel('Title')
                ->setMultiOptions(array('mr' => 'Mr', 'mrs' => 'Mrs'))
                ->setRequired(true)->addValidator('NotEmpty', true);

        $firstName = new Zend_Form_Element_Text('firstName');
        $firstName->setLabel('First name')
                ->setRequired(true)
                ->addValidator('NotEmpty');

        $lastName = new Zend_Form_Element_Text('lastName');
        $lastName->setLabel('Last name')->setRequired(true)
                ->addValidator('NotEmpty');

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email address')
                ->addFilter('StringToLower')
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->addValidator('EmailAddress');


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Contact us');

        $this->addElements(array($title, $firstName,
            $lastName, $email, $submit));
    }

}
