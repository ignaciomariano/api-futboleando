<?php

class Pmovil_Db_Profiler_Log extends Zend_Db_Profiler {

/**
 * Zend_Log instance
 * @var Zend_Log
 */
protected $_log;

/**
 * counter of the total elapsed time
 * @var double 
 */
protected $_totalElapsedTime;


        public function __construct($enabled = false) {
            parent::__construct($enabled);
	    $this->_log = Zend_Registry::get("log");
	}

	/**
	 * Intercept the query end and log the profiling data.
	 *
	 * @param  integer $queryId
	 * @throws Zend_Db_Profiler_Exception
	 * @return void
	 */
	public function queryEnd($queryId) {
	    $state = parent::queryEnd($queryId);
	    if (!$this->getEnabled() || $state == self::IGNORED || empty($this->_log)) {
	        return;
	    }
	
	    // get profile of the current query
	    $profile = $this->getQueryProfile($queryId);
	
	
	
	        // update totalElapsedTime counter
	        $this->_totalElapsedTime += $profile->getElapsedSecs();
	
	        // create the message to be logged
	        $sql = $profile->getQuery();
	        $sql = str_replace("\n", ' ', $sql);
	        $sql = str_replace("\t", ' ', $sql);
	        $sql = trim($sql);
	        $sql = preg_replace("/\s+/", " ", $sql);
	         
	        $message = "SQL: ".$sql." - ".json_encode($profile->getQueryParams())." - ".$this->_totalElapsedTime;
	
	        // log the message as INFO message
	        $this->_log->info($message);
	
	}

}

