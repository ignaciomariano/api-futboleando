function movein(which,html) {
    which.style.background='#4DD734'
}
function moveout(which){
    which.style.background='#F8FAF5'
}
function get(id){
    return document.getElementById(id);
}

function redirect(url,div,params){
    var p= "";
    if (get(div))
    { 
        var show =  get(div); 
        $.ajax(
        {
			
            type:"POST",
            url:url,
            data: p,
            success: function(result)
            {
                show.innerHTML = result;
            }
        }); 
    }
}

function nuevoAjax()
{ 
    var xmlhttp=false; 
    try 
    { 
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
    }
    catch(e)
    { 
        try
        { 
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
        } 
        catch(E) {
            xmlhttp=false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp=new XMLHttpRequest();
    } 
    return xmlhttp; 
}

function abrir(div1,div2,web,params,itemmenu)
{  
    if ((g(div1)==null)||(g(div2)==null))
    {
        alert("No existe div");
        return;
    }	
    var ajax = nuevoAjax();
    ajax.open("POST", web, true);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send(params);
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        { 
            // if (web=='php/jugando.php') alert(ajax.responseText);
            switch (ajax.responseText.substr(0,2))
            {
                case "NO":
                    g(div2).innerHTML = "Sorry, had a problem"; 
                    break;
                case "US":
                    g(div2).innerHTML = "Please login or sign up";
                    break;
                case "OK":
                    // alert(g(div1).id);
                    // alert(ajax.responseText.substr(2,ajax.responseText.length-2));
                    g(div1).innerHTML = ajax.responseText.substr(2,ajax.responseText.length-2);
                    break;
                case "MS":
                    g(div2).innerHTML = ajax.responseText.substr(2,ajax.responseText.length-2);
                    break;
                default:
                {
                    g(div2).innerHTML = "<span class='chiquito'>No se puede acceder al servidor. Intente mas tarde.</span>";
						 
                    break;
					
                }
				
            };//end swithc
            /*
            if (web == 'secciones/fotos.php')
                $(function() {
                    $('#gallery a').lightBox();
                });
            if (web == 'secciones/comodidades.php')
                $(function() {
                    $('#gallery a').lightBox();
                });
            */
        }//end readyState
    }//end readyStateChange
}//end funct 

function suscribirse(){
    var parametros = "";
    var email= get("email");
    if(!comprobarEmail2(email.value)){
        alert("Ingresa un email valido");
        return 0;
    }
    parametros+="p0="+email.value;
    $.ajax({
        type:"POST",
        url:"php/nuevo_email.php",
        data: parametros,
        success: function(result)
        {
            if(result){
                alert("Gracias por suscribirte. Recibiras un email de confirmacion.");
                email.value = "";
            }else{
                alert("Ya estas suscripto.");
                email.value = "";
            }
        }
    })
    return 1;
}

function limpiarErrores(){
    get("error-nombre").innerHTML = "";
    get("error-apellido").innerHTML = "";
    get("error-email").innerHTML = "";
    get("error-claves").innerHTML = "";
}
	

function comprobarNumero(str,diverror){
    var myRegxp = /^[0-9]+$/; //solo numeros
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(2,diverror);
    else
        mostrarError(0,diverror);
}

function acceptNum(evt){ 
    var nav4 = window.Event ? true : false;
    // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57 
    var key = nav4 ? evt.which : evt.keyCode; 
    return (key <= 13 || (key >= 48 && key <= 57));
} 

function acceptLetras(evt){ 
    var nav4 = window.Event ? true : false;
    // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57 
    var key = nav4 ? evt.which : evt.keyCode; 
    return ((key >= 65 && key <= 90)|| key == 8);
}

function mostrarError(codigoerror,diverror){
    var errores = new Array (   "",
                                "Usa solo letras.",
                                "Usa solo numeros.",
                                "Usa el formato nombre@ejemplo.com.",
                                "Ingresa al menos 2 caracteres.",
                                "Ingresa al menos 6 caracteres.",
                                "Las claves son diferentes",
                                "El campo esta vacio");
    get(diverror).innerHTML = errores[codigoerror];
}
function comprobarTexto(str,diverror){
    var myRegxp = /^[a-zA-Z\s]+$/; //solo letras
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(1,diverror);
    else
        mostrarError(0,diverror);
}
function comprobarClave(diverror){
    var clave = (get("clave"))?get("clave").value:0;
    var repiteclave = (get("repiteclave"))?get("repiteclave").value:0;
    if( clave != repiteclave)
        mostrarError(6,diverror);
    else
        mostrarError(0,diverror);
}
function comprobarEmail(str,diverror){
    var myRegxp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; //comprobar Email
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(3,diverror);
    else
        mostrarError(0,diverror);
}

function comprobarEmail2(str){
    var myRegxp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; //comprobar Email
    if (myRegxp.test(str)==false)
        return 0;
    else
        return 1;
}