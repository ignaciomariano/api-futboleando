#!/bin/bash
set -e
BASEDIR=$(pwd)

echo "Baixando biblioteca de build script..."
wget "http://gitlab.pmovil.net/gitlab/ci-bashlibs/repository/archive.tar.gz?ref=v1.0.1&private_token=rQxCYRAZG2zeW3XAME8b" -O ci-bashlibs.tgz

tar -xzvf ci-bashlibs.tgz

. ci-bashlibs.git/git.sh
. ci-bashlibs.git/rpm.sh

rpm_create_folders

git_set_branch

if git_is_tagged_with_semver
then
    echo "Buscando config.php de producao do S3..."
    CONFIG_TIMESTAMP=`aws s3api get-object --bucket ci-configs --key futboleando/config.php build/SOURCES/config.php --profile ci-client | grep LastModified | sed -r 's/.*LastModified".*"(.*)".*/\1/' | sed 's/T/ /'`
    if [[ -z "$CONFIG_TIMESTAMP" ]]
    then
        echo "Problemas resgatando a configuracao de producao, checar permissoes"
        echo "(para configurar pemissoes, executar: aws configure --profile ci-client)"
        exit 1
    fi
    RELEASE_POSTFIX='.'
    #RELEASE_POSTFIX+=`date -d "$CONFIG_TIMESTAMP" +%s`
    RELEASE_POSTFIX+=`date +%s`
else
    cp config.php.default build/SOURCES/config.php
    RELEASE_POSTFIX=''
fi

rpm_set_release_and_version $RELEASE_POSTFIX

cp -v specs/* build/SPECS
rpm_changelog 'build/SPECS/futboleando-backend.spec'

#montando diretorio de SOURCE
echo "Syncando codigos fonte para a geracao do RPM..."
echo "adm..."
rsync -a --exclude '*/logs/*' --exclude '*/.git*' --exclude '*/vendor/*' --exclude '*composer.lock' adm build/SOURCES/
echo "adm2..."
rsync -a --exclude '*/logs/*' --exclude '*/.git*' --exclude '*/vendor/*' --exclude '*composer.lock' adm2 build/SOURCES/
echo "api..."
rsync -a --exclude '*/logs/*' --exclude '*/.git*' --exclude '*/vendor/*' --exclude '*composer.lock' api build/SOURCES/
echo "media-converter..."
rsync -a --exclude '*/logs/*' --exclude '*/.git*' --exclude '*/vendor/*' --exclude '*composer.lock' media-converter build/SOURCES/

echo "Instalando dependencias via composer..."
echo "adm..."
cd build/SOURCES/adm
composer remove --ignore-platform-reqs --update-no-dev --no-update --no-progress bombayworks/zendframework1
composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --prefer-dist --ignore-platform-reqs --optimize-autoloader
cd $BASEDIR

echo "adm2..."
cd build/SOURCES/adm2
composer remove --ignore-platform-reqs --update-no-dev --no-update --no-progress bombayworks/zendframework1
composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --prefer-dist --ignore-platform-reqs --optimize-autoloader
cd $BASEDIR

echo "api..."
cd build/SOURCES/api
composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --prefer-dist --ignore-platform-reqs --optimize-autoloader
cd $BASEDIR

echo "media-converter..."
cd build/SOURCES/media-converter
composer remove --ignore-platform-reqs --update-no-dev --no-update --no-progress aws/aws-sdk-php
composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --prefer-dist --ignore-platform-reqs --optimize-autoloader
cd $BASEDIR

#gerando o RPM
echo "Gerando o RPM ..."
cd build
rpmbuild -bb --define "_topdir ${PWD}" --define "version $VERSION" --define "release $RELEASE" SPECS/futboleando-backend.spec 
cd $BASEDIR

echo "Syncando para repositorio"
rpm_sync_repo
