<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    
    public function initialize(){
        $za = $this->session->get('Zend_Auth');
        $user = AdminUser::findFirst($za['storage']->id);
        $this->view->logged_user = $user;
    }
}
