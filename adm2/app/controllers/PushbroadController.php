<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;

class PushbroadController extends ControllerBase 
{

    /**
     * Searches for push_broad
     */
    public function indexAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "PushBroad", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        $parameters["order"] = "push_broad_id";

        $push_broad = PushBroad::find($parameters);
        //print_r($push_broad); exit();
        
        $paginator = new Paginator(array(
            "data" => $push_broad,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a push_broad
     *
     * @param string $id_push_broad
     * 
     * TODO: Refazer
     */
    public function editAction($id_push_broad)
    {
        //echo $id_push_broad;
        if (!$this->request->isPost()) {

            $push_broad = PushBroad::findFirst($id_push_broad);
            if (!$push_broad) {
                $this->flash->error("push_broad was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "push_broad",
                    "action" => "index"
                ));
            }
            
            $fields_cron = explode(" ", $push_broad->cron);
            $extras = unserialize($push_broad->extras);
            
            $week_days = explode(",", $fields_cron[4]);
            foreach($push_broad->PushBroadCountry as $pbc) {
                $push_broad_country[$pbc->country_id] = $pbc->push_message;
            }
            
            $this->view->push_broad_id = $push_broad->push_broad_id;
            $this->view->week_days = $week_days;
            $this->view->platform = explode(",", $extras->platform);
            $this->view->pushBroadCountry = $push_broad_country;
            $this->view->push_user_source_id = $push_broad->push_user_source_id;

            $this->tag->setDefault("id_push_broad", $push_broad->push_broad_id);
            $this->tag->setDefault("push_user_source_id", $push_broad->push_user_source_id); 
            $this->tag->setDefault("minutes", $fields_cron[0]);
            $this->tag->setDefault("hour", $fields_cron[1]);
            $this->tag->setDefault("description", $push_broad->description);
            
            $arr_relac_par_extra = array("2" => "seconds_for_search",
                                         "3" => "seconds_for_search",
                                         "4" => "lives_for_search",
                                         "5" => "lives_for_search",
                                         "6" => "user_id_for_search",
                                         "7" => "coins_for_search",
                                         "8" => "coins_for_search"
                                    );
            if (isset($arr_relac_par_extra[$push_broad->push_user_source_id])) {
                $this->tag->setDefault($arr_relac_par_extra[$push_broad->push_user_source_id], $extras->parameter_extra);
            }

            //$this->tag->setDefault("push_message", $push_broad->push_message);
            //$this->tag->setDefault("extras", $push_broad->extras);
            
        }
    }

    /**
     * Creates a new push_broad
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "pushbroad",
                "action" => "index"
            ));
        }

        $transactionManager = new TxManager();
        $transaction = $transactionManager->get();
        
        $push_broad_id = $this->request->getPost("id_push_broad");
        if ($push_broad_id)
            $push_broad = PushBroad::findFirst($push_broad_id);
        else    
            $push_broad = new PushBroad();
        
        $push_broad->setTransaction($transaction);

        $push_broad->push_user_source_id = $this->request->getPost("push_user_source_id");
        $cron = $this->request->getPost("minutes");
        $cron .= ' '.$this->request->getPost("hour");
        $cron .= ' *';
        $cron .= ' *';
        $cron .= ' '.  implode(',', $this->request->getPost("week_days"));
        
        $push_broad->cron = $cron;
        $push_broad->description = $this->request->getPost("description");
        $push_broad->push_message = $this->request->getPost("push_message");
        
        /*
         * Tratamento do parametro da requisicao "platform"
         */
        $extras = new stdClass();
        if (count($this->request->getPost("platform")) == 3 ) {
            $extras->platform = '*';
        }
        else {
            $extras->platform = implode(',', $this->request->getPost("platform"));
        }
        
        $arr_relac_par_extra = array("seconds_for_search" => "time", "user_id_for_search" =>"user_id", "lives_for_search" => "live", "coins_for_search" => "coin");
        foreach($arr_relac_par_extra as $par_request => $par_extra) {
            $par_temp = $this->request->getPost($par_request);
            if (isset($par_temp)) {
                $par_extra = "parameter_extra";
                $extras->$par_extra = $par_temp;
            }
            unset($par_temp);
        }
        
        $push_broad->extras = serialize($extras);
        //$push_broad->extras = $this->request->getPost("extras");

        if (!$push_broad->save()) {
            foreach ($push_broad->getMessages() as $message) {
                $this->flash->error($message);
            }

            $transaction->rollback();
            return $this->dispatcher->forward(array(
                "controller" => "pushbroad",
                "action" => "new"
            ));
        }
        
        foreach($this->request->getPost("push_message") as $country => $push){
            if (!empty($push)) {
                $push_broad_country = new PushBroadCountry();
                $push_broad_country->setTransaction($transaction);
                $push_broad_country->push_broad_id = $push_broad->push_broad_id;
                $push_broad_country->country_id = $country;
                $push_broad_country->push_message = $push;
                $push_broad_country->save();
                if (!$push_broad_country->save()) {
                    foreach ($push_broad_country->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                    $transaction->rollback();

                    return $this->dispatcher->forward(array(
                        "controller" => "pushbroad",
                        "action" => "new"
                    ));
                }
            }    
        }
        
        
        $transaction->commit();


        $this->flash->success("Broadcast creado con suceso");

        return $this->dispatcher->forward(array(
            "controller" => "pushbroad",
            "action" => "index"
        ));

    }

    /**
     * Saves a push_broad edited
     *
     * TODO: refazer
     * 
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "push_broad",
                "action" => "index"
            ));
        }

        $id_push_broad = $this->request->getPost("id_push_broad");

        $push_broad = PushBroad::findFirstBypush_broad_id($id_push_broad);
        if (!$push_broad) {
            $this->flash->error("push_broad does not exist " . $id_push_broad);

            return $this->dispatcher->forward(array(
                "controller" => "push_broad",
                "action" => "index"
            ));
        }

        $push_broad->push_broad_id = $this->request->getPost("push_broad_id");
        $push_broad->push_user_source_id = $this->request->getPost("push_user_source_id");
        $push_broad->cron = $this->request->getPost("cron");
        $push_broad->description = $this->request->getPost("description");
        $push_broad->push_message = $this->request->getPost("push_message");
        $push_broad->extras = $this->request->getPost("extras");
        

        if (!$push_broad->save()) {

            foreach ($push_broad->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pushbroad",
                "action" => "edit",
                "params" => array($push_broad->id_push_broad)
            ));
        }

        $this->flash->success("push_broad was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pushbroad",
            "action" => "index"
        ));

    }

    /**
     * Deletes a push_broad
     *
     * @param string $push_broad_id
     */
    public function deleteAction($push_broad_id)
    {

        $push_broad = PushBroad::findFirstBypush_broad_id($push_broad_id);
        if (!$push_broad) {
            $this->flash->error("pushbroad was not found");

            return $this->dispatcher->forward(array(
                "controller" => "pushbroad",
                "action" => "index"
            ));
        }

        if (!$push_broad->PushBroadCountry->delete()) {
            foreach ($push_broad->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pushbroad",
                "action" => "search"
            ));
        }
        
        if (!$push_broad->delete()) {

            foreach ($push_broad->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "pushbroad",
                "action" => "search"
            ));
        }

        $this->flash->success("push_broad was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "pushbroad",
            "action" => "index"
        ));
    }

}
