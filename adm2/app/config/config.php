<?php

define('BASE_DIR', realpath(__DIR__ . '/../'));

$config = include BASE_DIR . '/../../config.php';

$config['application'] = array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => '/',
	'schemaCacheTimeout' => $config['api']['schemaCacheTimeout'],
);

$config['models'] = array(
    'metadata' => array(
          'adapter' => 'Apc'
        )
);

$config = new \Phalcon\Config($config);

return $config;
