<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();


if(empty($config->log4php)){
    Logger::configure();
} else {
    Logger::configure($config->log4php->toArray());
}

$logger_services = Logger::getLogger('services');

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    $host_parts = explode('.',$_SERVER['HTTP_HOST']);
    array_shift($host_parts);
    $domain = implode('.', $host_parts);
    $view->setVar('domain', $domain);
    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
        $db = new DbAdapter(array(
            'host' => $config->database->params->host,
            'username' => $config->database->params->username,
            'password' => $config->database->params->password,
            'dbname' => $config->database->params->dbname,
            'options' => $config->database->params->driver_options->toArray()
        ));

        $eventsManager = new Phalcon\Events\Manager();

        $profiler = new Phalcon\Db\Profiler();

        //Listen all the database events
        $eventsManager->attach('db', function($event, $db) use ($profiler) {

            $logger = Logger::getLogger('services');
            LoggerNDC::push("SQL");
            
            if ($event->getType() == 'beforeQuery') {
                $profiler->startProfile($db->getSQLStatement());
                if (!$vars = $db->getSQLVariables()) {
                    $vars = array();
                }
                $sql = preg_replace("/\s+/", ' ', trim($db->getSQLStatement()));
                $logger->debug($sql . ' (' . join(',', $vars) . ')');
            } elseif ($event->getType() == 'afterQuery') {
                $profiler->stopProfile();
                $last = $profiler->getLastProfile();
                $logger->debug('done (' . $last->getTotalElapsedSeconds() . ')');
            } elseif(in_array($event->getType(), array('beginTransaction', 'commitTransaction'))){
                $logger->debug($event->getType());
            }
            
            LoggerNDC::pop();
            
        });
        $db->setEventsManager($eventsManager);

        return $db;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$currentCookieParams = session_get_cookie_params();

session_set_cookie_params (
        "86400", 
        $currentCookieParams["path"], 
        $di->get('view')->domain, 
        $currentCookieParams["secure"],
        $currentCookieParams["httponly"] 
        );

$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});
