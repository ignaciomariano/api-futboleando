<?php

class UserDevice extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_device_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $parse_device_id;

    /**
     *
     * @var string
     */
    public $native_device_id;

    /**
     *
     * @var string
     */
    public $platform;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;



}
