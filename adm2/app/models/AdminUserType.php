<?php


class AdminUserType extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_type;

    /**
     *
     * @var string
     */
    public $type;

    
    public function initialize(){
        $this->hasMany('id_type', 'AdminUser', 'id_type');
    }
 
}
