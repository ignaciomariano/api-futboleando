<?php

class PushBroad extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $push_broad_id;

    /**
     *
     * @var integer
     */
    public $push_user_source_id;

    /**
     *
     * @var string
     */
    public $cron;

    /**
     *
     * @var string
     */
    public $description;


    /**
     *
     * @var string
     */
    public $extras;
    
    
    public function initialize(){
        $this->hasMany('push_broad_id', 'PushBroadCountry', 'push_broad_id');
        $this->belongsTo('push_user_source_id', 'PushUserSource', 'push_user_source_id');
    }

}
