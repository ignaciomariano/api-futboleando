<?php


class Users extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    public $passwd;

    /**
     *
     * @var string
     */
    public $fb_token;

    /**
     *
     * @var string
     */
    public $fb_data;

    /**
     *
     * @var string
     */
    public $created_on;

    /**
     *
     * @var string
     */
    public $phone_data;

    /**
     *
     * @var string
     */
    public $country_id;

    /**
     *
     * @var string
     */
    public $platform;

    /**
     *
     * @var string
     */
    public $device_id;

    /**
     *
     * @var string
     */
    public $gender;

    /**
     *
     * @var integer
     */
    public $avatar;

    /**
     *
     * @var string
     */
    public $whistle_time;

    /**
     *
     * @var integer
     */
    public $whistle_items;

    /**
     *
     * @var integer
     */
    public $coins;

    /**
     *
     * @var integer
     */
    public $show_ended;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $latest_dt;

     
}
