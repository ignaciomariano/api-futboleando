<?php


class AdminUser extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $email;


    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var integer
     */
    public $id_type;

    /**
     *
     * @var integer
     */
    public $status;


    public function initialize(){
        $this->belongsTo('id_type', 'AdminUserType', 'id_type');
    }
 
}
