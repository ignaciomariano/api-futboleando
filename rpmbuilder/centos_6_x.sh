#!/usr/bin/env bash
set -e

yum install -y rpm-build php-cli wget unzip
php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/bin --filename=composer
wget https://s3.amazonaws.com/aws-cli/awscli-bundle.zip
unzip awscli-bundle.zip
./awscli-bundle/install -i /usr/local/aws -b /usr/bin/aws

