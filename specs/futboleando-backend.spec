%define base_dir /var/local/pmovil/httpd/srv/futboleando/
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
Name:				futboleando-backend
Version:			%{version}
Release:			%{release}
Summary:			Futboleando admin and API

Group:				Pmovil/Futboleando
License:			Pmovil
URL:				http://gitlab.pmovil.net/Futboleando/Api-V2
BuildArch:			noarch
Requires:			httpd
Requires:			php-mysql
Requires:			php-pdo
Requires:			php-xml
Requires:			php-pecl(http) >= 1.0.0
Requires:			php-pecl(http) < 2.0.0
Requires:			php-process
Requires:			php-pecl-imagick
Requires:			php-pecl(apcu)
Requires:			php-ZendFramework
Requires:			php-ZendFramework-Cache-Backend-Apc
Requires:			php-ZendFramework-Db-Adapter-Pdo-Mysql
Requires:			php-aws-sdk
Requires:			php-phalcon >= 1.3.0
Requires:			php-pecl-geoip
Requires:			GeoIP
Requires:			GeoIP-update


%description


%prep


%build
mkdir -p root%{base_dir}
rm -Rf root%{base_dir}/*
mv $RPM_SOURCE_DIR/* root%{base_dir}
mkdir -p root%{base_dir}adm/logs
mkdir -p root%{base_dir}api/logs
mkdir -p root%{base_dir}media-converter/logs


%install
rm -rf $RPM_BUILD_ROOT
(cd root   ; find . -depth -print | cpio -dump $RPM_BUILD_ROOT)

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,apache,apache,-)
%{base_dir}*
%attr(775, apache, apache) %{base_dir}api/logs
%config(noreplace) %{base_dir}config.php

%changelog
