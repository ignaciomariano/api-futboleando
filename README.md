# Introduction

API for Futboleando


# Instalation

## General instructions


```bash
mkdir /my/installation/path
cd /my/installation/path
git clone git@gitlab.pmovil.net:futboleando/api-v2.git .
git checkout desiredbranch
```

If your Web Server runs on a distinct user/group than apache, just change it

```bash
mkdir api/logs && sudo chown apache.apache api/logs
mkdir adm/logs && sudo chown apache.apache adm/logs
mkdir media-converter/logs && sudo chown apache.apache media-converter/logs
```

Configure an Apache Virtual Host:

```
<VirtualHost *:80>
    ServerName media-converter.futboleando.yourdomain
    DocumentRoot /my/installation/path/media-converter/
    ErrorDocument 404 /index.php
    ErrorDocument 403 /index.php
    ErrorLog logs/media-converter.futboleando.srv.yourdomain-error_log
    CustomLog logs/media-converter.futboleando.srv.yourdomain-access.log combined
</VirtualHost>
<VirtualHost *:80>
    ServerAdmin yourname@yourdomain
    ServerName futboleando.srv.yourdomain
    ServerAlias *.futboleando.srv.yourdomain
    VirtualDocumentRoot /my/installation/path/futboleando/%1/public
    Options FollowSymLinks
    SetEnv APPLICATION_ENV "development"
    AliasMatch /docs/(.+) /my/installation/path/futboleando/api/public/docson/$1
    <Directory /my/installation/path/futboleando/api/public>
        AddDefaultCharset UTF-8
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^(.*)$ /index.php?_url=/$1 [QSA,L]
    </Directory>
    <Directory /my/installation/path/futboleando/adm/public>
        AddDefaultCharset UTF-8
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} -s [OR]
        RewriteCond %{REQUEST_FILENAME} -l [OR]
        RewriteCond %{REQUEST_FILENAME} -d
        RewriteRule ^.*$ - [NC,L]
        RewriteRule ^.*$ /index.php [NC,L]
    </Directory>
    ErrorLog logs/api.futboleando.yourdomain-error.log
    CustomLog logs/api.futboleando.yourdomain-access.log combined
</VirtualHost>
```

Copy and edit config file:

```bash
cp config.php.default config.php
vim config.php
```


## First time database setup (only on new installations without a preexisting database)

Create a database structure with MySQLWorkbench file at docs/futboleando.mwb using Forward Engineering and "Generate INSERT Statements for Tables" checked.

MySQLWorkbench will create a database user called futboleando with password futboleando.


