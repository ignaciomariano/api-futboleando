<?php

require_once __DIR__.'/vendor/autoload.php';

Logger::configure(null, 'Pmovil2_Log4Php_Configurators_Pmovil');

if(is_file('config.php')){
    include 'config.php';
}

$ignorarPrefixo = '/media';

include __DIR__.'/vendor/phplibs/media-converter/converte_midia.php';
