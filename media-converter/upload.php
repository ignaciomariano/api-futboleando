<?PHP
ini_set('display_errors', 'Off');
ini_set('display_startup_errors', 'Off');

require_once __DIR__.'/vendor/autoload.php';

Logger::configure(null, 'Pmovil2_Log4Php_Configurators_Pmovil');


if(is_file('config.php')){
    include 'config.php';
}

include __DIR__.'/vendor/phplibs/media-converter/recebe_upload.php';
