<?php

define('PARSE_APPLICATION_ID', 'kw2FO4O5YWzmLprK7nYEWZXKIXsQjquQ4Mg7zqMk');
define('PARSE_REST_API_KEY', 'ugZfZU8yfcROClFVQMlq7NKNtApsg0cZZtJOhKzS');
define('ANDROID_API_KEY', 'AIzaSyAkCKj42iWzR4h3kv0m4VoQlFvBmqhYC8g');
define('error_reporting', E_ALL);
define('FILE_LOOP', './push-keep-alive');

require_once __DIR__ . '/push.class.php';

class DbTask extends \Phalcon\CLI\Task {

    private $logger;

    public function __construct() {
        $this->logger = Logger::getLogger('DbMantainmentTask');
    }

    public function updatePointsMatchAction() {
        $matchs = Matchs::find();
        foreach ($matchs as $match) {
            $match->getPointsMatchModel(array('extras' => array('match_start' => $match->created_on)))->delete();
            $matchlogs = $match->getMatchLog('is_correct = 1');
            foreach ($matchlogs as $matchlog) {
                Ranking::addRanking($matchlog->toArray(), false);
                $this->logger->debug("Saved id " . $match->match_id);
            }
        }
    }

    public function updateRankingAction() {
        $tables = PointsMatchModel::getTables();
        Ranking::dropAllSet();
        foreach ($tables as $table) {
            $this->logger->info("Consoliding table $table");
            $matchPoints = PointsMatchModel::find(array(    
                'columns' => 'user_id, (SUM(points_correct) + SUM(points_avatar) + SUM(points_duel)) as points',
                'group' => 'user_id',
                'extras' => array('table' => $table)
            ));

            
            
            foreach ($matchPoints as $matchPoint) {
                $ranking = new \Ranking();
                $ranking->setId($matchPoint->user_id);
                $ranking->incScore($matchPoint->points);
                $ranking->save();
            }
        }
    }
}
