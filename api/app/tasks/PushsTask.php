<?php

define('PARSE_APPLICATION_ID', 'kw2FO4O5YWzmLprK7nYEWZXKIXsQjquQ4Mg7zqMk');
define('PARSE_REST_API_KEY', 'ugZfZU8yfcROClFVQMlq7NKNtApsg0cZZtJOhKzS');
define('ANDROID_API_KEY', 'AIzaSyAkCKj42iWzR4h3kv0m4VoQlFvBmqhYC8g');
define('error_reporting', E_ALL);
define('FILE_LOOP', './push-keep-alive');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire;


class PushsTask extends \Phalcon\CLI\Task {

    private $logger;
    private $config;

    public function __construct() {
        $this->logger = Logger::getLogger('PushsTask');
        $this->config = $this->getDI()->get('config');
    }

    public function sendAction() {
        $match_id = 0;
        $sender = new Push($this->config->pushs->toArray());
        $this->logger->info('Staring Push Notifications...');
        echo "\n" . date('Y-m-d H:i:s', time()) . " :: Start push notification service...";

        $pushs = PushMessages::findPushsToSend();
        
        foreach ($pushs as $push) {
            $id = $push->id;
            $device_id = $push->device_id;
            $msg = $push->alert;
            $platform = strtoupper($push->platform);
            $user_id = intval($push->user_id);

            $enviar = (strlen($device_id) > 9 && !empty($msg)) ? true : false;
            $mode = ($platform == 'ANDROID') ? 'GCM' : 'PARSE';
            $mode = (strlen($device_id) == 10) ? 'PARSE' : $mode;
            $mode = ($platform == 'IOS') ? "IOS" : $mode;
            if (($mode == 'GCM') && (strlen($device_id) < 50)) {
                $enviar = false;
            }
            if ($enviar) {

                $mode = ($platform == 'ANDROID') ? 'GCM' : 'PARSE';
                $mode = (strlen($device_id) == 10) ? 'PARSE' : $mode;
                $mode = ($platform == 'IOS') ? "IOS" : $mode;

                $response = $sender->send($mode, $device_id, $msg);
                $this->logger->info("[$mode:$id:user:$user_id:$device_id:$msg:$platform][response:$response]");
                if ($mode == 'GCM') {
                    $resp_array = json_decode($response);
                    $results = $resp_array->results;
                    foreach ($results as $device_response) {
                        if (!is_array($device_response)) {
                            if ((isset($device_response->error)) && $device_response->error == "NotRegistered") {
                                $this->logger->info("Unregistered user:$user_id");
                                $user = Users::findFirst($user_id);
                                $user->phone_data = 'LOGOUT';
                                $user->gender = 'GCM-LOCK';
                                $user->save();
                            }
                        }
                    }
                }
                $n++;
                $new_status = 'enviado';
            } else {
                $new_status = 'cancelado';
            }

            $push->status = $new_status;
            $push->save();
        }
        $this->logger->info("$n envios terminados.");
        $this->logger->info('Fin de proceso');
        echo "\n" . date('Y-m-d H:i:s', time()) . " :: Fin de proceso";
    }

    public function broadcastAction($params) {
        $MSG['ES'] = 'Futboleando te extraña. Hay nuevas preguntas para ti';
        $MSG['PT'] = 'Futeboleando está com saudades. Tem novas perguntas para vc';
        $lang = $params[0];
        echo "LANG: $lang";
        if ($lang != 'ES' && $lang != 'PT') {
            die('invalid language');
        }
        $match_id = 0;
        $sender = new Push($this->config->pushs->toArray());
        $date = date('Ymd', time());
        $this->logger->info('Staring Push Notifications...');

        $platform = 'ANDROID';
        
        $users = Users::findByInactivity(3600*24, $lang);
        
        $arr_device_id = array();
        $n = 0;
        foreach ($users as $res) {
            $device_id = $res->device_id;
            if(empty($device_id)){
               continue; 
            }
            $lang = ($res->country_id == 'BR') ? 'PT' : 'ES';
            $msg = $MSG[$lang];
            $id = $res->user_id;
            $latest_dt = $res->latest_dt;

            $mode = ($platform == 'ANDROID') ? 'GCM-BROADCAST' : 'PARSE';
            $mode = (strlen($device_id) == 10) ? 'PARSE' : $mode;
            $mode = ($platform == 'IOS') ? "IOS" : $mode;

            if ($mode == 'GCM-BROADCAST') {
                $arr_device_id[] = $device_id;
                if (count($arr_device_id) < 250) {
                    $enviar = false;
                    echo ".";
                    $this->logger->info("[$mode:$id:$latest_dt:sumando:" . $device_id . ":$msg:$platform]");
                } else {
                    $response = $sender->send($mode, $arr_device_id, $msg);
                    $this->logger->info("[$mode:$id:$latest_dt:enviando_array:" . var_export($arr_device_id, true) . ":$msg:$platform][response:$response]");
                    $n++;
                    unset($arr_device_id);
                    $arr_device_id = array();
                }
            } elseif ($mode == 'PARSE') {
                #send unique ia parse
                $response = $sender->send($mode, $device_id, $msg);
                $this->logger->info("[$mode:$id:$latest_dt:enviando parse:" . $device_id . ":$msg:$platform][response:$response]");
            } elseif ($mode == 'IOS') {
                $response = $sender->send($mode, $device_id, $msg);
                $this->logger->info("[$mode:$id:$latest_dt:enviando ios:" . $device_id . ":$msg:$platform][response:$response]");
            }
        }

        # Enviar cola sin enviar de android.
        $mode = 'GCM-BROADCAST';
        if (count($arr_device_id) > 0) {
            $response = $sender->send($mode, $arr_device_id, $msg);
            $this->logger->info("[$mode:$id:$latest_dt:enviando:" . var_export($arr_device_id, true) . ":$msg:$platform][response:$response]");
        }

        $this->logger->info('No existe file for loop. Cerrando archivo');
    }

    #Pedido Fabian 29/10/14 - Ignacio Gonzalez

    public function broadcastmatchspendientesAction($params) {
        $lang = $params[0];
        echo "LANG: $lang \n";
        if ($lang != 'ES' && $lang != 'PT') {
            die('invalid language');
        }
        $sender = new Push($this->config->pushs->toArray());
        $this->logger->info('Staring Push Notifications...');


        $platform = 'ANDROID';

        $users = Users::findByPendingMatch();

        
        $arr_device_id = array();
        $n = 0;

        $deviceTokens = array();
        $deviceTokensTOTAL = array();

        foreach ($users as $res) {

            $deviceTokensTOTAL[] = $res->device_id;
            if (in_array($res->device_id, $deviceTokens)) {
                
            } else {
                if (empty($res->fb_data)) {
                    $name = $res->username;
                } else {
                    $fb_data = explode(",", $res->fb_data);
                    $name = $fb_data['2'];
                }

                $MSG['ES'] = "$name tienes un Duelo Pendiente";
                $MSG['PT'] = "$name tienes un Duelo Pendiente";
                $deviceTokens[] = $res->device_id;
                $device_id = $res->device_id;
                $lang = ($res->country_id == 'BR') ? 'PT' : 'ES';
                $msg = $MSG[$lang];
                $id = $res->user_id;
                $latest_dt = $res->latest_dt;
                $platform = $res->platform;

                switch ($platform) {
                    case 'ANDROID':
                        $mode = 'GCM-BROADCAST';
                        break;
                    case 'ios':
                        $mode = 'IOS';
                        break;
                    case 'androidp':
                        $mode = 'PARSE';
                        break;
                    case 'ios_parse':
                        $mode = 'PARSE';
                        break;
                    case 'windows8p':
                        $mode = 'PARSE';
                        break;
                }


                if ($mode == 'GCM-BROADCAST') {
                    $arr_device_id[] = $device_id;
                    if (count($arr_device_id) < 250) {
                        $enviar = false;
                        echo ".";
                        $this->logger->info("[$mode:$id:$latest_dt::sumando:" . $device_id . ":$msg:$platform]");
                    } else {
                        $response = $sender->send($mode, $arr_device_id, $msg);
                        $this->logger->info("[$mode:$id:$latest_dt::enviando_array:" . var_export($arr_device_id, true) . ":$msg:$platform][response:$response]");
                        $n++;
                        unset($arr_device_id);
                        $arr_device_id = array();
                    }
                } elseif ($mode == 'PARSE') {
                    #send unique ia parse
                    $response = $sender->send($mode, $device_id, $msg);
                    $this->logger->info("[$mode:$id:$latest_dt::enviando parse:" . $device_id . ":$msg:$platform][response:$response]");
                } elseif ($mode == 'IOS') {
                    $response = $sender->send($mode, $device_id, $msg);
                    $this->logger->info("[$mode:$id:$latest_dt::enviando ios:" . $device_id . ":$msg:$platform][response:$response]");
                }
            }
        }

        echo "\n count CON duplicados " . count($deviceTokensTOTAL) . "\n";
        #print_r($deviceTokensTOTAL);
        echo "\n count SIN duplicados " . count($deviceTokens) . "\n";
        #print_r($deviceTokens);
        #
        # Enviar cola sin enviar de android.
        $mode = 'GCM-BROADCAST';
        if (count($arr_device_id) > 0) {
            $response = $sender->send($mode, $arr_device_id, $msg);
            $this->logger->info("[$mode:$id:$latest_dt::enviando:" . var_export($arr_device_id, true) . ":$msg:$platform][response:$response]");
        }

        $this->logger->info('No existe file for loop. Cerrando archivo');
    }

    public function unenvioAction($params) {

        $push = PushMessages::findFirst(7433256);
        if ($push) {
            #$MSG['ES'] = iconv(mb_detect_enconding($res['alert']), 'utf-8',$res['alert']);
            $MSG['ES'] = $push->alert;
        }

        #$MSG['ES'] = 'Ignacio González';
        $MSG['PT'] = 'Futeboleando está com saudades da sua paixão. Jogue agora';
        $lang = $params[0];
        echo "LANG: $lang";
        if ($lang != 'ES' && $lang != 'PT') {
            die('invalid language');
        }
        $apple = new ApplePush();
        $match_id = 0;
        $sender = new Push($this->config->pushs->toArray());
        $date = date('Ymd', time());
        $this->logger->info('Staring Push Notifications...');

        $device_id = "JRvqUc52RP";
        $lang = 'ES';
        $msg = $MSG[$lang];
        $id = 123;
        $now = date("Y-m-d H:i:s");
        $mode = 'PARSE';
        $platform = 'ANDROID';



        if ($mode == 'GCM-BROADCAST') {
            $arr_device_id[] = $device_id;
            if (count($arr_device_id) < 250) {
                $enviar = false;
                echo ".";
                $this->logger->info("[$mode:$id:$latest_dt:$now:sumando:" . $device_id . ":$msg:$platform]");
            } else {
                $response = $sender->send($mode, $arr_device_id, $msg);
                $this->logger->info("[$mode:$id:$latest_dt:$now:enviando_array:" . var_export($arr_device_id, true) . ":$msg:$platform][response:$response]");
                $n++;
                unset($arr_device_id);
                $arr_device_id = array();
            }
        } elseif ($mode == 'PARSE') {
            #send unique ia parse
            $response = $sender->send($mode, $device_id, $msg);
            $this->logger->info("[$mode:$id:$now:enviando parse:" . $device_id . ":$msg:$platform][response:$response]");
        } elseif ($mode == 'IOS') {
            $response = $sender->send($mode, $device_id, $msg);
            $this->logger->info("[$mode:$id:$now:enviando ios:" . $device_id . ":$msg:$platform][response:$response]");
        }
# Enviar cola sin enviar de android.
        #$mode = 'GCM-BROADCAST';
        #if (count($arr_device_id) > 0) {
        #    $response = $sender->send($mode, $arr_device_id, $msg);
        #    $this->logger->info("[$mode:$id:$latest_dt:$now:enviando:" . var_export($arr_device_id, true) . ":$msg:$platform][response:$response]");
        #}
        #$this->logger->info('No existe file for loop. Cerrando archivo');
    }

    
    public function migrateDeviceDatabaseAction(){
        
        
        ini_set("memory_limit","1024M");        
        
        $users = Users::find(array(
           "phone_data != 'LOGOUT'"
        ));
        
        $sender = new Push($this->config->pushs->toArray());        
        
        foreach ($users as $user){
            $parse = false;
            if($user->platform && !empty($user->device_id)){
                //Default
                $device = null;
                $platform = 'ANDROID';
                $user->platform = strtoupper($user->platform);
                switch ($user->platform){
                    case 'ANDROID':
                        $device = UserDevice::findFirst("native_device_id = '$user->device_id'");
                        $platform = 'ANDROID';
                        break;
                    case 'IOS':
                        $device = UserDevice::findFirst("native_device_id = '$user->device_id'");
                        $platform = 'IOS';
                        break;
                    case 'ANDROIDP':
                        $device = UserDevice::findFirst("parse_device_id = '$user->device_id'");
                        $platform = 'ANDROID';
                        $parse = true;
                        break;
                    case 'IOS_PARSE':
                        $device = UserDevice::findFirst("parse_device_id = '$user->device_id'");
                        $platform = 'IOS';
                        $parse = true;
                        break;
                    case 'WINDOWS8P':
                        $device = UserDevice::findFirst("parse_device_id = '$user->device_id'");
                        $platform = 'WINDOWS8';
                        $parse = true;
                        break;
                }
                if($device){
                    //Ignorar devices ja registrados
                    
                } else {
                    $device = new UserDevice();
                    $device->user_id = $user->user_id;
                    if($parse){
                        $device->parse_device_id = $user->device_id;
                        $device->native_device_id = $sender->nativeDeviceId($device->parse_device_id);
                    } else {
                        $device->native_device_id = $user->device_id;
                    }
                    $device->platform = $platform;
                    
                    if($device->native_device_id !== false){
                        $device->save();
                    }
                }
            }
        }
    }
    
    public function crontabAction() {
        $this->logger = Logger::getLogger('PushsTaskCrontabAction');

        $this->logger->info('Running crontab action from PushTasks');
        
        $broads = PushBroad::find();
        $this->logger->info('Found '. count($broads) . ' cron jobs');
        
        foreach($broads as $broad){
            
            $funcao = $broad->PushUserSource->function;

            $ce = Cron\CronExpression::factory($broad->cron);
            if($ce->isDue()){
                $this->logger->info('CRON JOB: [' . $broad->cron . '] Run date is due!! Running now ...');

                $broad->extras = unserialize($broad->extras);
                
                foreach($broad->PushBroadCountry as $push_per_country){
                    if($push_per_country->push_message){

                        $this->logger->info('Country push: ' . $push_per_country->country_id);
                        
                        $pid = null;
                        if($pid == pcntl_fork()){
                            $this->getDI()->getShared('db')->connect();
                            continue;
                        } else {
                            
                            $this->getDI()->getShared('db')->connect();
                            $this->logger->info("Broadcast process: $push_per_country->country_id - $funcao ( ".$broad->extras->platform.", ".$broad->extras->parameter_extra." ) - $broad->cron");
                            $users = PushBroadUsersList::$funcao($push_per_country->country_id, $broad->extras->platform, $broad->extras->parameter_extra);

                            $connection = new AMQPConnection(
                                        $this->config->rabbitMQ->host,
                                        $this->config->rabbitMQ->port,
                                        $this->config->rabbitMQ->user,
                                        $this->config->rabbitMQ->pass);
                            $channel = $connection->channel();
                            $channel->exchange_declare('topic_notifications', 'topic', false, false, false);                            
                            
                            // Quantidade de devices por lote
                            $amount_part = 5000;
                            $numbers_devices=array('PARSE' => 0, 'ANDROID' => 0, 'IOS' => 0);
                            $control_keys = array();
                            
                            foreach($users as $user){
                                foreach($user->userDevice as $user_device){
//                              foreach($user->getUserDevice() as $user_device){
                                    if($user_device->parse_device_id){
                                        $device_id = $user_device->parse_device_id;
                                        $platform = 'PARSE';
                                    } else {
                                        $device_id = $user_device->native_device_id;
                                        $platform = $user_device->platform;
                                    }

                                    $numbers_devices[$platform]++;
   
                                    $part_number = (int) ($numbers_devices[$platform] / $amount_part) + 1;
   
                                    //Criando o nome do routing key com base na Plataforma / Lote (quantidade de dispositivos)
                                    $routing_key_control = strtolower('create.' . $push_per_country->country_id . "." . $platform . ".lote_" . $part_number);
                                    $routing_key = strtolower($push_per_country->country_id . "." . strtolower($platform) . ".lote_" . $part_number);
                                    
                                    // Publicando as "routing key" para o script de controle criar os scripts consumidores correspondente
                                    if ( !in_array($routing_key_control,$control_keys) ) {

                                        array_push($control_keys, $routing_key_control);
                                        
                                        $msg_control = new AMQPMessage($routing_key_control);
                                        $this->logger->info("rabbitMQ: Publish to control_consumers_notification: ".$routing_key_control);
                                        $channel->basic_publish($msg_control, 'topic_notifications', 'control_consumers_notifications');
                                        sleep(2);
                                    }
                                    
                                    $msg = new AMQPMessage($push_per_country->push_message);
                                    $hdrs=new Wire\AMQPTable(array(
                                        'x-deviceId' => $device_id,
                                        'x-userId' => $user->user_id,
                                        'x-pubTimestamp' => new DateTime()
                                    ));

                                   /* Neste momento não é necessario passar no header os dados abaixo, já que o envio esta sendo realizado
                                    * nesta mesma aplicação, em outro metodo da classe, portanto as configurações de envio se encontram disponíveis para 
                                    * o envio
                                    *  if($platform == 'PARSE'){
                                            $hdrs->set("X-Parse-Application-Id", $this->config->pushs->parse->applicationId);
                                            $hdrs->set("X-Parse-REST-API-Key", $this->config->pushs->parse->RESTAPIKey);
                                    } elseif($platform == 'ANDROID'){
                                            $hdrs->set("Authorization", $this->config->pushs->android->apiKey);
                                    } */ 

                                   $msg->set('application_headers', $hdrs);
                                   $this->logger->info("rabbitMQ: Publish to $routing_key (".$numbers_devices[$platform]."): ".$push_per_country->push_message." ($device_id)");
                                   $channel->basic_publish($msg, 'topic_notifications', $routing_key);
                                }
                            }
                            
                            $channel->close();
                            $connection->close();
                            exit();
                        }
                    }
                }
                $this->logger->info("End of execution!");
            } else {           
                $this->logger->info('CRON JOB: [' . $broad->cron . '] Not the due date to run.');     
            }
        }
    }
    
    public function consumerControlAction() {
        $this->logger = Logger::getLogger('PushsTaskConsumerControlAction');
        // Array de verificacao, para evitar correr mais de uma vez o mesmo processo consumidor
        $arr_control_process_created = array();

        $connection = new AMQPConnection(
                            $this->config->rabbitMQ->host,
                            $this->config->rabbitMQ->port,
                            $this->config->rabbitMQ->user,
                            $this->config->rabbitMQ->pass);
        
        $channel = $connection->channel();

        $channel->exchange_declare('topic_notifications', 'topic', false, false, false);

        list($queue_name, ,) = $channel->queue_declare("", false, false, true, false);

        $channel->queue_bind($queue_name, 'topic_notifications', 'control_consumers_notifications');
        $this->logger->info("[*] Waiting for messages from control_consumers_notifications...");
        //echo '[*] Waiting for messages. To exit press CTRL+C', "\n";
        $GLOBALS['arr_control_process_created'] = array();
        
        $callback = function($msg) {
            $logger = Logger::getLogger('PushsTaskConsumerControlAction');
            $logger->info("Message received: ".$msg->body);
            preg_match("/^(.+)\.(.+)\.(.+)\.(.+)/", $msg->body, $re);
            $command = $re[1];
            $country_id = $re[2];
            $plataform = $re[3];
            $part = $re[4];
            $id_consumer = $country_id.".".$plataform.".".$part;
            //$this->logger->info("$command , $country_id , $plataform , $part");
            switch ($command) {
                case 'create':
                    $logger->info("Creating consumer ...");
                    //$this->logger->info("arr_control_process_created =>".print_r($GLOBALS['arr_control_process_created'], 1));
                    if ( !in_array($id_consumer, $GLOBALS['arr_control_process_created']) ) {
                        $consumer_cli = "php " . BASE_DIR . "/cli.php pushs consumer $id_consumer";
                        $command_shell = "nohup $consumer_cli > /dev/null & echo $!";
                        $logger->info("Command shell: $command_shell");
                        $PID = shell_exec($command_shell);
                        $PID = (int) trim($PID);
                        $logger->info("Consumer $id_consumer created! PID: $PID");
                        $GLOBALS['arr_control_process_created'][$PID] = $id_consumer;
                    }
                    else {
                        $logger->info("Consumer $id_consumer is already running. No new consumer creates.");
                        //echo "Consumer para $id_consumer ja esta em execucao.";
                    }
                    break;
                case 'kill':
                    if (in_array($id_consumer, $GLOBALS['arr_control_process_created'])) {
                        $PID = array_search($id_consumer, $GLOBALS['arr_control_process_created']);
                        exec("kill -KILL $PID");
                        unset($GLOBALS['arr_control_process_created'][$PID]);
                    }
                    break;
            }
        };

        $channel->basic_consume($queue_name, '', false, true, false, false, $callback);

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
        
    }
    
    public function consumerAction(array $params) {
        $this->logger = Logger::getLogger('PushsTaskConsumerAction');
        
        $id_consumer = $params[0];
        
        $connection = new AMQPConnection(
                            $this->config->rabbitMQ->host,
                            $this->config->rabbitMQ->port,
                            $this->config->rabbitMQ->user,
                            $this->config->rabbitMQ->pass);
        
        $channel = $connection->channel();

        $channel->exchange_declare('topic_notifications', 'topic', false, false, false);

        list($queue_name, ,) = $channel->queue_declare("", false, false, true, false);

        $channel->queue_bind($queue_name, 'topic_notifications', $id_consumer);

        $this->logger->info("[*] Waiting for messages from ".$id_consumer);
        //$logger->info("iiii [*] Waiting for messages. To exit press CTRL+C");
        //echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        $config = $this->config;
        $callback = function($msg) use ($id_consumer, $config) {
            $logger = Logger::getLogger('PushsTaskConsumerAction');
            list($country_id, $platform, $part_name) = explode(".", $id_consumer);
            
            //$this->logger->info($msg->delivery_info['routing_key']."\tMessage received: ".$msg->body);
            //$this->logger->info($msg->delivery_info['routing_key']."\tMessage headers follows: ".print_r($msg->get('application_headers')->getNativeData(), 1));
            $platform = strtoupper($platform);
            $headers_msg = $msg->get('application_headers')->getNativeData();
            $device_id = $headers_msg['x-deviceId'];
            $user_id = $headers_msg['x-userId'];
            $date_source_msg = $headers_msg['x-pubTimestamp']->format('c');
            
            $mode = ($platform == 'ANDROID') ? 'GCM-BROADCAST' : 'PARSE';
            $mode = (strlen($device_id) == 10) ? 'PARSE' : $mode;
            $mode = ($platform == 'IOS') ? "IOS" : $mode;            
            
            $logger->info("Start Push Notification");
            $sender = new Push($config->pushs->toArray());
            $response = $sender->send($mode, $device_id, $msg->body);
            $logger->info("[$id_consumer:$mode:$user_id:" .$country_id .":". $device_id . ":" . $date_source_msg . ":$msg->body:$platform][response:$response]");
            //echo PHP_EOL . ' [x] ', $msg->delivery_info['routing_key'], ':', $msg->body, "\n";
            //echo 'Message headers follows' . PHP_EOL;
            //var_dump($msg->get('application_headers')->getNativeData());
            //echo PHP_EOL;
        };

        $channel->basic_consume($queue_name, '', false, true, false, false, $callback);

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();        
        
    }
    
    public function holdUpConsumerControlAction() {
        $this->logger = Logger::getLogger('HoldUpConsumerControlAction');
        
        $command_consumer_control = "php app/cli.php pushs consumercontrol";

        $is_running = shell_exec("pgrep -l -f \"$command_consumer_control\"");
        if ($is_running) {
            $this->logger->info("Consumer control process is running");
        }
        else {
            $this->logger->info("Consumer control process is NOT running");
            $this->logger->info("Execute: " . $command_consumer_control);
            shell_exec($command_consumer_control." > /dev/null &");
        }
    }
    
}
