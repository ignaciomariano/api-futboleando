<?php

use Phalcon\DI\FactoryDefault\CLI as CliDI,
    Phalcon\CLI\Console as ConsoleApp;


$config = include __DIR__ . "/config/config.php";

/**
 * Read auto-loader
 */
include __DIR__ . "/config/loader.php";

$loader->registerDirs(
    array(__DIR__.'/tasks'),
    true
);

$config->log4php->appenders->default->params->datePattern = '\t\a\s\k\s_Ymd';

include __DIR__ . "/config/services.php";


//Using the CLI factory default services container
$diCli = new CliDI();

$console = new ConsoleApp();
$diCli->set('config', $config);
$diCli->setShared('db', $di->getShared('db'));
$console->setDI($diCli);

/**
 * Process the console arguments
 */
$arguments = array();
foreach ($argv as $k => $arg) {
    if ($k == 1) {
        $arguments['task'] = $arg;
    } elseif ($k == 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

// define global constants for the current task and action
define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

try {
    // handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}
