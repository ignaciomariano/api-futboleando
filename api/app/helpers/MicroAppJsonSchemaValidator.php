<?php


class MicroAppJsonSchemaValidator {

    private $schemasDir;
    private $cache;
    private $lastErrors = array();
    private $ignoreInexistentSchema = false;
    private $wasIgnored = false;

    public function __construct(\Phalcon\Cache\Backend $cache, $schemasDir) {
        $this->schemasDir = $schemasDir;
        $this->cache = $cache;
    }
    
    public function setIgnoreInexistentSchema($ignore){
        $this->ignoreInexistentSchema = $ignore;
    }

    /**
     * 
     * @param string $schema
     * @param Phalcon\Http\Request $request
     * @param boolean $lascive
     * @return boolean
     * @throws \BadMethodCallException
     */
    public function validatePhalconRequest($pattern, $request, $lascive = false) {
        $method = $request->getMethod();
        $schema = preg_replace('/(\{(.+?)\})+/', 'param', strtolower($method) . $pattern);
        $schema = preg_replace('/\//', '-', $schema);
        $schema = "$schema-request";
        
        $paramsObj = (object) ($request->get());
        if(isset($paramsObj->_url)){
            unset($paramsObj->_url);
        }
        return $this->validate($schema, $paramsObj, $lascive);
    }
    
    /**
     * 
     * @param string $schema
     * @param Phalcon\Http\Response $response
     * @return boolean
     */
    public function validatePhalconResponse($method, $pattern, $response){
        $schema = preg_replace('/(\{(.+?)\})+/', 'param', strtolower($method) . $pattern);
        $schema = preg_replace('/\//', '-', $schema);
        $schema = "$schema-response";
        $paramsObj = json_decode($response->getContent());
        return $this->validate($schema, $paramsObj);
    }
    
    
    public function validate($schema, $json, $lascive = false){
        if($parsedSchema = $this->getParsedSchema($schema)){
            
            if($lascive){
                //Using jsv4 to coerce
                $jsv4 = Jsv4::coerce($json, $parsedSchema);
                if($jsv4->valid) {
                    $json = $jsv4->value;
                } else {
                    //Let JsonSchema invalid this
                }
            }
            
            // Validate
            $validator = new JsonSchema\Validator();
            if($lascive){
                $validator = new JsonSchema\Validator(JsonSchema\Validator::CHECK_MODE_TYPE_CAST);
            } else {
                $validator = new JsonSchema\Validator();
            }
            $validator->check($json, $parsedSchema);

            if ($validator->isValid()) {
                $this->lastErrors = array();
                return true;
            } else {
                $this->lastErrors = $validator->getErrors();
                return false;
            }            
        } else {
            if($this->ignoreInexistentSchema){
                $this->lastErrors = array();
                $this->wasIgnored = true;
                return true;
            } else {
                throw new \BadMethodCallException('Schema '.$this->schemasDir."/$schema.json not found");
            }
        }        
        
    }
    
    private function getParsedSchema($schema){
        $cacheKey = "schemas-$schema";
            
        $parsedSchema = null;

        $this->wasIgnored = false;
        
        if ($this->cache->exists($cacheKey)) {
            $parsedSchema = $this->cache->get($cacheKey);
        } else {
            $schemaFile = $this->schemasDir . "/$schema.json";
            if (is_file($schemaFile)) {
                $retriever = new JsonSchema\Uri\UriRetriever;
                $parsedSchema = $retriever->retrieve('file://' . $schemaFile, $this->schemasDir.'/');

                // If you use $ref or if you are unsure, resolve those references here
                // This modifies the $schema object
                $refResolver = new JsonSchema\RefResolver($retriever);
                $refResolver->resolve($parsedSchema, 'file://' . $this->schemasDir.'/');
                $this->cache->save($cacheKey, $parsedSchema);
            } else {
                $this->cache->save($cacheKey, null);
            }
        }
        
        return $parsedSchema;
        
    }
    
    
    public function getLastErrors(){
        return $this->lastErrors;
    }
    
    public function wasIgnored(){
        return $this->wasIgnored;
    }
    

}
