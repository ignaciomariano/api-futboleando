<?php


class Push {

    private $parseApplicationId;
    private $parseRESTAPIKey;
    private $androidApiKey;
    private $logger;

    public function __construct($params) {
        $this->parseApplicationId = $params['parse']['applicationId'];
        $this->parseRESTAPIKey = $params['parse']['RESTAPIKey'];
        $this->androidApiKey = $params['android']['apiKey'];
        $this->apple = $params['apple'];
        $this->logger = Logger::getLogger('Push');
    }

    private function ios_connect() {
        
    }

    public function nativeDeviceId($parse_device_id) {
        $url = "https://api.parse.com/1/installations/$parse_device_id";

        $req = new Pmovil2_Http_Request($url, HTTP_METH_GET);
        $req->addHeaders(array(
            "X-Parse-Application-Id" => $this->parseApplicationId,
            "X-Parse-REST-API-Key" => $this->parseRESTAPIKey
        ));

        try{
             $response = $req->send()->getBody();
        } catch (Exception $ex) {
            return false;
        }
        
        $response = json_decode($response);
        
        if(isset($response->code) && $response->code == 101){
            //Objeto nao encontrado
            return false;
        }
        
        if(!empty($response->deviceToken)){
            $native_device_id = $response->deviceToken;
        } elseif(!empty($response->deviceUris->_Toast)){
            $native_device_id = $response->deviceUris->_Toast;
        } else {
            $native_device_id  = null;
        }
        
        return $native_device_id;
        
    }

    public function send($platform, $device_id, $message, $uniqueId = 0, $match_id = 0, $log = null) {


        switch (strtoupper($platform)) {

            case 'GCM':
            case 'GCM-BROADCAST':
                $api_key = $this->androidApiKey;
                $arr_device_id = (strtoupper($platform) == 'GCM-BROADCAST') ? $device_id : array($device_id);
                $url = 'https://android.googleapis.com/gcm/send';
                $fields = array(
                    'registration_ids' => $arr_device_id,
                    'collapse_key' => "$uniqueId",
                    'data' => array('alert' => $message, 'match_id' => $match_id),
                );

                $headers = array(
                    'Authorization' => ' key=' . $api_key,
                    'Content-Type' => 'application/json'
                );
                
                $req = new Pmovil2_Http_Request($url, HTTP_METH_POST);
                $req->addHeaders($headers);
                $req->setBody(json_encode($fields));
                // Execute post
                try {
                    $response = $req->send()->getBody();
                } catch (exception $ex) {
                    $response = $ex->getMessage();
                }
                return $response;

                break;
            case 'IOS':
                $apple_obj = new ApplePush($this->apple);
                $response = $apple_obj->send($message, $device_id);
                return $response;
                break;
            case 'PARSE':
            default:
                $url = 'https://api.parse.com/1/push';
                $push_payload = json_encode(array(
                    "where" => array(
                        "objectId" => $device_id,
                    ),
                    "data" => array(
                        "alert" => $message, #"PHP Push test.",
                        "activityOpen" => "match",
                        "matchId" => $match_id
                    )
                ));

                $req = new Pmovil2_Http_Request($url, HTTP_METH_POST);
                $req->addHeaders(array("X-Parse-Application-Id" => $this->parseApplicationId,
                    "X-Parse-REST-API-Key" => $this->parseRESTAPIKey,
                    "Content-Type"  => "application/json"));
                $req->setBody($push_payload);

                try {
                    $response = $req->send()->getBody();
                } catch (exception $ex) {
                    $response = $ex->getMessage();
                }

                return $response;
                break;
        }
    }

}
