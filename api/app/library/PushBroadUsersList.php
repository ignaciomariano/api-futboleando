<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersList
 *
 * @author ivan
 */
class PushBroadUsersList {
    
    public static $platform_types = array(
                                            'gcm' => array('android','ANDROIDP'), 
                                            'ios' => array('IOS','ios_parse'), 
                                            'win' => array('windows8p') 
                                         );
    
    
    public static function all($country_id, $platforms){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id'");
    }
    
    public static function activityTime($country_id, $platforms, $time_interval){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND latest_dt > '".date("Y-m-d H:i:s", time() - $time_interval)."'");
    }

    public static function inactivityTime($country_id, $platforms, $time_interval){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND latest_dt < '".date("Y-m-d H:i:s", time() - $time_interval)."'");
    }
    
    public static function lessLives($country_id, $platforms, $lives_ref){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND whistle_items <= $lives_ref");
    }
    
    public static function moreLives($country_id, $platforms, $lives_ref){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND whistle_items >= $lives_ref");
    }
    
    public static function lessCoins($country_id, $platforms, $coins_ref){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND coins <= $coins_ref");
    }
    
    public static function moreCoins($country_id, $platforms, $coins_ref){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND coins >= $coins_ref");
    }
    
    public static function specificUserId($country_id, $platforms, $user_id){
                
        return Users::find("phone_data != 'LOGOUT' ".self::mount_where_clause($platforms)."AND country_id='$country_id' AND user_id= $user_id");
    }     
    
    private function mount_where_clause($platforms) {
        
        $where_platform = '';
        if ($platforms != '*') {
            $arr_plat = explode(",", $platforms);
            $sep="";
            foreach ($arr_plat as $plat) {
                $str_in .= $sep.implode("','", self::$platform_types[$plat]);
                $sep = "','";
            }
            $where_platform = "AND platform IN ('$str_in') ";
        }
        return $where_platform;
    }
    
}
