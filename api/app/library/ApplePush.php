<?php

class ApplePush {

    public $PRODUCTION_CERT;
    public $status = 0;
    private $ssl = 'ssl://gateway.push.apple.com:2195';
    private $sandboxSsl = 'ssl://gateway.sandbox.push.apple.com:2195';
    private $sandboxFeedback = 'ssl://feedback.sandbox.push.apple.com:2196';
    private $feedback = 'ssl://feedback.push.apple.com:2196';
    private $passphrase;
    private $socket = null;
    private $ctx = null;
    private $iosExpire = null;
    private $check_response = false;
    
    private $logger;

    private static $connections;
    
    public function __construct($params) {
        $this->iosExpire = time() + (90 * 24 * 60 * 60);
        $this->PRODUCTION_CERT = $params['cert'];
        $this->passphrase = $params['passphrase'];
        $this->logger = Logger::getLogger('ApplePush');
    }

    public function connect() {
        
        if(empty(self::$connections[$this->PRODUCTION_CERT]) || $this->status != 1){
            $this->ctx = stream_context_create();
            stream_context_set_option($this->ctx, 'ssl', 'local_cert', $this->PRODUCTION_CERT);
            stream_context_set_option($this->ctx, 'ssl', 'passphrase', $this->passphrase);
            $this->socket = stream_socket_client($this->ssl, $error, $errorString, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $this->ctx);
            stream_set_blocking($this->socket, 0);
            if (!$this->socket) {
                $this->status = -1;
                return false;
            }
            $this->status = 1; # connected
            self::$connections[$this->PRODUCTION_CERT]['socket'] = $this->socket;
            self::$connections[$this->PRODUCTION_CERT]['status'] = $this->status;
            self::$connections[$this->PRODUCTION_CERT]['ctx'] = $this->ctx;
        } else {
            $this->socket = self::$connections[$this->PRODUCTION_CERT]['socket'];
            $this->status = self::$connections[$this->PRODUCTION_CERT]['status'];
            $this->ctx = self::$connections[$this->PRODUCTION_CERT]['ctx'];
        }
        return true;
    }

    public function disconnect() {
        fclose($this->socket);
        $this->status = 0;
    }

    private function echoe($msg) {
        $this->logger->info($msg);
    }

    public function send($msg, $token, $data = null) {
        $this->connect();
        if ($this->status < 1) {
            return 'disconnected';
        }
        $body['aps'] = array('alert' => $msg, 'sound' => 'default');
        $body['data'] = $data;
        $payload = json_encode($body);

        $this->logger->info("Request: $data - $payload");
        
        $msg = pack("C", 1) . pack("N", 1000) . pack("N", $this->iosExpire) . pack("n", 32) . pack('H*', str_replace(' ', '', $token)) . pack("n", strlen($payload)) . $payload;
        $result = fwrite($this->socket, $msg, strlen($msg));
        if ($this->check_response && !$this->ios_check_response($this->socket)) {
            $this->echoe('Token Error: ' . $token);
            return 'error';
        }
        return 'success';
    }

    private function ios_check_response($fp) {
        $status = true;
        usleep(500000); //Min: 50ms. Avg: 100ms.
        $apple_error_response = fread($fp, 6);
        if ($apple_error_response) {
            $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);
            switch ($error_response['status_code']) {
                case 0:
                    $status = true;
                    break;
                case 1:
                    $this->echoe('iOS: Processing error');
                    $status = false;
                    break;
                case 2:
                    $this->echoe('iOS: Missing device token');
                    $status = false;
                    break;
                case 3:
                    $this->echoe('iOS: Missing topic');
                    $status = false;
                    break;
                case 4:
                    $this->echoe('iOS: Missing payload');
                    $status = false;
                    break;
                case 5:
                    $this->echoe('iOS: Invalid token size');
                    $status = false;
                    break;
                case 6:
                    $this->echoe('iOS: Invalid topic size');
                    $status = false;
                    break;
                case 7:
                    $this->echoe('iOS: Invalid payload size');
                    $status = false;
                    break;
                case 8:
                    $this->echoe('iOS: Invalid token');
                    $status = false;
                    break;
                case 10:
                    $this->echoe('iOS: Shutdown');
                    $status = false;
                    break;
                case 255:
                    $this->echoe('iOS: 255-None (unknown)');
                    $status = false;
                    break;
                default:
                    $this->echoe('iOS: Unknown Error.');
                    $status = false;
                    break;
            }
        }

        return $status;
    }

}
