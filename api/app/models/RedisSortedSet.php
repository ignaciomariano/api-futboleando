<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redis
 *
 * @author ivan
 */
abstract class RedisSortedSet implements \Phalcon\Mvc\CollectionInterface{
    
    /**
     *
     * @var Redis
     */
    protected $redisConn;
    
    public $id;
    public $score;
    
    public static $source;
    
    public function __construct(Phalcon\DiInterface $dependencyInjector = null){
        $di = \Phalcon\DI::getDefault();
        $this->redisConn = $di->getShared('redis');
        
        if(is_callable(array($this, 'initialize'))){
            $this->initialize();
        }
    }
    
    public function appendMessage($message) {
        
    }

    public function fireEvent($eventName) {
        
    }

    public function fireEventCancel($eventName) {
        
    }

    public function getConnection() {
        
    }

    public function getId() {
        
    }

    public function getMessages() {
        
    }

    public function getReservedAttributes() {
        
    }

    public function getSource() {
        
    }

    public function readAttribute($attribute) {
        
    }

    public function setConnectionService($connectionService) {
        
    }

    public function setId($id) {
        
    }

    public function validationHasFailed() {
        
    }

    public function writeAttribute($attribute, $value) {
        
    }

    public static function cloneResult($collection, $document) {
        
    }

    public static function count($parameters = null) {
        
    }

    public static function find($parameters = null) {
    }

    public static function findById($id) {
    }

    public static function findFirst($parameters = null) {
    }
    
    
    public function delete(){
        
    }
    public function save(){
        
    }
    public function update(){
        
    }
    public function create(){
        
    }
    
    /**
     * WARNING!!!! This one drops all Sorted Set
     */
    public static function dropAllSet(){
        $di = \Phalcon\DI::getDefault();
        $redisConn = $di->getShared('redis');
        $redisConn->del(static::$source);
    }


}
