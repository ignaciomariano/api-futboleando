<?php

class Redir extends \Phalcon\Mvc\Model {

    public static function getUrlsByCode($code) {
        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $select = "SELECT * FROM redir WHERE url_code = :code;";
        $result = $db->fetchOne($select, Phalcon\Db::FETCH_ASSOC, array('code' => $code));
        return $result;
    }

    public static function saveHistory($code, $ua, $ua_result, $redir_result) {
        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO redir_history VALUES (null, :code, :ua,:ua_result, :redir_result,NOW()); ";
        $success = $db->execute($query, array(
            'code' => $code,
            'ua' => $ua,
            'ua_result' => $ua_result,
            'redir_result' => $redir_result
        ));
        return $success;
    }

}
