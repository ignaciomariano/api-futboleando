<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ranking
 *
 * @author ivan
 */
class Ranking extends \RedisSortedSet {

    public $id = null;
    public $score = null;
    public static $source = 'ranking';
    

    public function getSource() {

        return self::$source;
    }

    public static function get($id = "") {

        if ($id != "") {

            $redis = \Phalcon\DI\FactoryDefault::getDefault()->getShared('redis');

            $out["id"] = $id;
            $out["score"] = $redis->zScore(self::$source, $id);
            $out["rank"] = $redis->zRevRank(self::$source, $id) + 1;

            return $out;
        } else {

            return false;
        }
    }

    public static function getRanking($offset = 1, $tot = 10) {

        $redis = \Phalcon\DI\FactoryDefault::getDefault()->getShared('redis');

        $ini = $offset - 1;
        $limit = $tot + $ini - 1;

        $ranking = $redis->zRevRange(self::$source, $ini, $limit, false);

        $out = array();

        foreach ($ranking as $rank => $id) {

            $out[$rank] = self::get($id);
        }

        return $out;
    }

    public static function getUserPos($id = "", $tot = 10) {

        $redis = \Phalcon\DI\FactoryDefault::getDefault()->getShared('redis');

        if ($id != "") {

            $limit = $tot - 1;

            if ($limit % 2 == 0) {

                $limit_a = $limit / 2;
                $limit_d = $limit / 2;
            } else {

                $limit_a = floor($limit / 2);
                $limit_d = ceil($limit / 2);
            }

            $pos = $redis->zRevRank(self::$source, $id);

            if ($pos <= ( $limit_a - 1 )) {

                $out = self::getRanking(1, $tot);
            } else {

                // ZREVRANGE foo <$OFFSET - $LIMIT> <$OFFSET -1>
                // menos pontos
                $ranking_a = $redis->zRevRange(self::$source, ( $pos - $limit_a), ( $pos - 1), false);
                // ZREVRANGE foo <$OFFSET +1 > <$OFFSET + $LIMIT - 1>
                // mais pontos

                $ranking_d = $redis->zRevRange(self::$source, ( $pos + 1), ($pos + $limit_d), false);

                $rank_ref = 0;
                foreach ($ranking_a as $rank => $idl) {

                    $out[$rank_ref] = self::get($idl);
                    $rank_ref++;
                }

                $out[$rank_ref] = self::get($id);
                $rank_ref++;

                foreach ($ranking_d as $rank => $idl) {

                    $out[$rank_ref] = self::get($idl);
                    $rank_ref++;
                }
            }

            return $out;
        } else {

            return false;
        }
    }

    public function getConnection() {

        return $this->redisConn;
    }

    public function save() {

        if (!is_null($this->id) and ! is_null($this->score)) {
            $this->redisConn->zAdd(self::$source, $this->score, $this->id);

            return true;
        } else {

            return null;
        }
    }

    public function setId($id) {

        $this->id = $id;
        $this->getScore($id);
    }

    public function getId() {

        return $this->id;
    }

    public function setScore($score) {

        $this->score = $score;
    }

    public function getScore() {

        if (empty($this->score)) {

            $tmp_score = $this->redisConn->zScore(self::$source, $this->id);
            $score = ( $tmp_score ) ? (int) $tmp_score : 0;

            $this->setScore($score);
        }

        return $this->score;
    }

    public function incScore($inc) {

        $score = $this->getScore();

        $this->setScore($score + $inc);
    }

    public function delete() {

        $this->redisConn->zRem(self::$source, $this->id);
        $this->id = null;
        $this->score = null;
    }

    public static function count($parameters = null) {

        return $this->redisConn->zCard(self::$source);
    }

    public static function find($parameters = null) {
        
    }

    public static function findById($id) {
        
    }

    public static function findFirst($parameters = null) {
        
    }

    public static function getBasePoints() { // Retorna array com os pontos recebidos por cada acao
        // :TODO Buscar valores na base
        $out["correct"] = 1;
        $out["avatar"] = 10;
        $out["duel"] = 20;

        return $out;
    }

    public static function addRanking($params = array(), $update_global = true) {

        if (empty($params["match_id"])) {

            // $this->log->alert( "Ranking::addLog -> match_id faltando" );
            return null;
        } else {

            if ($params["is_correct"] != "1") {

                return null;
            } else {

                $points = self::getBasePoints();

                $tot = 0;

                // Sempre ganha um ponto por resposta correta
                $in["correct"] = $points["correct"];
                $tot += $points["correct"];
                $in["qtd_correct"] = "1";

                // Define os pontos ganhos por duelo
                if ($params["result_type"] == RESULT_TYPE_DUELO) {

                    $in["duel"] = $points["duel"];
                    $in["qtd_duel"] = "1";
                    $tot += $points["duel"];
                } else {

                    $in["duel"] = "0";
                    $in["qtd_duel"] = "0";
                }

                // Define os pontos ganhos por bola de ouro
                if ($params["category_id"] != "0" and $params["result_type"] != RESULT_TYPE_DUELO) {

                    $in["avatar"] = $points["avatar"];
                    $tot += $points["avatar"];
                    $in["qtd_avatar"] = "1";
                } else {

                    $in["avatar"] = "0";
                    $in["qtd_avatar"] = "0";
                }


                if (!($match_points = PointsMatchModel::findFirst(array(
                            'conditions' => "user_id = $params[user_id] AND match_id = $params[match_id]",
                            'extras' => array('match_id' => $params['match_id'])
                                )
                        ))) {
                    $match_points = new PointsMatchModel();
                    $match_points->match_id = $params['match_id'];
                    $match_points->user_id = $params['user_id'];
                }
                $match_points->correct_answers += $in['qtd_correct'];
                $match_points->points_correct += $in['correct'];
                $match_points->avatars_won += $in['qtd_avatar'];
                $match_points->points_avatar += $in['avatar'];
                $match_points->duels_won += $in['qtd_duel'];
                $match_points->points_duel += $in['duel'];
                $match_points->save();


                if ($update_global) {
                    $ranking = new \Ranking();

                    $ranking->setId($params["user_id"]);
                    $ranking->incScore($tot);
                    $ranking->save();
                }
                return null;
            }
        }
    }

    public static function getPointsMatch($params = array()) {

        try {
        	
        	// $table = self::createPointsMatch($params);
        	
        	$table["table"] = PointsMatchModel::dynTableName($params["created_on"]);
        	
            $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

            $query = "SELECT * FROM " . $table["table"] . " where match_id = :match_id AND user_id IN( :owner, :guest )";

            $in["match_id"] = $params["match_id"];
            $in["owner"] = $params["owner_user_id"];
            $in["guest"] = $params["guest_user_id"];

            $m = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, $in);

            foreach ($m as $i) {

                if ($i['user_id'] == $params["owner_user_id"]) {
                    $type = "owner";
                } else {
                    $type = "guest";
                }

                $out[$type]['correct'] = $i["points_correct"];
                $out[$type]['duelos'] = $i["points_duel"];
                $out[$type]['copas'] = $i["points_avatar"];
                $out[$type]['correct_qtd'] = $i["correct_answers"];
                $out[$type]['duelos_qtd'] = $i["duels_won"];
                $out[$type]['copas_qtd'] = $i["avatars_won"];
            }

            return $out;
        } catch (Exception $e) {
            return null;
        }
    }

}
