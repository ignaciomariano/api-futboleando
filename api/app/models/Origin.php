<?php

class Origin extends \Phalcon\Mvc\Model {

    public static function add($ip, $origin) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO origin VALUES ( NULL, :ip, :origin, NOW());";
        $p = $db->execute($query, array(
            'ip' => $ip,
            'origin' => $origin
        ));
        if ($p) {
            return intval($db->lastInsertId());
        }
        return 0;
    }

    public static function deleteById($id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $success = $db->delete("origin", "id= $id");
        return $success;
    }

    public static function getByIp($ip) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT * FROM origin o WHERE o.ip = :ip;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('ip' => $ip));
        return $result;
    }

}
