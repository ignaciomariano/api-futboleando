<?php

class PointsMatchModel extends \Phalcon\Mvc\Model {

    public $match_id;
    public $user_id;
    public $points_correct = 0;
    public $points_avatar = 0;
    public $points_duel = 0;
    public $correct_answers = 0;
    public $avatars_won = 0;
    public $duels_won = 0;
    public $match_start;
    
    private $logger;
    
    private static $tmpMatchStart;
    
    public function initialize(){
        $this->useDynamicUpdate(true);
        $this->belongsTo('match_id', 'Matchs', 'match_id');
        $this->belongsTo('user_id', 'Users', 'user_id');
    }

    public function onConstruct(){
        $this->logger = Logger::getLogger('PointsMatchModel');
    }

    public static function find($parameters = null) {
        static::checkParameters($parameters);
        try {
            $ret = parent::find($parameters);
        } catch (\Exception $e) {
            static::createTableIfNotExists(static::$tmpMatchStart);
            $ret = parent::find($parameters);
        }
        static::$tmpMatchStart = null;
        return $ret;
    }

    public static function findFirst($parameters = null) {
        static::checkParameters($parameters);
        try{
            $ret = parent::findFirst($parameters);
        } catch (\Exception $e){
            static::createTableIfNotExists(static::$tmpMatchStart);
            $ret = parent::findFirst($parameters);
        }
        static::$tmpMatchStart = null;
        return $ret;
    }
    
    public static function checkParameters($parameters = null){
        if(!is_array($parameters)) throw new InvalidArgumentException('This model needs a array with extras => match_start, extras => match_id or extras => table on parameters to work');
        
        if(empty($parameters['extras']['match_start']) && empty($parameters['extras']['match_id']) && empty($parameters['extras']['table'])) throw new InvalidArgumentException('This model needs extras => match_start, extras => match_id or extras => table on parameters to work');
        
        if(isset($parameters['extras']['match_start'])){
            static::$tmpMatchStart = $parameters['extras']['match_start'];
        } elseif(isset($parameters['extras']['table'])){
            static::$tmpMatchStart = preg_replace('/^points_match_/', '', $parameters['extras']['table']).'01000000';
        } else {
            static::$tmpMatchStart = Matchs::findFirst($parameters['extras']['match_id'])->created_on;
        }
    }
    
    
    public static function dynTableName($strTime){
        return 'points_match_'.gmdate('Ym', strtotime($strTime));
    }
    
    
    public function save($data = NULL, $whiteList = NULL) {
        try {
            return parent::save($data, $whiteList);
        } catch (\Phalcon\Mvc\Model\Exception $e) {
            $this->createTableIfNotExists($this->match_start);
            return parent::save($data, $whiteList);
        }
    }    
    
    public function getSource() {
        if(!empty(static::$tmpMatchStart)){
            $this->match_start = static::$tmpMatchStart;
            $this->getModelsManager()->__destruct();
        } else if(isset($this->match_start)){
            //Already set
        } else if(isset($this->match_id)){
            $this->match_start = $this->getMatchs()->created_on;
        }
        
        if(empty($this->match_start)){
            throw new InvalidArgumentException('This model needs match_start date to work');
        }
        $this->logger->info("Match start: $this->match_start");
        $table = static::dynTableName($this->match_start);
        $this->logger->info("Using points_match_$table");

        return $table;
    }
    
    private static function createTableIfNotExists($strTime) {
        $creator = new PointsMatchModel();
        $creator->getWriteConnection()->execute('CREATE TABLE IF NOT EXISTS '.static::dynTableName($strTime).' LIKE points_match_model');
    }   
    
    public static function getTables(){
        $dumper = new PointsMatchModel();
        $stm = $dumper->getReadConnection()->query('SHOW TABLES LIKE "points_match_2%"');
        $stm->setFetchMode(\Phalcon\Db::FETCH_COLUMN, 0);
        return $stm->fetchAll();
    }
    
}
