<?php

class Countries extends \Phalcon\Mvc\Model {

    public static function getAll() {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT id, name, code, lang from countries;";
        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC);
        return $p;
    }

}
