<?php

class Reported extends \Phalcon\Mvc\Model {

    public $id;
    public $question_id;
    public $user_id;
    public $date;
    public $reported_reasons_id;
    public $reasons;
    public $checked;

    public static function getAll($lang) {#
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT id, question_id, user_id, date, reported_reasons_id from reported;";
        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('lang' => $lang));
        return $p;
    }

    public static function add($info) {
        #print_r($info);exit
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $fields = array_keys($info);
        $values = implode(",", array_values($info));
        $fieldlist = implode(',', $fields);
        $query = "INSERT INTO reported  values (NULL,$values);";
        #print_r($query);exit;
        $p = $db->execute($query);
        if ($p) {
            return intval($db->lastInsertId());
        }
        return 0;
    }

}
