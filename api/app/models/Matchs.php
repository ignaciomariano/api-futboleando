<?php

class Matchs extends \Phalcon\Mvc\Model {

    public $match_id;
    public $owner_user_id;
    public $guest_user_id;
    public $created_on;
    public $status;
    public $owner_points;
    public $guest_points;
    public $lang;
    public $turn;
    public $ronda;
    public $updated;
    public $last_result_type;
    public $winner;

    public function initialize() {
        $this->useDynamicUpdate(true);
        
        $this->hasMany('match_id', 'MatchLog', 'match_id');

        $this->belongsTo('owner_user_id', 'Users', 'user_id', array(
            'alias' => 'OwnerUser'
        ));

        $this->belongsTo('guest_user_id', 'Users', 'user_id', array(
            'alias' => 'GuestUser'
        ));
        
        $this->hasMany('match_id', 'PointsMatchModel', 'match_id');
        
    }
    

    public static function updatePoints($arr_info) {
        if (isset($arr_info['guest_points'])) {
            $wh = " guest_points = " . $arr_info['guest_points'];
        } elseif (isset($arr_info['owner_points'])) {
            $wh = " owner_points = " . $arr_info['owner_points'];
        } else {
            return 0;
        }
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE matchs SET $wh WHERE match_id = :match_id";
        $p = $db->execute($query, array(
            'match_id' => $arr_info['match_id']
        ));
        return $p;
    }

    public static function getEnemy($match_id, $user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT m.owner_user_id, m.guest_user_id, u.* 
            FROM matchs m INNER JOIN users u ON (m.owner_user_id = u.user_id OR m.guest_user_id = u.user_id) 
            WHERE m.match_id = :match_id AND u.user_id <> :user_id LIMIT 1;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array(
            'match_id' => $match_id,
            'user_id' => $user_id));
        return $result;
    }

    public static function addAnswer($arr_info) {
        #print_r($arr_info);
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO match_log VALUES ("
                . " NULL, :match_id, :user_id, :question_id, :answer_id, :is_correct, :category_id, NOW(), :result_type, :time, :duel_id);";
        #print_r($query);
        $success = $db->execute($query, array(
            'match_id' => $arr_info['match_id'],
            'user_id' => $arr_info['user_id'],
            'question_id' => $arr_info['question_id'],
            'answer_id' => $arr_info['answer_id'],
            'is_correct' => $arr_info['is_correct'],
            'category_id' => $arr_info['category_id'],
            'result_type' => $arr_info['result_type'],
            'time' => $arr_info['time_in_seconds'],
            'duel_id' => $arr_info['duel_id']
        ));

        Ranking::addRanking($arr_info);
        return $success;
    }

    public static function addbackuop($owner, $guest) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO matchs VALUES ("
                . " NULL, :owner, :guest, NOW(), 0, 0, 0, 'ES', :owner, 1, NOW(), 0, 0);";
        $p = $db->execute($query, array(
            'owner' => $owner,
            'guest' => $guest
        ));
        if ($p) {
            return intval($db->lastInsertId());
        }
        return 0;
    }

    public static function addinvite($owner, $guest) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO matchs VALUES ("
                . " NULL, :owner,:guest , NOW(), 1, 0, 0, 'ES', :owner, 1, NOW(), 0, 0);";
        $p = $db->execute($query, array(
            'owner' => $owner,
            'guest' => $guest
        ));
        if ($p) {
            return intval($db->lastInsertId());
        }
        return 0;
    }

    public static function add($owner, $guest) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO matchs VALUES ("
                . " NULL, :owner,0 , NOW(), 1, 0, 0, 'ES', :owner, 1, NOW(), 0, 0);";
        $p = $db->execute($query, array(
            'owner' => $owner
        ));
        if ($p) {
            return intval($db->lastInsertId());
        }
        return 0;
    }

    public static function getPerformance($user_id, $match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT q.category_id, COUNT(m.is_correct) as cant, m.is_correct as is_correct  
        FROM match_log m INNER JOIN questions q ON q.question_id = m.question_id 
        WHERE m.user_id = :user_id AND m.match_id = :match_id AND m.result_type = 0 
        GROUP BY q.category_id, m.is_correct 
        ORDER BY q.category_id ASC, m.is_correct ASC;";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array("user_id" => $user_id, "match_id" => $match_id));
        return $result;
    }

    public static function getInfo($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT * FROM matchs m 
            WHERE m.match_id = :match_id;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
        return $result;
    }

    public static function getLog($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT ml.*, m.owner_user_id, m.guest_user_id, m.status "
                . "FROM match_log ml left join matchs m ON (m.match_id = ml.match_id) WHERE m.match_id = :match_id ORDER BY ml.id_match_log ASC; ";
        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
        return $p;
    }

    public static function deleteMatch($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $success = $db->delete(
                "matchs", "match_id = $match_id"
        );
        return $success;
    }

    public static function queryUpdate($query) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $success = $db->execute($query);
        return $success;
    }

    public static function updateStatus($status, $match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE matchs SET status = :status WHERE match_id = :match_id";
        $p = $db->execute($query, array(
            'status' => $status,
            'match_id' => $match_id
        ));
        return $p;
    }

    public static function duelsNoAnswered($match_id) {
        try {
            $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
            $query = "SELECT id FROM duels WHERE match_id = :match_id AND status = 0";
            $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
            if (isset($p['id'])) {
                return $p['id'];
            }
            return null;
        } catch (\Phalcon\Exception $e) {
            return null;
        }
    }

    public static function reply($match_id, $status, $user_type, $user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $winner_str = "";
        if ($status == 4) {
            # Si abandono un rival
            $winner = ($user_type == 'owner') ? 'guest_user_id' : 'owner_user_id';
            $winner_str = " winner = {$winner} , ";
        }
        $query = "UPDATE matchs SET {$winner_str} status = :status, updated = NOW() "
                . " WHERE match_id = :match_id AND {$user_type}_user_id = :user_id;";
        $p = $db->execute($query, array(
            'status' => $status,
            'match_id' => $match_id,
            'user_id' => $user_id
        ));
        return $p;
    }

    public static function getToOwner($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT m.*, u.* FROM matchs m INNER JOIN users u ON u.user_id = m.owner_user_id WHERE m.match_id = :match_id;";
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
        return $p;
    }

    public static function getToGuest($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT m.*, u.* FROM matchs m INNER JOIN users u ON u.user_id = m.guest_user_id WHERE m.match_id = :match_id;";
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
        return $p;
    }

    public static function getMatchPending($owner_user_id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT match_id FROM matchs WHERE guest_user_id = 0 and owner_user_id != $owner_user_id limit 1";
        # HACER EL LOCK EN ESTA MISMA CONSULTA PARA NO TENER PEDIDOS SOLAPADOS
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC);
        if ($p) {
            if (!$db->query("SELECT GET_LOCK('match_piendente_$p[match_id]',0)")) {
                return array();
            }
        }
        return $p;
    }

    public static function updateMatchPending($match_id, $guest_user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE matchs SET guest_user_id = " . $guest_user_id . "  WHERE match_id = :match_id";
        $p = $db->execute($query, array(
            'match_id' => $match_id
        ));
        $db->query("SELECT RELEASE_LOCK('match_piendente_$match_id')");
        return $p;
    }

    public static function getToUser($match_id, $user_id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT m.*, u.* FROM matchs m INNER JOIN users u ON (u.user_id = m.owner_user_id OR u.user_id = m.guest_user_id)
            WHERE m.match_id = :match_id AND u.user_id <> :user_id; ";
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array(
            'match_id' => $match_id,
            'user_id' => $user_id
        ));
        return $p;
    }

    public static function getMatch($match_id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT owner_user_id, guest_user_id FROM matchs  WHERE match_id = :match_id;";
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
        return $p;
    }

    public static function getAll($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT m.*, IF(ISNULL(u.user_id),u2.user_id, u.user_id) as user_id, "
                . "IF(ISNULL(u.username),u2.username, u.username) as username,  "
                . "IF(ISNULL(u.fb_data),u2.fb_data, u.fb_data) as fb_data, "
                . "IF(ISNULL(u.avatar),u2.avatar, u.avatar) as avatar, "
                . "IF(ISNULL(u.fb_token),u2.fb_token, u.fb_token) as fb_token, TIMEDIFF(NOW(),m.updated) as time_desc "
                . "FROM matchs m LEFT JOIN users u ON (u.user_id = m.owner_user_id and u.user_id <> :user_id) "
                . "LEFT JOIN users u2 ON (u2.user_id = m.guest_user_id and u2.user_id <> :user_id )  "
                . "WHERE  ( m.owner_user_id = :user_id OR m.guest_user_id = :user_id )  ORDER BY m.updated DESC LIMIT 50;";
        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
        return $p;
    }

    public function getUserType($user_id) {
        if ($this->guest_user_id == $user_id) {
            return "guest";
        }
        return "owner";
    }

    public function getRivalUserId($user_id) {
        if ($this->guest_user_id == $user_id) {
            return $this->owner_user_id; 
        } else {
            return $this->guest_user_id; 
        }
    }
    
    public static function getMyTurn($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT m.match_id, m.turn, m.owner_user_id, m.guest_user_id, u.fb_data owner, us.fb_data as guest,"
                . " u.username owner_username, us.username guest_username "
                . "FROM matchs m "
                . "INNER JOIN users u on m.owner_user_id = u.user_id "
                . "INNER JOIN users us on m.guest_user_id = us.user_id "
                . "WHERE m.status = 1 and m.turn = $user_id;";

        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
        return $p;
    }
    
    
}
