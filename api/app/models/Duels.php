<?php

class Duels extends \Phalcon\Mvc\Model {

    public static function add($info) {

        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO duels VALUES ("
                . " NULL, :match_id, NOW(), :user_id, :win_category, :lost_category, 0);";
        $p = $db->execute($query, array(
            'match_id' => $info['match_id'],
            'user_id' => $info['created_by'],
            'win_category' => $info['win_category'],
            'lost_category' => $info['lost_category']
        ));
        if ($p) {
            return intval($db->lastInsertId());
        }
        return 0;
    }

    public static function updateStatus($duel_id, $status) {
        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE duels SET status = :status WHERE id = :duel_id;";
        $success = $db->execute($query, array(
            'status' => $status,
            'duel_id' => $duel_id
        ));
        return $success;
    }

    public static function addQuestions($questions, $duelId) {

        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $p = null;
        foreach ($questions as $q) {
            $question_id = $q['question_id'];
            $query = "INSERT INTO duel_questions VALUES ("
                    . " NULL, :question_id, :duel_id);";
            $p = $db->execute($query, array(
                'question_id' => $question_id,
                'duel_id' => $duelId
            ));
        }
        return $p;
    }

    public static function getGuest($duel_id) {

        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT m.guest_user_id, SUM(ml.is_correct) as correctas 
FROM matchs m INNER JOIN duels d ON d.id = :duel_id AND d.match_id = m.match_id INNER JOIN match_log ml ON m.guest_user_id = ml.user_id AND ml.duel_id = d.id;";
        $result = $db->fetchAll($query, \Phalcon\Db::FETCH_ASSOC, array(
            'duel_id' => $duel_id
        ));
        return $result;
    }

    public static function getWinner($duel_id) {
        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "select m.owner_user_id, SUM(ml.is_correct) as correctas, d.created_by, d.win_category 'win_cat', d.lost_category 'lost_cat' 
from matchs m INNER JOIN duels d ON d.id = :duel_id AND d.match_id = m.match_id INNER JOIN match_log ml ON m.owner_user_id = ml.user_id AND ml.duel_id = d.id;";
        $result = $db->fetchAll($query, \Phalcon\Db::FETCH_ASSOC, array(
            'duel_id' => $duel_id
        ));
        return $result;
    }

    public static function getDuelQuestions($duel_id, $match_id, $user_id) {
        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT d.id, d.created_by, d.status, d.match_id , dq.id, dq.question_id, mt.user_id, mt.answer_id , mt.time_in_seconds as 'time'
            FROM duels d INNER JOIN duel_questions dq ON dq.duel_id = d.id 
            LEFT JOIN match_log mt ON 
            (d.id = :duel_id AND mt.match_id = d.match_id AND d.id = mt.duel_id AND mt.user_id = :user_id AND dq.question_id = mt.question_id ) 
            WHERE d.id = :duel_id AND d.match_id = :match_id ORDER BY dq.id ASC;";
        $result = $db->fetchAll($query, \Phalcon\Db::FETCH_ASSOC, array(
            'user_id' => $user_id,
            'match_id' => $match_id,
            'duel_id' => $duel_id
        ));
        return $result;
    }

    public static function checkDuelToAnswer($match_id, $user_id) {
        $db =  \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT d.id FROM duels d INNER JOIN matchs m 
ON (m.match_id = d.match_id) where m.match_id = :match_id and 
m.turn = :user_id and d.status = 0;";
        $result = $db->fetchOne($query, \Phalcon\Db::FETCH_ASSOC, array(
            'user_id' => $user_id,
            'match_id' => $match_id
        ));

        if (isset($result['id'])) {
            return intval($result['id']);
        }
        return null;
    }

}
