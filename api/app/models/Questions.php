<?php

class Questions extends \Phalcon\Mvc\Model {

    public static function getId($question_id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

        $query = "SELECT q.question_id, q.question, q.level, q.category_id, q.answer_id, q.answer, q.is_correct , q.lang1 as 'lang'
            FROM questList q 
            WHERE q.question_id = :question_id ;";

        $result = $db->fetchAll($query, \Phalcon\Db::FETCH_ASSOC, array(
            'question_id' => $question_id
        ));

        return $result;
    }

    public static function isCorrect($question_id, $answer_id) {
        try {
            $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

            $query = "SELECT answer_id FROM answers WHERE answer_id = :answer_id AND question_id = :question_id AND is_correct = 1;";

            $result = $db->fetchOne($query, \Phalcon\Db::FETCH_ASSOC, array(
                'question_id' => $question_id,
                'answer_id' => $answer_id
            ));
            if ($result === false) {
                return 0;
            }
            return 1;
        } catch (\Phalcon\Exception $e) {
            return null;
        }
    }

    public static function getPowerUpPrice($id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

        $query = "SELECT cant_coins FROM prices WHERE id = :id";

        $result = $db->fetchOne($query, \Phalcon\Db::FETCH_ASSOC, array(
            'id' => $id
        ));
        if ($result) {
            return intval($result['cant_coins']);
        }
        return null;
    }

    public static function getEachCategory($lang, $user_id, $enemy_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        if (empty($lang)) {
            $lang = DEFAULT_LANG;
        }

        $cat = array(1, 2, 3, 4, 5, 6);
        $in_query = " ";

        foreach ($cat as $category) {
            $select = "
SELECT q.question_id FROM questions q LEFT JOIN question_lang ql ON q.question_id = ql.question_id 
LEFT JOIN match_log m ON q.question_id = m.question_id AND (m.user_id =:user_id or m.user_id=:enemy_id) 
WHERE  q.country_id = '*'  
AND ql.lang = :lang AND ( q.category_id = :category_id ) AND m.question_id IS NULL  limit 1;";

            if (!($data = $db->fetchOne($select, Phalcon\Db::FETCH_ASSOC, array(
                'user_id' => $user_id,
                'category_id' => $category,
                'enemy_id' => $enemy_id,
                'lang' => $lang
                    )))) {
                $data = $db->fetchOne("SELECT q.question_id FROM questions q LEFT JOIN question_lang ql ON q.question_id = ql.question_id WHERE q.country_id = '*' AND ql.lang = :lang AND ( q.category_id = :category_id)  ORDER BY RAND() limit 1", Phalcon\Db::FETCH_ASSOC, array(
                    'category_id' => $category,
                    'lang' => $lang
                ));
            }
            if (isset($data['question_id'])) {
                $in_query .= " q.question_id = " . $data['question_id'] . " or ";
            }
        }
        $in_query .= " q.question_id = 0 ";
        $select = "SELECT q.question_id, q.question, q.level, q.category_id, q.answer_id, q.answer, q.is_correct , q.lang1 as 'lang'
            FROM questList q 
            WHERE 1 AND $in_query
            ORDER BY q.question_id 
            LIMIT 24;";

        $result = $db->fetchAll($select, \Phalcon\Db::FETCH_ASSOC);
        return $result;
    }

    public static function getCountByUser($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT COUNT(ml.user_id) as answers FROM match_log ml WHERE ml.user_id = :user_id AND result_type <> 4;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array(
            'user_id' => $user_id
        ));
        if (isset($result['answers'])) {
            return intval($result['answers']);
        }
        return 10;
    }

    public static function iscorrectquestion($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT is_correct FROM match_log ml WHERE ml.user_id = :user_id AND result_type <> 4 order by id_match_log desc limit 1;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array(
            'user_id' => $user_id
        ));
        if (isset($result['is_correct'])) {
            return $result['is_correct'];
        }
        return 10;
    }

    public static function get($lang, $user_id, $qtd, $juegaCorona, $country, $photo, $log = null, $level = null) {
        if (empty($qtd)) {
            $qtd = 1;
        }
        $limit = $qtd * 4;
        if (empty($lang)) {
            $lang = DEFAULT_LANG;
        }
        if (empty($country)) {
            $country = '*'; #default
        }
        if (empty($user_id)) {
            $user_id = "0";
        }
        $category_where = "";
        if ($juegaCorona) {
            $category_where = " AND ( q.category_id = $juegaCorona ) ";
        } else {
            
        }

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $encontro = false;
        $q_arr = array();

        $orderLevel = $orderWhere = "";
        if ($level !== null) {
            $orderLevel = "FIELD (q.level, {$level},1,2,3), ";
            $orderWhere = " AND q.level = {$level} ";
        }
        if ($photo == 1) {
            $photo_where = " "; #" AND rectangular_image_at is NOT NULL or squared_image_at is NOT NULL ";
        } elseif ($photo == 2) {
            $photo_where = " AND (rectangular_image_at is NOT NULL OR squared_image_at is NOT NULL) ";
        } else {
            $photo_where = " AND (rectangular_image_at is NULL AND squared_image_at is NULL) ";
        }
        $string_cat_rand = "";
        $arr_cats = array(1, 2, 3, 4, 5, 6);
        shuffle($arr_cats);
        foreach ($arr_cats as $c) {
            $string_cat_rand .= $c . " ,";
        }
        $string_cat_rand.="0";
        #obtener ultimas 2 categorias usadas
        #select q.category_id from match_log m left join questions q ON q.question_id = m.question_id where m.user_id = 136742 and m.result_type = 9 order by m.id_match_log desc limit 2;
        ## condicion ideal
        if ($photo == 2) {
            $select1 = "SELECT q.question_id, q.category_id FROM questions q " .
                    "LEFT JOIN question_lang ql ON q.question_id = ql.question_id " .
                    "LEFT JOIN match_log ml ON (ml.user_id = {$user_id} AND ml.question_id = q.question_id ) " .
                    "WHERE (ml.question_id is NULL) AND (q.country_id = '{$country}' or q.country_id = '*') "
                    . "{$orderWhere} {$photo_where} {$category_where} AND ql.lang = '{$lang}' "
                    . "ORDER BY FIELD(q.category_id, $string_cat_rand), "
                    . "FIELD(q.country_id, '{$country}', '*') "
                    . "LIMIT 20;";
            $result1 = $db->fetchAll($select1, Phalcon\Db::FETCH_ASSOC);
            if (!empty($result1)) {
                # si encontramos, no buscamos más.
                if ($log) {
                    $log->log("ENCONTRADO_EN_1:" . $select1, \Phalcon\Logger::INFO);
                }
                $encontro = true;
            }
        }

        if (!$encontro) {
            $select1 = "SELECT q.question_id, q.category_id FROM questions q " .
                    "LEFT JOIN question_lang ql ON q.question_id = ql.question_id " .
                    "LEFT JOIN match_log ml ON (ml.user_id = {$user_id} AND ml.question_id = q.question_id ) " .
                    "WHERE (ml.question_id is NULL) AND (q.country_id = '{$country}' or q.country_id = '*') "
                    . "{$orderWhere} {$photo_where} {$category_where} AND ql.lang = '{$lang}' "
                    . "ORDER BY FIELD(q.category_id, $string_cat_rand), FIELD(q.country_id, '{$country}', '*') "
                    . "LIMIT 20;";
            $result1 = $db->fetchAll($select1, Phalcon\Db::FETCH_ASSOC);
            if (!empty($result1)) {
                # si encontramos, no buscamos más.
                if ($log) {
                    $log->log("ENCONTRADO_EN_1:" . $select1, \Phalcon\Logger::INFO);
                }
                $encontro = true;
            }
        }
        /*
          if (!$encontro){
          $select1 = "SELECT q.question_id, q.category_id FROM questions q ".
          "LEFT JOIN question_lang ql ON q.question_id = ql.question_id ".
          "LEFT JOIN match_log ml ON (ml.user_id = {$user_id} AND ml.question_id = q.question_id ) ".
          "WHERE (ml.question_id is NULL) AND (q.country_id = '{$country}' OR q.country_id = '*') ".
          "{$category_where} AND ql.lang = '{$lang}'  ORDER BY {$orderLevel} FIELD(q.category_id, $string_cat_rand), FIELD(q.country_id,'{$country}','*') LIMIT 20";
          # loguear primera busqueda.

          $result1 = $db->fetchAll($select1, Phalcon\Db::FETCH_ASSOC);
          if (!empty($result1)) {
          # si encontramos, no buscamos más.
          if ($log) {
          $log->log("ENCONTRADO_EN_2:".$select1, \Phalcon\Logger::INFO);
          }
          $encontro = true;
          }
          } */
        if (!$encontro) {
            # mostrar que no encontro la primera vez.
            $select1 = "SELECT q.question_id, q.category_id FROM questions q 
            LEFT JOIN question_lang ql ON q.question_id = ql.question_id  
             AND (q.country_id = '$country' or q.country_id = '*')  
               $category_where  "
                    . "WHERE ql.lang = '$lang'  "
                    . "ORDER BY FIELD(q.category_id, $string_cat_rand), "
                    . "FIELD(q.country_id,'$country','*'), q.question_id DESC LIMIT 20";
            $result1 = $db->fetchAll($select1, Phalcon\Db::FETCH_ASSOC);
            if (!empty($result1)) {
                # si encontramos, desordenamos 
                if ($log) {
                    $log->log("ENCONTRADO_EN_3:" . $select1, \Phalcon\Logger::INFO);
                }
                $encontro = true;
            }
        }

        if ($encontro) {
            foreach ($result1 as $q) {
                $q_arr[] = intval($q['question_id']);
            }
            shuffle($q_arr);
            $q_selected = $q_arr[0];
            $in_query = " = $q_selected ";
            $select = "SELECT q.question_id, q.question, q.level, q.category_id, q.answer_id, q.answer, q.is_correct , q.lang1 as 'lang', UNIX_TIMESTAMP(q.squared_image_at) as squared_image_id, UNIX_TIMESTAMP(q.rectangular_image_at) as rectangular_image_id, q.user_id 
            FROM questList q 
            WHERE q.question_id $in_query
            ORDER BY q.question_id 
            LIMIT $limit;";
            $data = $db->fetchAll($select, Phalcon\Db::FETCH_ASSOC);
            return $data;
        } else {
            return null;
        }
    }

}
