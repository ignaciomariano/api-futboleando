<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use \Phalcon\Db\RawValue;

class PushMessages extends \Phalcon\Mvc\Model {

    public $id;
    public $alert;
    public $match_id;
    public $user_id;
    public $device_id;
    public $dt;
    public $status;
    public $platform;
    
    
    public function beforeValidationOnCreate(){
        if(empty($this->dt)) $this->dt = new  RawValue('NOW()');
    }
    
    public static function findPushsToSend($interval = '60'){
        // A raw SQL statement
        $sql = "SELECT * FROM push_messages WHERE dt > DATE_SUB(NOW(), INTERVAL $interval SECOND) AND status = 'pendiente'";

        // Base model
        $push = new PushMessages();

        // Execute the query
        return new Resultset(null, $push, $push->getReadConnection()->query($sql));
    }
    
}
