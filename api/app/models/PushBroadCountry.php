<?php

class PushBroadCountry extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $push_broad_id;

    /**
     *
     * @var string
     */
    public $country_id;

    /**
     *
     * @var string
     */
    public $push_message;

}
