<?php

use \Phalcon\Db\RawValue;


class UserDevice extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_device_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $parse_device_id;

    /**
     *
     * @var string
     */
    public $native_device_id;
    
    /**
     *
     * @var string
     */
    public $platform;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    
    public function initialize(){
        $this->useDynamicUpdate(true);
    }
    
   public function beforeValidationOnCreate(){
        if(empty($this->created_at)) $this->created_at = new RawValue('default');
        if(empty($this->platform)) $this->platform = new RawValue('default');
    }
   public function beforeValidation(){
        $this->updated_at = new RawValue('NOW()');
    }
    
    
    
}
