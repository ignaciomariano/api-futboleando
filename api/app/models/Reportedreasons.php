<?php

class Reportedreasons extends \Phalcon\Mvc\Model {

    public static function getAll($lang) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT id, reasons, lang from reported_reasons where lang = :lang;";
        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('lang' => $lang));
        return $p;
    }

}
