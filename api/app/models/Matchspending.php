<?php

class Matchspending extends \Phalcon\Mvc\Model {

    public static function add($owner) {
        try {
            $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
            $query = "INSERT INTO matchs2 VALUES (NULL, :owner, '999999999', NOW(), 0, 0, 0, 'ES', :owner, 1, NOW(), 0, 0);";
            $p = $db->execute($query, array(
                'owner' => $owner
            ));
            if ($p) {
                return intval($db->lastInsertId());
            }
            return 0;
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public static function get() {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT* from matchs2 limit 1;";
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC);
        return $p;
    }

    public static function deleteMatch($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $success = $db->delete(
                "matchs2", "match_id = $match_id"
        );
        return $success;
    }

    public static function getToOwner($match_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = " SELECT m.*, u.* FROM matchs m INNER JOIN users u ON (u.user_id = m.owner_user_id OR u.user_id = m.guest_user_id)
            WHERE m.match_id = :match_id AND u.user_id = m.guest_user_id; ";
        $p = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('match_id' => $match_id));
        return $p;
    }

}
