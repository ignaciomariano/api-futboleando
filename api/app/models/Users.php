<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Users extends \Phalcon\Mvc\Model {

    public $user_id;
    public $email;
    public $username;
    public $passwd;
    public $fb_token;
    public $fb_data;
    public $created_on;
    public $phone_data;
    public $country_id;
    public $platform;
    public $device_id;
    public $gender;
    public $avatar;
    public $whistle_time;
    public $whistle_items;
    public $coins;
    public $show_ended;
    public $lang;
    public $latest_dt;

    
    public function initialize(){
        $this->hasMany('user_id', 'UserDevice', 'user_id');
    }
    
    #############3
    # getProfile functions

    public static function getProfileData($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT username, fb_data, fb_token, country_id, avatar FROM users where user_id = :user_id LIMIT 1";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array("user_id" => $user_id));
        return $result;
    }

    public static function getCategoryPerformance($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT q.category_id, COUNT(m.is_correct) as cant, m.is_correct as is_correct  
        FROM match_log m INNER JOIN questions q ON q.question_id = m.question_id 
        WHERE m.user_id = :user_id AND m.result_type = 0 
        GROUP BY q.category_id, m.is_correct 
        ORDER BY q.category_id ASC, m.is_correct ASC;";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array("user_id" => $user_id));
        return $result;
    }

    public static function getMatchPerformance($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT owner_user_id, guest_user_id, status, owner_points, guest_points, turn, winner 
        FROM matchs WHERE status > 1 AND owner_user_id = :user_id or guest_user_id = :user_id;";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array("user_id" => $user_id));
        return $result;
    }

    public static function getDuelPerformance($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT count(is_correct) as cant, is_correct FROM match_log WHERE result_type = 4 and user_id = :user_id GROUP BY is_correct ORDER BY is_correct ASC";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array("user_id" => $user_id));
        return $result;
    }

    ###############

    public static function getBillingSession($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT session_id FROM billing_celco WHERE user_id = :user_id AND status = 'PENDING' ORDER BY dt DESC LIMIT 1;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array("user_id" => $user_id));
        if (isset($result['session_id'])) {
            return $result['session_id'];
        } else {
            return null;
        }
    }

    public static function addBillingCelco($session_id, $user_id, $country_id, $billing_code, $coins = 0) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO billing_celco VALUES (null, {$user_id}, '{$billing_code}', {$coins} , '{$session_id}', 'PENDING', '$country_id', NOW());";
        #die($query);
        $success = $db->execute($query);
        return $success;
    }


    public static function getOneRandom($user_id) {
        # Evit the user_id in params
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        # si no hay facebook
        /*
          $query = "SELECT u.user_id, m.status FROM users u LEFT JOIN "
          . " matchs m ON ( u.user_id is not null AND m.guest_user_id = u.user_id AND m.status = 0 ) "
          . "WHERE u.user_id <> :user_id AND m.match_id is null ORDER BY u.user_id AND u.phone_data <> 'LOGOUT' DESC LIMIT 50; ";
          $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
          shuffle($result);
          foreach ($result as $u) {
          return intval($u['user_id']);
          }
         */
        $query = "SELECT u.user_id FROM users u WHERE u.user_id <> :user_id AND u.phone_data <> 'LOGOUT' ORDER BY u.user_id DESC LIMIT 200;";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
        shuffle($result);
        $n = 0;
        foreach ($result as $u) {
            $n++;
            $query2 = "SELECT COUNT(match_id) 'cuenta' FROM matchs WHERE guest_user_id = " . $u['user_id'] . " AND status = 0;";
            $result2 = $db->fetchOne($query2, Phalcon\Db::FETCH_ASSOC);
            if ((empty($result2)) || (isset($result2['cuenta']) && ($result2['cuenta'] == '0'))) {
                return intval($u['user_id']);
            }
            if ($n > 10) {
                return intval($u['user_id']);
            }
        }
        return 0;
    }

    public static function getByFbId($value) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT * FROM users WHERE fb_token = :value ORDER BY user_id DESC LIMIT 1;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array("value" => $value));
        return $result;
    }

    public static function getCountry($user_id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT country_id FROM users WHERE user_id = :user_id LIMIT 1;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));

        if (isset($result['country_id'])) {
            return $result['country_id'];
        }
        return 'AR';
    }

    public static function getByFacebookId($fb_id) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT user_id FROM users WHERE fb_token = :fb_token ORDER BY user_id DESC LIMIT 1;";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array('fb_token' => $fb_id));

        foreach ($result as $u) {
            return intval($u['user_id']);
        }
        return 0;
    }

    public static function getPrices() {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT * FROM prices;";
        $p = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC);
        return $p;
    }

    public static function logout($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET device_id = '', latest_dt = NOW(), phone_data = 'LOGOUT' WHERE user_id = $user_id;";
        $success = $db->execute($query);
        return $success;
    }

    public static function updateLatestDt($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET latest_dt = NOW() WHERE user_id = $user_id;";
        $success = $db->execute($query);
        return $success;
    }

    public static function buyLifes($user_id, $code) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT * FROM prices where id = :code;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('code' => $code));
        if ($result) {
            $coins = intval($result['cant_coins']);
            if ($coins) {
                if ($code == 'COMPLETE_LIFES') {
                    $change = "whistle_items = " . MAX_LIFES;
                } else {
                    $change = "whistle_items = whistle_items + 1";
                }
                $q = "UPDATE users SET $change,  coins = coins - $coins WHERE user_id = $user_id AND whistle_items < " . EXTRA_MAX_LIFES . " ;";
                $success = $db->execute($q);
                return $success;
            }
        }
        return false;
    }

    public static function purchaseOk($user_id, $code) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

        $query = "SELECT coins FROM billing_options  where billing_code = :code LIMIT 1;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('code' => $code));
        $coins = 0;

        if (isset($result['coins'])) {
            $coins = intval($result['coins']);
            # rest coins

            $query = "UPDATE users SET coins = coins + :coins WHERE user_id = :user_id;";
            $success1 = $db->execute($query, array(
                'coins' => $coins,
                'user_id' => $user_id
            ));
            if ($success1) {
                $query2 = "INSERT INTO billing_history VALUES (null, :user_id, NOW(), :code, '');";
                $success2 = $db->execute($query2, array(
                    'coins' => $coins,
                    'user_id' => $user_id
                ));
            }
            return $success1;
        }
        return false;
    }

    public static function putCoinHistory($user_id, $price_id, $cant, $coins) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "INSERT INTO coin_history VALUES (null, :price_id, :user_id, NOW(), :cant, :coins);";
        $success = $db->execute($query, array(
            'coins' => $coins,
            'user_id' => $user_id,
            'price_id' => $price_id,
            'cant' => $cant
        ));
        return $success;
    }

    public static function getCoinInfo($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db'); #SHARED_DB; 
        #var_dump($db);exit;
        #$db->connect(); // connect automatically on constructor, use for reconnect
        $query = "SELECT whistle_items, TIME_TO_SEC( TIMEDIFF(NOW() , whistle_time) ) as whistle_time, coins, show_ended FROM users WHERE user_id = :user_id;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
        return $result;
    }

    public static function getCoinsLifes($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        #$db->connect(); // connect automatically on constructor, use for reconnect
        $query = "SELECT  coins, whistle_items FROM users WHERE user_id = :user_id;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
        if (isset($result['coins'])) {
            return $result;
        }
        return null;
    }

    public static function updateCoins($user_id, $coins) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET coins = :coins WHERE user_id = :user_id; ";
        $success = $db->execute($query, array(
            'user_id' => $user_id,
            'coins' => $coins
        ));

        return $success;
    }

    public static function putLifes($user_id, $lifes) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET whistle_items = whistle_items+:lifes WHERE user_id = :user_id AND whistle_items < " . MAX_LIFES . ";";
        $success = $db->execute($query, array(
            'user_id' => $user_id,
            'lifes' => $lifes
        ));
        return $success;
    }

    public static function putCoins($user_id, $coins) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET coins=coins+:coins WHERE user_id = :user_id;";
        $success = $db->execute($query, array(
            'user_id' => $user_id,
            'coins' => $coins
        ));
        return $success;
    }

    public static function upgradeToFBAccount($user_id, $fb_token, $fb_data) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

        if ($fb_token) {
            $query = "UPDATE users SET gender='DIED', phone_data = 'LOGOUT' WHERE fb_token = :fb_token;";
            $success = $db->execute($query, array(
                'fb_token' => $fb_token
            ));
        }
        $query = "UPDATE users SET fb_data = :fb_data, fb_token = :fb_token, latest_dt = NOW() WHERE user_id = :user_id;";
        $success = $db->execute($query, array(
            'user_id' => $user_id,
            'fb_data' => $fb_data,
            'fb_token' => $fb_token
        ));
        return $success;
    }

    public static function getBillingOptions($country_id, $platform) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        #$db->connect(); // connect automatically on constructor, use for reconnect
        $query = "SELECT * FROM billing_options WHERE country_id = :country_id AND platform = :platform AND show_order < 10 ORDER BY show_order ASC;";
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array(
            'country_id' => $country_id,
            'platform' => $platform));
        if (!$result) {
            $query = "SELECT * FROM billing_options WHERE country_id = '*' AND platform = :platform AND show_order < 10 ORDER BY show_order ASC;";
            $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array(
                'platform' => $platform));
        }
        return $result;
    }

    public static function getById($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        #$db->connect(); // connect automatically on constructor, use for reconnect
        $query = "SELECT * FROM users WHERE user_id = :user_id;";
        $result = $db->fetchOne($query, Phalcon\Db::FETCH_ASSOC, array('user_id' => $user_id));
        return $result;
    }

    public static function updateFb($user_id, $fb_data) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $q = "UPDATE users SET phone_data = 'LOGIN', latest_dt = NOW(), fb_data = '$fb_data' WHERE user_id = $user_id;";
        $success = $db->execute($q);
        return $success;
    }

    public static function updateGuest($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $success = $db->update(
                "users", array("username"), array('Fut' . intval(intval(31000000) + intval($user_id))), "user_id = " . $user_id
        );
        return $success;
    }

    public static function search($user_id, $country_id, $search, $limit = 100) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        if (strlen($search) > 2) {
            $search_numbers = "";
            $search2 = "";
            if (strtolower(substr($search, 0, 3) == 'fut')) {
                if (is_numeric(strtolower(substr($search, 3, 25)))) {
                    $search2 = strtolower(substr($search, 3, 25));
                    $search_numbers = " OR user_id like '%{$search2}%' ";
                }
            } else {
                if (is_numeric($search)) {
                    $search_numbers = " OR user_id like '%{$search}%' ";
                }
            }

            $busqueda = (strlen($search) > 3) ? " AND ( fb_data like '%{$search}%' OR username like '%{$search}%' {$search_numbers} ) " : " AND ( username like '%{$search}%' {$search_numbers} ) ";
            $query = "SELECT * FROM users WHERE user_id <> :user_id $busqueda AND phone_data <> :condition_died ORDER BY user_id DESC LIMIT $limit";
        } else {
            # sugeridos
            $query = "SELECT * FROM users WHERE user_id <> :user_id AND phone_data <> :condition_died ORDER BY user_id DESC LIMIT $limit";
            /* futuros cambios
              SELECT u.*, m.status FROM users u LEFT JOIN
              matchs m ON ( m.guest_user_id = u.user_id AND m.status = 0 )
              WHERE u.user_id <> 35092 AND u.phone_data <> 'LOGOUT' AND m.match_id is null
              ORDER BY u.user_id DESC LIMIT 50;
             */
        }
        #die($query); 
        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array(
            'user_id' => $user_id,
            // 'country_id' => $country_id,
            'condition_died' => 'LOGOUT'
        ));
        return $result;
    }

    public static function searchFbUsers($user_id) {
        $limit = 100;
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "SELECT * FROM users WHERE user_id <> :user_id AND phone_data <> :condition_died AND LENGTH(fb_token)>0 ORDER BY user_id DESC LIMIT $limit";

        $result = $db->fetchAll($query, Phalcon\Db::FETCH_ASSOC, array(
            'user_id' => $user_id,
            'condition_died' => 'LOGOUT'
        ));
        return $result;
    }

    public static function add($info) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');

        $query = "INSERT  users VALUES (null, null, null, null, '{$info['fb_token']}', '{$info['fb_data']}',"
                . " NOW(), '{$info['phone_data']}', '{$info['country_id']}', '{$info['platform']}', '{$info['device_id']}', '{$info['gender']}', {$info['avatar']}, NOW(), 3, {$info['coins']}, 0, '{$info['lang']}', NOW());";
        $success = $db->execute($query);
        if ($success) {
            $id = $db->lastInsertId();
            return $id;
        }
        return 0;
    }

    public static function showEnded($user_id, $show_ended = 0) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET show_ended = {$show_ended} WHERE user_id = :user_id";
        $p = $db->execute($query, array(
            'user_id' => $user_id
        ));
        return $p;
    }

    public static function updateWhistle($user_id, $whistle_items) {

        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET whistle_time = NOW(), whistle_items = :whistle WHERE user_id = :user_id;";
        $p = $db->execute($query, array(
            'whistle' => $whistle_items,
            'user_id' => $user_id
        ));
        return $p;
    }

    public static function restLife($user_id) {
        $db = \Phalcon\DI\FactoryDefault::getDefault()->getShared('db');
        $query = "UPDATE users SET whistle_items = whistle_items-1, whistle_time = NOW() WHERE user_id = :user_id and whistle_items > 0;";
        $p = $db->execute($query, array(
            'user_id' => $user_id
        ));
        return $p;
    }

    
    
    public static function findByInactivity($interval, $lang){
        
        $country_sql = ($lang == 'ES') ? " <> 'BR' " : " = 'BR' ";
        
        // A raw SQL statement
        $sql = "SELECT * FROM users"
                . " WHERE latest_dt < DATE_SUB(NOW(), INTERVAL $interval SECOND)"
                . " AND country_id $country_sql"
                . " AND phone_data != 'LOGOUT'";
        

        // Base model
        $user = new Users();

        // Execute the query
        return new Resultset(null, $user, $user->getReadConnection()->query($sql));
    }

    public static function findByPendingMatch(){
    
        $query = "SELECT  user_id, fb_data, username,  latest_dt, device_id, platform, u.lang, country_id, datediff(now(),m.updated) as diff  FROM users u INNER JOIN matchs m on m.turn = u.user_id WHERE  u.phone_data != 'LOGOUT' AND LENGTH(u.device_id) > 1 and u.lang = 'ES' and u.country_id = 'AR' and m.status = 1 and datediff(now(),m.updated) >= 4 and datediff(now(),m.updated) <=5 ;";
    
        // Base model
        $user = new Users();

        // Execute the query
        return new Resultset(null, $user, $user->getReadConnection()->query($sql));
    }
    
}
