<?php

class PushUserSource extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $push_user_source_id;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $function;

}
