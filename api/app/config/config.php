<?php

define('BASE_DIR', realpath(__DIR__ . '/../'));

define('VERSION', '1.0.0');
define('DEFAULT_CANT_QUESTIONS', 100);
define('DEFAULT_CANT_QUESTIONS_BY_CATEGORY', 10);
define('QTD_USERS_RANDOM_DEFAULT', 30);
define('DEFAULT_LANG', 'ES');
define('PROBABILIDAD_CORONA', 15);
define('CATEGORIA_CORONA', 1000);
define('MAX_ENDED_MATCHS', 3);
define('MAX_LIFES', 3);
define('RESPONDIO_MAL_CAMBIA_TURNO', 0);
define('RESPONDIO_BIEN', 1);
define('RESPONDIO_BIEN_PIDE_DUELO', 2);
define('RESPONDIO_BIEN_GANO', 3);
define('RESPONDIO_DUELO', 4);
define('RESULT_TYPE_DUELO', 4);
define('STATUS_INVITATION', 0);
define('STATUS_INVITATION_REJECTED', 3);
define('STATUS_QUIT', 4);
define('STATUS_PLAYING', 1);
define('MINUTOS_VIDA', 15);
define('STATUS_ENDED', 2);
define('COINS_FOR_WIN_MATCH', 3);
define('DEFAULT_COINS', 50);
define('DEFAULT_WINDOWS_BR_COINS', 100);
define('EXTRA_MAX_LIFES', 25);

$config = include BASE_DIR . '/../../config.php';

$config['application'] = array(
        'controllersDir' => BASE_DIR . '/controllers',
        'modelsDir'      => BASE_DIR . '/models',
        'helpersDir'      => BASE_DIR . '/helpers',
        'pluginsDir'     => BASE_DIR . '/plugins',
        'libraryDir'     => BASE_DIR . '/library',
        'cacheDir'       => BASE_DIR . '/cache',
        'schemaCacheTimeout' => $config['api']['schemaCacheTimeout'],
        'viewsDir'       => BASE_DIR . '/views',
        'schemasDir'       => realpath(BASE_DIR . '/../schemas'),
        'baseUri'        => '/',
        'logDir'         => realpath(BASE_DIR . '/../logs')
);

$config['models'] = array(
    'metadata' => array(
        'adapter' => 'Apc'
    )
);

$config = new \Phalcon\Config($config);

return $config;