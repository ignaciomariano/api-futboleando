<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();


if(empty($config->log4php)){
    Logger::configure();
} else {
    Logger::configure($config->log4php->toArray());
}

$logger_services = Logger::getLogger('services');

if(php_sapi_name() != 'cli'){
    $logger_services->info('request: '.$_SERVER['REQUEST_URI']);
}



/**
 * The URL component is used to generate all kind of urls in the application
 */
 


$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);


$di->set('mediaConverterUrl', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->mediaConverterFrontEnd);

    return $url;
}, true);


/**
 * Setting up the view component
 */

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function() use ($di, $config) {
        #   $dbclass = '\Phalcon\Db\Adapter\\' . $config->database->adapter;
        # $configDb = (array) $config->database->params;
        # $configDb['options'] = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
        
        $db = new DbAdapter(array(
            'host' => $config->database->params->host,
            'username' => $config->database->params->username,
            'password' => $config->database->params->password,
            'dbname' => $config->database->params->dbname,
            'options' => $config->database->params->driver_options->toArray()
        ));

        $eventsManager = new Phalcon\Events\Manager();

        $profiler = new Phalcon\Db\Profiler();

        //Listen all the database events
        $eventsManager->attach('db', function($event, $db) use ($profiler) {

            $logger = Logger::getLogger('services');
            LoggerNDC::push("SQL");
            
            if ($event->getType() == 'beforeQuery') {
                $profiler->startProfile($db->getSQLStatement());
                if (!$vars = $db->getSQLVariables()) {
                    $vars = array();
                }
                $sql = preg_replace("/\s+/", ' ', trim($db->getSQLStatement()));
                $logger->debug($sql . ' (' . join(',', $vars) . ')');
            } elseif ($event->getType() == 'afterQuery') {
                $profiler->stopProfile();
                $last = $profiler->getLastProfile();
                $logger->debug('done (' . $last->getTotalElapsedSeconds() . ')');
            } elseif(in_array($event->getType(), array('beginTransaction', 'commitTransaction'))){
                $logger->debug($event->getType());
            }
            
            LoggerNDC::pop();
            
        });
        $db->setEventsManager($eventsManager);

        
        return $db;
    });

    
/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */

$di->set('modelsMetadata', function() use ($config) {
            $metadataAdapter = 'Phalcon\Mvc\Model\Metadata\Apc';
            return new $metadataAdapter();
});


/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di['config'] = $config;


$di->setShared('redis', function() use ($config, $logger_services) {
    $redis = new Redis();
    if($redis->pconnect( $config->redis->params->host , $config->redis->params->port )){
        $logger_services->info('Connected on redis '.$config->redis->params->host);
    } else {
        $logger_services->error('Connection to redis failed: '.$config->redis->params->host);
    }
	
    return $redis;
});


