<?php

class SuggestedController extends ControllerBase {

    public function addAction() {
        try {
            $this->load();
            $info = array(
                'question' => "'" . $this->dispatcher->getParam('question') . "'",
                'answer_correct' => "'" . $this->dispatcher->getParam('answer_correct') . "'",
                'answer_incorrect1' => "'" . $this->dispatcher->getParam('answer_incorrect1') . "'",
                'answer_incorrect2' => "'" . $this->dispatcher->getParam('answer_incorrect2') . "'",
                'answer_incorrect3' => "'" . $this->dispatcher->getParam('answer_incorrect3') . "'",
                'lang' => "'" . $this->dispatcher->getParam('lang') . "'",
                'category_id' => $this->dispatcher->getParam('category_id'),
                'country_id' => "'" . $this->dispatcher->getParam('country_id') . "'",
                'user_id' => $this->dispatcher->getParam('user_id'),
                'date' => "'" . date('Y-m-d H:i:s') . "'"
            );
            $data = Suggested::add($info);
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
