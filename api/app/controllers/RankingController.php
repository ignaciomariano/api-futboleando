<?php

class RankingController extends ControllerBase {

    /**
     * @var Logger
     */
    private $log;

    public function onConstruct() {
        parent::onConstruct();
        $this->log = Logger::getLogger('RankingController');
    }

    public function getranking() {
        $this->load();

        // $ini = intval($this->dispatcher->getParam('ini'));
        $ini = intval($this->request->get('ini'));
        $qtd = intval($this->request->get('qtd'));

        $ranking = Ranking::getRanking($ini, $qtd);

        foreach ($ranking as &$user) {

            $u = Users::getProfileData($user["id"]);
            $this->setShowname($u);

            $user['user_id'] = $user["id"];
            $user['position'] = $user["rank"];
            $user['showname'] = $u['showname'];
            
            if(!empty($u['fb_token'])){
                $user['fb_token']=$u['fb_token'];
            }


            unset($user["id"]);
            unset($user["rank"]);
        }

        return $this->response($ranking);
    }

    public function getRankingUser() {
        $this->load();

        // $ini = intval($this->dispatcher->getParam('ini'));
        $user_id = $this->request->get('user_id');
        $qtd = intval($this->request->get('qtd'));

        $ranking = Ranking::getUserPos($user_id, $qtd);

        $this->log->debug(print_r($ranking, 1) . "PORRA");
        foreach ($ranking as &$user) {

            $u = Users::getProfileData($user["id"]);
            $this->setShowname($u);

            $user['user_id'] = $user["id"];
            $user['position'] = $user["rank"];
            $user['fb_token'] = $u['fb_token'];
            $user['showname'] = $u['showname'];

            unset($user["id"]);
            unset($user["rank"]);
        }

        return $this->response($ranking);
    }

}
