<?php

class CountriesController extends ControllerBase {

    public function getallAction() {
        try {
            $this->load();
            $data = Countries::getAll();
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
