<?php

class ReportedController extends ControllerBase {

    public function getallAction() {
        try {
            $this->load();
            $lang = $this->dispatcher->getParam('lang');
            $data = Reported::getAll($lang);
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    public function addAction() {
        try {
            $this->load();
            $reported = new Reported();
            $reported->question_id = $this->dispatcher->getParam('question_id');
            $reported->user_id = $this->dispatcher->getParam('user_id');
            $reported->date = "'" . date('Y-m-d H:i:s') . "'";
            $reported->reported_reasons_id = $this->dispatcher->getParam('reported_reasons_id');
            $reported->reasons = $this->dispatcher->getParam('reason_text');
            $reported->checked = 0;
            $data = $reported->save();
            #$data = Reported::add($info);
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
