<?php

class OriginController extends ControllerBase {

    public function originAction() {
        try {
            $this->load();
            $ip = $this->request->getServer('REMOTE_ADDR');
            $origin = $this->dispatcher->getParam('origin');
            switch ($origin) {
                case "movistar_es_android":
                    $url = "https://play.google.com/store/apps/details?id=com.pmovil.android.apps.trivia&referrer=recomienda";
                    break;
                case "movistar_es_ios":
                    $url = "https://itunes.apple.com/us/app/futboleando/id908464739?l=es&ls=1&mt=8";
                    break;
                case "movistar_es_windows":
                    $url = "http://www.windowsphone.com/en-us/store/app/futboleando/170fd585-a32d-4b8f-bf66-284b030dc08e";
                    break;
                default:
                    $url = "http://www.futboleando.com";
            }
            $origin_id = Origin::add($ip, $origin);
            $this->response->redirect($url);
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
