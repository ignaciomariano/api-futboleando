<?php

use Phalcon\Mvc\Controller,
    Phalcon\Logger\Adapter\File as FileAdapter;

class ControllerBase extends \Phalcon\Mvc\Controller {

    public $request = null;
    protected $_timeInit = 0;

    /**
     * File Adapter for logging
     *
     * @var Logger
     * @access public
     */
    private $log = null;

    /**
     * Array for known the source from requests
     *
     * @var array
     * @access private
     */
    private $tokenList = Array('pmovil', 'ios', 'unity', 'android', 'web');

    /**
     * Strings for translate
     *
     * @var array
     * @access private
     */
    protected $config;
    private $strings = Array(
        'EN' => array(
            "#SHOWNAME# te ha invitado a jugar una partida!" => '#SHOWNAME# has invited you to play a game!',
            '#SHOWNAME# ha fallado. Ahora te toca jugar a ti!' => '#SHOWNAME# has failed. It\'s your turn to play',
            'Tienes nuevas invitaciones para jugar ahora!' => 'You have pending games. Play now!',
            '#SHOWNAME# Te ha desafiado a penales!' => '#SHOWNAME# has challenged you to penalty kicks!',
            'Has perdido la partida contra #SHOWNAME#' => 'You have lost the game against #SHOWNAME#',
            'Felicitaciones! Has ganado la partida contra #SHOWNAME#' => 'Congratulations! You have won the game against #SHOWNAME#',
            'Tú ganas el duelo de penales!' => 'You win the penalty kicks duel!',
            'Has perdido el duelo de penales. Es tu turno, recupera este personaje ahora!' => "You lost the penalty kicks duel. It's your turn. Retrieve this character now!",
            'Restan' => 'Remaining',
            'Finalizado' => 'Ended',
            'dias' => 'days',
            'minutos' => 'minutes',
            'horas' => 'hours',
            'hora' => 'hour',
            'No te quedes sin monedas!' => "Don't get out of coins!",
            'Mira las ofertas que tenemos para ti' => 'Check it out these special offers for you'),
        'PT' => array(
            '#SHOWNAME# te ha invitado a jugar una partida!' => "#SHOWNAME# lhe convidou para um torneio!",
            '#SHOWNAME# ha fallado. Ahora te toca jugar a ti!' => '#SHOWNAME# falhou. Agora é a sua vez de jogar!',
            'Tienes nuevas invitaciones para jugar ahora!' => 'Você têm novos convites para jogar agora!',
            '#SHOWNAME# Te ha desafiado a penales!' => '#SHOWNAME# te desafiou para os Pênaltis!',
            'Has perdido la partida contra #SHOWNAME#' => 'Você perdeu a partida para #SHOWNAME#',
            'Felicitaciones! Has ganado la partida contra #SHOWNAME#' => 'Parabéns! Você ganhou a partida para "#SHOWNAME#',
            'Tú ganas el duelo de penales!' => 'Você ganhou o duelo de pênaltis!',
            'Has perdido el duelo de penales. Es tu turno, recupera este personaje ahora!' => 'Você perdeu o duelo de pênaltis. Agora é seu turno. Recupere seu personagem agora! ',
            'Restan ' => 'Faltam ',
            'No te quedes sin monedas!' => 'Não fique sem moedas!',
            'Mira las ofertas que tenemos para ti' => 'Confira ofertas especiais para você'
        )
    );

    public function onConstruct() {
        $this->log = Logger::getLogger('ControllerBase');
        $this->response->setStatusCode(200, "Ok");
        $this->config = $this->getDI()->get('config');
    }

    /**
     * Translate
     *
     * Intent to translate any word from with $string array dictionary
     *
     * @access public
     */
    public function lang($string, $lang) {
        if (empty($lang)) {
            $lang = DEFAULT_LANG;
        }

        if (($lang == 'ES') || (empty($this->strings[$lang][$string]))) {
            return $string;
        }

        return $this->strings[$lang][$string];
    }

    /**
     * RemoveUnusedFields
     *
     * Remove private fields from users
     *
     * @access public
     */
    public function removeUnusedFields(&$data) {

        unset($data['phone_data']);
        unset($data['country_id']);
        unset($data['platform']);
        unset($data['device_id']);
        unset($data['gender']);
        unset($data['passwd']);
        unset($data['username']);
        unset($data['created_on']);
        unset($data['last_result_type']);
        unset($data['updated']);
        unset($data['fb_data']);
        unset($data['email']);
    }

    /*
     * addPush
     * 
     * Put Message in Queue for sending.
     */

    public function addPush($msg, $match_id, UserDevice $device) {
        LoggerNDC::push('PUSH');
        $platform = (!empty($platform)) ? $platform : 'ANDROID';

        $pushMsg = new PushMessages();
        $pushMsg->user_id = $device->user_id;
        $pushMsg->match_id = $match_id;
        $pushMsg->device_id = (empty($device->parse_device_id)? $device->native_device_id: $device->parse_device_id);
        $pushMsg->platform = $device->platform;
        $pushMsg->alert = $msg;

        if (!empty($msg) && !empty($device) && !empty($pushMsg->device_id) && $msg != 'null') {

            $pusher = new Push($this->config->pushs->toArray());
            $mode = ($device->platform == 'ANDROID') ? 'GCM' : 'PARSE';
            $mode = (strlen($pushMsg->device_id) == 10) ? 'PARSE' : $mode;
            $mode = ($device->platform == 'IOS') ? "IOS" : $mode;

            $response = $pusher->send($mode, $pushMsg->device_id, $msg, 0, $match_id);
            //TODO: Rever esta parte - lo user_id que llega no es del destino, y si del originador del push
            /*
            if ($mode == 'GCM') {
                $resp_array = json_decode($response);
                $results = $resp_array->results;
                foreach ($results as $device_response) {
                    if (!is_array($device_response)) {
                        if ((isset($device_response->error)) && $device_response->error == "NotRegistered") {
                            $this->logger->info("Unregistered user:$user_id");
                            $user = Users::findFirst($user_id);
                            $user->phone_data = 'LOGOUT';
                            $user->gender = 'GCM-LOCK';
                            $user->save();
                        }
                    }
                }
                $pushMsg->status = 'cancelado';
            } else {*/
                $pushMsg->status = 'enviado';
            //}
        } else {
            $pushMsg->status = 'cancelado';
        }
        $pushMsg->save();
        LoggerNDC::pop();
        return true;
    }

    /**
     * Set Showname
     *
     * Put the showname in array
     *
     * @access public
     */
    public function setShowname(&$friend) {

        $noname = 1;
        $fb_name = 'guest';
        if (!empty($friend['fb_data'])) {
            #die('hay fb_data');
            $arr_fb_data = explode(",", $friend['fb_data']);

            if (isset($arr_fb_data[2])) {
                $fb_name = $arr_fb_data[2];

                $noname = 0;
            }
        }
        if ($noname) {
            if (!empty($friend['username'])) {
                $fb_name = $friend['username'];
            }
        }
        $friend['showname'] = $fb_name;
    }

    /**
     * responseError
     *
     * Show an error and exit
     *
     * @access public
     */
    public function responseError($msg, $code = 0) {
        $json = array(
            'status' => 'ERROR',
            'error_number' => $code,
            'error' => $msg);
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent($json);
        $this->log->error($this->request->getURI() . "::" . var_export($json, true));
        return $this->response;
    }

    /**
     * Read Params
     *
     * Read & Transform the request params to friendly-url
     *
     * @access public
     */
    private function readParams() {
        $this->request = new \Phalcon\Http\Request();
        $s = $this->request->getURI();
        $paramsBefore = explode("/", $s);
        foreach ($paramsBefore as &$param) {
            $param = urldecode($param);
        }
        $paramsAfter = array();
        for ($i = 1; $i < count($paramsBefore); $i += 2) {
            if ($i > 2) {
                if (isset($paramsBefore[$i + 1])) {
                    $paramsAfter[$paramsBefore[$i]] = $paramsBefore[$i + 1];
                } else {
                    $paramsAfter[$paramsBefore[$i]] = "";
                }
            }
        }

        /* $paramsBefore = $this->dispatcher->getParams(); 
          if (count($paramsBefore) > 0) {
          for ($i = 0; $i < count($paramsBefore); $i += 2) {
          if (isset($paramsBefore[$i + 1])) {
          $paramsAfter[$paramsBefore[$i]] = $paramsBefore[$i + 1];
          }
          }

         */
        $this->dispatcher->setParams($paramsAfter);
    }

    /**
     * Load Controller Base
     *
     * All functions before begin an Action
     *
     * @access public
     */
    public function load() {
        try {
            $this->readParams();

            $token = $this->dispatcher->getParam('token');

            if (!$this->isValidToken($token)) {
                #   $this->responseError("Wrong or missing token");
            }


            #      $this->log->log("Request:" . $this->request->getURI(), \Phalcon\Logger::DEBUG);
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * HTTP Response
     *
     * Response the http request with any data
     *
     * @access public
     */
    public function response($json) {
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent($json);
        $this->log->info($this->request->getURI() . "::" . strlen($this->response->getContent()) . " bytes");
        $this->log->debug($this->response->getContent());
        return $this->response;
    }

    /**
     * isValidToken
     *
     * return true if token is valid;
     *
     * @access public
     */
    private function isValidToken($token) {
        return in_array($token, $this->tokenList);
    }

    /**
     * getSplash
     *
     * return help splash info
     *
     * @access public
     */
    public function getSplash($n) {
        switch ($n) {
            case 4:
                return "POWER_UP_DROP_2_ANSWERS";
                break;
            case 5:
                return "POWER_UP_2_CLICK_ANSWER";
                break;
            case 6:
                return "POWER_UP_NEW_QUESTION";
                break;
            default:
                return "";
                break;
        }
    }

}
