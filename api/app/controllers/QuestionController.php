<?php

use Phalcon\Logger\Adapter\File as FileAdapter;

#use Models\Users;

class QuestionController extends ControllerBase {

    /**
     * @var Logger
     */
    private $log;

    public function onConstruct() {
        parent::onConstruct();
        $this->log = Logger::getLogger('QuestionController');
    }

    public function getLog($match_id) {
        $data = Matchs::getLog($match_id);
        #print_r($data);
        #exit;
        if (!empty($data)) {
            $detail = Array(
                'status' => $data[0]['status'],
                'duel_id' => 0,
                'ronda' => 1,
                'owner' => array(
                    'balls' => 0,
                    'id' => intval($data[0]['owner_user_id']),
                    'points' => 0,
                    'avatarstr' => '000000',
                    'avatars' => array(
                        'avatar1' => 0, 'avatar2' => 0, 'avatar3' => 0, 'avatar4' => 0, 'avatar5' => 0, 'avatar6' => 0
                    )
                ),
                'guest' => array(
                    'balls' => 0,
                    'id' => intval($data[0]['guest_user_id']),
                    'points' => 0,
                    'avatarstr' => '000000',
                    'avatars' => array(
                        'avatar1' => 0, 'avatar2' => 0, 'avatar3' => 0, 'avatar4' => 0, 'avatar5' => 0, 'avatar6' => 0
                    )
                )
            );


            $ronda = 1;
            #     print_r($data);
            #   exit;
            foreach ($data as $row) {

                if (intval($row['duel_id']) == 0) {
                    # si es owner
                    if ($row['user_id'] == $row['owner_user_id']) {
                        #if pregunta normal
                        if ($row['category_id'] == 0) {
                            if (intval($row['is_correct']) == 1) {
                                $detail['owner']['balls'] = ($detail['owner']['balls'] == 3) ? 1 : $detail['owner']['balls'] + 1;
                            }
                        } else {
                            #pregunta por corona
                            if (intval($row['is_correct']) == 1) {
                                if ($detail['owner']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['owner']['points'] = intval($detail['owner']['points']) + 1;
                                }
                                $detail['owner']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['owner']['avatarstr'][$row['category_id'] - 1] = "1";
                            }
                            $detail['owner']['balls'] = 0;
                        }
                    } else {
                        #if pregunta normal

                        if (intval($row['is_correct'] == 0)) {
                            $ronda++;
                        }
                        if ($row['category_id'] == 0) {
                            if (intval($row['is_correct']) == 1) {
                                $detail['guest']['balls'] = ($detail['guest']['balls'] == 3) ? 1 : $detail['guest']['balls'] + 1;
                            } else {
                                #  $ronda++;
                            }
                        } else {
                            #pregunta por corona 
                            if (intval($row['is_correct']) == 1) {
                                if ($detail['guest']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['guest']['points'] = intval($detail['guest']['points']) + 1;
                                }

                                $detail['guest']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['guest']['avatarstr'][$row['category_id'] - 1] = "1";
                            }
                            $detail['guest']['balls'] = 0;
                        }
                    }
                } else {
                    #es duelo
                    #y esta ganando o perdiendo avatar
                    if ($row['result_type'] == "4") {
                        if ($row['user_id'] == $row['owner_user_id']) {
                            $detail['owner']['balls'] = 0;

                            if (intval($row['is_correct']) == 1) {
                                if ($detail['owner']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['owner']['points'] = intval($detail['owner']['points']) + 1;
                                }

                                $detail['owner']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['owner']['avatarstr'][$row['category_id'] - 1] = "1";
                            } else {
                                if ($detail['owner']['avatars']['avatar' . $row['category_id']] > 0) {
                                    $detail['owner']['points'] = intval($detail['owner']['points']) - 1;
                                }

                                $detail['owner']['avatars']['avatar' . $row['category_id']] = 0;
                                $detail['owner']['avatarstr'][$row['category_id'] - 1] = "0";
                            }
                        } else {
                            #is guest
                            $detail['guest']['balls'] = 0;
                            $ronda++;
                            if (intval($row['is_correct']) == 1) {

                                if ($detail['guest']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['guest']['points'] = intval($detail['guest']['points']) + 1;
                                }

                                $detail['guest']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['guest']['avatarstr'][$row['category_id'] - 1] = "1";
                            } else {
                                if ($detail['guest']['avatars']['avatar' . $row['category_id']] > 0) {
                                    $detail['guest']['points'] = intval($detail['guest']['points']) - 1;
                                }

                                $detail['guest']['avatars']['avatar' . $row['category_id']] = 0;
                                $detail['guest']['avatarstr'][$row['category_id'] - 1] = "0";
                            }
                        }
                    }
                }
            }
            $detail['ronda'] = $ronda;
            return $detail;
        } else {

            $match_i = Matchs::getInfo($match_id);
            if ($match_i) {
                $ow_id = $match_i['owner_user_id'];
                $gu_id = $match_i['guest_user_id'];
                $detail = Array(
                    'duel_id' => 0,
                    'ronda' => 1,
                    'owner' => array(
                        'balls' => 0,
                        'id' => $ow_id,
                        'points' => 0,
                        'avatarstr' => '000000',
                        'avatars' => array(
                            'avatar1' => 0, 'avatar2' => 0, 'avatar3' => 0, 'avatar4' => 0, 'avatar5' => 0, 'avatar6' => 0
                        )
                    ),
                    'guest' => array(
                        'balls' => 0,
                        'id' => $gu_id,
                        'points' => 0,
                        'avatarstr' => '000000',
                        'avatars' => array(
                            'avatar1' => 0, 'avatar2' => 0, 'avatar3' => 0, 'avatar4' => 0, 'avatar5' => 0, 'avatar6' => 0
                        )
                    )
                );
            } else {
                $detail = array();
            }




            return $detail;
        }
    }

    public function getDuelQuestions($duel_id, $match_id, $user_id) {
        $result = Duels::getDuelQuestions($duel_id, $match_id, $user_id);

        $contest = 0;
        $cambio_turno = 0;
        $end = 0;
        $nonull = 0;
        $question_number_duel = 0;
        foreach ($result as $row) {
            $question_number_duel++;
            $nonull = 1;
            if ($row['answer_id'] == 'null' || $row['answer_id'] == null || $row['answer_id'] == '') {
                $contest = $row['question_id'];
                break;
            }
            if ($row['created_by'] == $user_id) {
                $cambio_turno = 1;
            }
        }
        if ($contest == 0) {
            $end = ($cambio_turno == 0) ? 1 : 0;
        }
        if (!$nonull) {
            return null;
        }
        return array('to_answer' => $contest, 'cambio_turno' => $cambio_turno, 'end' => $end, 'question_number_duel' => $question_number_duel);
    }

    private function calculateDetails($match_id) {


        $result = $this->getLog($match_id);
        return $result;
    }

    private function getQuestionNro($question_id) {

        $data = Questions::getId($question_id);
        $return = array('questions' => array());

        if ($data) {

            foreach ($data as $answer) {

                $respuesta = Array();

                $respuesta['answer'] = $answer['answer'];
                $respuesta['answer_id'] = $answer['answer_id'];
                $respuesta['is_correct'] = $answer['is_correct'];
                $pregunta = $answer;

                unset($pregunta['answer']);
                unset($pregunta['is_correct']);
                unset($pregunta['answer_id']);
#   unset($pregunta['answer_id']);
                if (isset($return['questions'][$answer['question_id']])) {
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                    if ($respuesta['is_correct'] == "1") {
                        $return['questions'][$answer['question_id']]['answer_correct'] = $respuesta['answer_id'];
                    }
                } else {
                    $return['questions'][$answer['question_id']] = $pregunta;
                    $return['questions'][$answer['question_id']]['answers'] = Array();
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                    if ($respuesta['is_correct'] == "1") {
                        $return['questions'][$answer['question_id']]['answer_correct'] = $respuesta['answer_id'];
                    }
                }
            }
            shuffle($return['questions']);
        } else {
            return null;
        }
        return $return['questions'];
    }

    private function finishDuel($duelId) {

        $status = 1;
        $success = Duels::updateStatus($duelId, $status);
        return $success;
    }

    private function duelWinner($duelId) {
        try {
            $correctas1 = 0;
            $correctas2 = 0;

            $result = Duels::getWinner($duelId);

            foreach ($result as $r) {
                $user_id1 = intval($r['owner_user_id']);
                $correctas1 = intval($r['correctas']);
                $created_by = intval($r['created_by']);
                $win_cat = intval($r['win_cat']);
                $lost_cat = intval($r['lost_cat']);
            }
            # get guest data
            $result_guest = Duels::getGuest($duelId);

            foreach ($result_guest as $r) {
                $user_id2 = intval($r['guest_user_id']);
                $correctas2 = intval($r['correctas']);
            }

            if ($correctas1 > $correctas2) {
                $ganador = $user_id1;
            } elseif ($correctas2 > $correctas1) {
                $ganador = $user_id2;
            } else {
                $ganador = $created_by;
            }
            $return = array(
                'duel_id' => $duelId,
                'win_id' => $ganador,
                'owner_user_id' => $user_id1,
                'guest_user_id' => $user_id2,
                'owner_correctas' => $correctas1,
                'guest_correctas' => $correctas2,
                'created_by' => $created_by,
                'win_category' => $win_cat,
                'lost_category' => $lost_cat
            );

            return $return;
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function updateMatch($info) {

        $update = "UPDATE matchs "
                . "SET updated = NOW() ";
        if (isset($info['owner_points'])) {
            $update .= " , owner_points = " . intval($info['owner_points']) . " ";
        }
        if (isset($info['guest_points'])) {
            $update.= " , guest_points = " . intval($info['guest_points']);
        }
        if (isset($info['ronda'])) {
            $update.= " , ronda = " . intval($info['ronda']) . " ";
        }
        if (isset($info['change_turn']) && intval($info['change_turn']) == 1) {
            if ($info['user_type'] == 'guest') {
                $update.= " , turn = owner_user_id ";
            } else {
                $update.= " , turn = guest_user_id ";
            }
        }
        if ($info['change_status']) {
            if (isset($info['status'])) {
                $update.= ", status =  " . intval($info['status']);
            }
        }
        if (isset($info['last_result_type'])) {
            $update.=", last_result_type = " . $info['last_result_type'];
        }
        $update.= " WHERE match_id = " . $info['match_id'] . ";";
        #Zend_Registry::get('log2')->log(' QUERY_UPDATE ' . $update, Zend_Log::INFO);
        $success = Matchs::queryUpdate($update);
        return $success;
    }

    /*
     * Answer one Question
     * 
     */

    public function putAction() {
        try {
            $this->load();


            $user_id = $user_winner = intval($this->dispatcher->getParam('user_id'));
            $powerup = $this->dispatcher->getParam('powerup');
            if (empty($powerup)) {
                $powerup = 'X';
            }
            $match_id = intval($this->dispatcher->getParam('match_id'));
            $duelId = $this->dispatcher->getParam('duel_id');
            if (empty($duelId)) {
                $duelId = 0;
            }
            $result_type = intval($this->dispatcher->getParam('result_type'));

            if (empty($match_id)) {
                throw new \Phalcon\Exception('Missing match_id', 101);
            }
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 101);
            }

            $match = Matchs::findFirst($match_id);

            $user_type = $match->getUserType($user_id);
            $send_push = false;
            $detalle_duelo = "";
            $respuestas = array();
            $qtd_answers = 0;
            $category = 0;
            $is_correct = 0;
            $extra = null;

            $request = $this->dispatcher->getParams();


            foreach ($request as $name => $val) {
                $pieces = explode("-", $val);
                if (strpos($name, '_q') !== FALSE) {
                    $qtd_answers++;
                    $question_id = intval(str_replace('_q', '', $name));
                    $answer_id = intval($pieces[0]);
                    $category = intval($pieces[1]);
                    $is_correct = Questions::isCorrect($question_id, $answer_id);
                    if ($is_correct === null) {
                        $is_correct = (intval($pieces[2]) == "1") ? 1 : 0;
                    }
                    $time_in_seconds = intval($pieces[3]);

                    $respuestas[] = Array(
                        'match_id' => $match_id,
                        'user_id' => $user_id,
                        'result_type' => $result_type,
                        'question_id' => $question_id,
                        'answer_id' => $answer_id,
                        'category_id' => $category,
                        'is_correct' => $is_correct,
                        'time_in_seconds' => $time_in_seconds,
                        'duel_id' => $duelId
                    );
                }
            }

            $change_turn = 0;
            if ($result_type < 2 && !$is_correct && $duelId == 0) { #RESPONDIO_MAL_CAMBIA_TURNO) {
                $change_turn = 1;
                $detalle_duelo = "TURN_END";
                $send_push = true;
            }

# enviar rtas
            $affected = $this->uploadAnswers($respuestas);
            if (count($respuestas)) {
                @Users::updateLatestDt($user_id);
            }
            foreach ($respuestas as $r) {
                if (intval($r['duel_id']) > 0) {


                    $duelId = intval($r['duel_id']);
                    $duelQuestions = $this->getDuelQuestions($duelId, $match_id, $user_id);


                    if (isset($duelQuestions['to_answer'])) {

                        $contest = intval($duelQuestions['to_answer']);
                        $end = intval($duelQuestions['end']);
                        $cambio_turno = intval($duelQuestions['cambio_turno']);
                        if ($contest == 0) {
                            if ($cambio_turno == 1) {
                                $change_turn = 1;
                                $detalle_duelo = "DUEL_TURN";
                                $send_push = true;
                            } else {
                                if ($end == 1) {
                                    $this->finishDuel($duelId); #$change_turn = 1;

                                    $arr = $this->duelWinner($duelId);
                                    #       Zend_Registry::get('logd')->log(var_export($arr, true), Zend_Log::INFO);

                                    $win = $arr['win_id'];
                                    # crear match_log para ganar o perder el duelo (??)
                                    $this->saveDuelInLog($match_id, $arr);
                                    $send_push = true;

                                    if ($win == $user_id) {
                                        # ganó el usuario que acaba de contestar
                                        $detalle_duelo = "DUEL_FINISH_WIN";
                                    } else {
                                        $change_turn = 1;
                                        $detalle_duelo = "DUEL_FINISH_LOST";
                                    }



                                    #     Zend_Registry::get('logd')->log("cambiar turno $change_turn - detalle duelo:" . $detalle_duelo, Zend_Log::INFO);
                                }
                                // Poner turno al ganador
                            }
                        }
                    } else {
                        # erro en duelo  - no existe
                    }
                }
            }
            $debug = $detalle_duelo;


            #$debug = 0;
            # nos fijamos si cambió el puntaje de la partida
            $details = $this->calculateDetails($match_id);

            $status_partida = intval($details['status']);
            $owner_points = 'owner_points';
            $guest_points = 'guest_points';
            if (isset($details['owner']['points'])) {
                $owner_points = intval($details['owner']['points']);
            }
            if (isset($details['guest']['points'])) {
                $guest_points = intval($details['guest']['points']);
            }

            $enemy = Users::findFirst($match->getRivalUserId($user_id));
            $dataEnemy = $enemy->toArray();
            $this->setShowname($dataEnemy);
            #actualizar match

            $change_status = false;
            $status_match = STATUS_PLAYING;
            if ($owner_points == 6 || $guest_points == 6) {
                $status_match = STATUS_ENDED;
                $change_status = true;
                $send_push = true;
                # cargar monedas al ganador:

                if ($user_type == 'owner' && $owner_points == 6 || $user_type == 'guest' && $guest_points == 6) {
                    $user_winner = $user_id;
                    $code = "MATCH_FINISH_WIN";
                    $debug = 'MATCH_FINISH_WIN';
                    if ($user_type == 'guest') {
                        $enemyPoints = $owner_points;
                    } else {
                        $enemyPoints = $guest_points;
                    }
                    $extra = array(
                        'coins' => COINS_FOR_WIN_MATCH,
                        'fb_token' => $dataEnemy['fb_token'],
                        'showname' => $dataEnemy['showname'],
                        'avatar' => $dataEnemy['avatar'],
                        'tu' => "6",
                        'rival' => "{$enemyPoints}"
                    );
                } else {

                    $user_winner = intval($dataEnemy['user_id']);
                    $code = "MATCH_FINISH_LOST";
                    $debug = 'MATCH_FINISH_LOST';
                }

                Users::putCoins($user_winner, COINS_FOR_WIN_MATCH);
                Users::putCoinHistory($user_winner, 'MATCH_WIN', 1, COINS_FOR_WIN_MATCH);
            }
            #         die("ow: $owner_points gu: $guest_points ");
            $ronda = (isset($details['ronda'])) ? intval($details['ronda']) : 1;
            # Actualiza la partida el turno
            $info = array(
                'match_id' => $match_id,
                'user_id' => $user_id,
                'owner_points' => $owner_points,
                'guest_points' => $guest_points,
                'ronda' => $ronda,
                'user_type' => $user_type,
                'status' => $status_match,
                'winner' => $user_winner,
                'change_status' => $change_status,
                'last_result_type' => intval($result_type),
                'change_turn' => $change_turn
            );
            $this->updateMatch($info);

            # Push
            $ud = Users::getById($user_id);
            if ($ud) {
                $this->setShowname($ud);
            }


            # son siempre para el rival.
            # entonces le vamos a enviar el nombre de mi usuario:
            if ($send_push) {
                if (is_array($dataEnemy)) {

                    $devices = $enemy->userDevice;
                    #print_r($devices);exit;
                    foreach ($devices as $udevice) {
                        $showname = $ud['showname'];

                        $langPush = ($dataEnemy['country_id'] == 'BR') ? 'PT' : 'ES';

                        switch ($debug) {
                            case 'MATCH_FINISH_WIN':
                                $mensajePush = 'Has perdido la partida contra #SHOWNAME#';
                                $mensajePush = $this->lang($mensajePush, $langPush);
                                $mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);
                                $this->showEndedByMatch($match_id);
                                break;
                            case 'MATCH_FINISH_LOST':
                                $mensajePush = 'Felicitaciones! Has ganado la partida contra #SHOWNAME#';
                                $mensajePush = $this->lang($mensajePush, $langPush);
                                $mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);
                                $this->showEndedByMatch($match_id);
                                break;
                            case 'DUEL_FINISH_WIN':
                                #  $mensajePush = 'Has perdido el duelo de penales. Es tu turno, recupera este personaje ahora!';
                                #  $mensajePush = $this->lang($mensajePush, $langPush);
                                #### #$mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);  
                                break;
                            case 'DUEL_FINISH_LOST':
                                $mensajePush = 'Tú ganas el duelo de penales!';
                                $mensajePush = $this->lang($mensajePush, $langPush);
                                #$mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);  
                                break;
                            case 'DUEL_TURN':
                                $mensajePush = '#SHOWNAME# te ha desafiado a penales!';
                                $mensajePush = $this->lang($mensajePush, $langPush);
                                $mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);
                                break;
                            default:
                                if ($status_partida > 0) {
                                    #$mensajePush = '#SHOWNAME# fallaron. Ahora te toca jugar a ti!';
                                    $mensajePush = '#SHOWNAME# ha fallado. Ahora te toca jugar a ti!';
                                    $mensajePush = $this->lang($mensajePush, $langPush);
                                    $mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);
                                    /*
                                      $namesPush = Matchs::getMyTurn($user_id);
                                      $names_array = array();
                                      foreach ($namesPush as $namePush) {
                                      if ($user_id == $namePush['owner_user_id']) {
                                      if (isset($namePush['guest'])) {
                                      $parts = explode(",", $namePush['guest']);
                                      $names_array[] = $parts[2];
                                      } else {
                                      $names_array[] = $namePush['guest_username'];
                                      }
                                      } else {
                                      if (isset($namePush['owner'])) {
                                      $parts = explode(",", $namePush['owner']);
                                      $names_array[] = $parts[2];
                                      } else {
                                      $names_array[] = $namePush['owner_username'];
                                      }
                                      }
                                      }
                                      $names = implode(", ", $names_array);
                                      $mensajePush = str_replace('#SHOWNAME#', $names, $mensajePush);
                                      #print_r($mensajePush);exit;
                                     */
                                } else {
                                    $mensajePush = "#SHOWNAME# te ha invitado a jugar una partida!";
                                    $mensajePush = $this->lang($mensajePush, $langPush);
                                    $mensajePush = str_replace('#SHOWNAME#', $showname, $mensajePush);
                                }
                                break;
                        }
                        $this->addPush($mensajePush, $match_id, $udevice);
                    }
                } else {
                    $this->log->error("NO_PUSH::no_enemy::" . var_export($dataEnemy, true));
                }
            }


            $debug = array('debug' => $debug);

            # ok

            /*
             *  HACER EL COBRO DE POWER UP
             */

            $user_info = $this->getUserCoinInfo($user_id);
            $coins = intval($user_info['coins']);
            if (!empty($powerup) && $powerup != 'X') {

                if ($coins) {

                    $coins_nuevas = $this->cobrarPowerUp($powerup, $user_id, $coins);
                    if ($coins_nuevas !== false) {
                        $user_info['coins'] = $coins_nuevas;
                        Users::putCoinHistory($user_id, $powerup, 1, intval($user_info['power_ups']['PRICE_' . $powerup]));
                        ### ADJUNTAR INFO DE COINS ##
                        $user_info = $this->getUserCoinInfo($user_id);
                    } else {
                        $this->log->error("No se cobro power up {$user_id}:{$coins}");
                        #      throw new \Phalcon\Exception("User haven't enough coins", 101);
                    }
                } else {
                    $this->log->error("No tiene coins {$user_id}:{$coins}");
                    #    throw new \Phalcon\Exception("User haven't coins", 101);
                }
            }




            $json = array('status' => 'OK', 'data' => $debug, 'extra' => $extra);
            return $this->response($json);
        } catch (\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function showEndedByMatch($match_id) {
        $data = Matchs::getMatch($match_id);
        if ($data) {
            $this->showEnded1($data['owner_user_id'], $data['guest_user_id']);
        } else {
            return 0;
        }
    }

    private function showEnded1($u1, $u2 = null) {
        Users::showEnded(intval($u1), 1);
        if (intval($u2)) {
            Users::showEnded(intval($u2), 1);
        }
    }

    private function saveDuelInLog($match_id, $arr) {

# arrancamos por el ganador
        $info = array();


#       Zend_Registry::get('logd')->log(var_export($arr, true), Zend_Log::INFO);
        $is_correct_owner = 0;
        $is_correct_guest = 0;
        if ($arr['win_id'] == $arr['created_by']) {
            #    Zend_Registry::get('logd')->log("creador = win", Zend_Log::INFO);
            $category_owner = $arr['win_category'];
            $category_guest = $arr['win_category'];
            # gano el creador del duelo
            # si es el owner del match
            if ($arr['owner_user_id'] == $arr['win_id']) {
                #     Zend_Registry::get('logd')->log("owner = win", Zend_Log::INFO);
                $is_correct_owner = 1;
            } else {
                #si ganó el creador, y es guest 
                $is_correct_guest = 1;
            }
            # guardar match_log para ambos
        } else {
            $category_owner = $arr['lost_category'];
            $category_guest = $arr['lost_category'];
            #      Zend_Registry::get('logd')->log("creador != win", Zend_Log::INFO);
            # gano el retador del duelo y es owner
            if ($arr['owner_user_id'] == $arr['win_id']) {
                #     Zend_Registry::get('logd')->log("guest = win", Zend_Log::INFO);
                $is_correct_owner = 1;
            } else {
                #gano el retador del duelo y es guest  
                $is_correct_guest = 1;
            }
        }


        $info['user_id'] = $arr['owner_user_id'];
        $info['category_id'] = $category_owner;
        $info['is_correct'] = $is_correct_owner;

        $info['question_id'] = 0;
        $info['answer_id'] = 0;
        $info['duel_id'] = $arr['duel_id'];
        $info['result_type'] = RESULT_TYPE_DUELO;
        $info['match_id'] = $match_id;
#$info['created_on'] = new Zend_Db_Expr('NOW()');
        $info['time_in_seconds'] = 0;

        $ml = Matchs::addAnswer($info);
        $info['user_id'] = $arr['guest_user_id'];
        $info['category_id'] = $category_guest;
        $info['is_correct'] = $is_correct_guest;

        $ml = Matchs::addAnswer($info);

#update match
        $infoMatch = array();
        $infoMatch['match_id'] = $match_id;
#$infoMatch['status'] = STATUS_ENDED;
        if ($is_correct_owner) {
            $infoMatch['owner_points'] = " (owner_points + 1) ";
        } else {
            $infoMatch['guest_points'] = " (guest_points + 1) ";
        }
        $updated = Matchs::updatePoints($infoMatch);

        return $updated;
    }

    private function uploadAnswers($respuestas) {
        $affected = 0;
        foreach ($respuestas as $r) {
            $result = Matchs::addAnswer($r);
            if ($result) {
                $affected++;
            }
        }
        return $affected;
    }

    /*
     * Request any question to backend
     */

    public function getAction() {
        try {
            $this->load();
            $log = new FileAdapter("../logs/queries.log");
            $return = Array();
            $user_id = strtoupper($this->dispatcher->getParam('user_id'));
            $powerup = strtoupper($this->dispatcher->getParam('powerup'));
            $country = strtoupper($this->dispatcher->getParam('country_id'));
            $match_id = intval($this->dispatcher->getParam('match_id'));
            $category = intval($this->dispatcher->getParam('category'));
            $lang = strtoupper($this->dispatcher->getParam('lang'));
            $foto = strtoupper($this->dispatcher->getParam('photo'));
            if ($foto != 1) {
                $foto = 0;
            }
            $splash_info = "";
            #$qtd = intval($this->dispatcher->getParam('qtd'));
            #if (!$qtd) {
            $qtd = 1;
            #}

            if (empty($country)) {
                $country = Users::getCountry($user_id);
            }

            if (empty($lang) || $lang == 'EN') {
                $lang = DEFAULT_LANG;
            }

            $continua = 1;
            $duelId = 0;
            $details = $this->calculateDetails($match_id);
            $return['details'] = $details;
            $return['details']['duel_id'] = $duelId;
            if ($match_id) {
                #chequear si hay duelo y enviar las preguntas
                $duelId = Duels::checkDuelToAnswer($match_id, $user_id);
                if ($duelId) {


                    $duelQuestions = $this->getDuelQuestions($duelId, $match_id, $user_id);
                    if ($duelQuestions !== null) {
                        $to_answer = $duelQuestions['to_answer'];
                        if ($duelQuestions['to_answer'] <> 0) {
                            #devolver una pregunta mas del duelo 
                            $return['questions'] = $this->getQuestionNro($to_answer);
                            shuffle($return['questions'][0]['answers']);
                            $continua = 0;
                            $return['details']['question_number_duel'] = $duelQuestions['question_number_duel'] . "/5";
                            $return['details']['duel_id'] = $duelId;
                        }
                    }
                }
            } else {
                #      throw new \Phalcon\Exception('missing match_id', 101);
            }

            if ($continua) {

                if (!$category) {
                    $category = null;
                }

                $user_id = intval($this->dispatcher->getParam('user_id'));
                if ($qtd == 1 && !$category) {

                    $corona = rand(1, PROBABILIDAD_CORONA);
                    if ($corona == 1) {
                        $continua = false;
                        $return['questions'] = Array(array('category_id' => CATEGORIA_CORONA));
                    }
                }
                if ($continua) {
                    # check if first 3 questions
                    $count_answered = Questions::getCountByUser($user_id);
                    $splash_info = $this->getSplash($count_answered);
                    #  if ($splash_info == null){
                    #      $splash_info = 'count'.$count_answered;
                    #  }

                    if ($count_answered < 3) {
                        $level = 0; #easy
                        $data = Questions::get($lang, $user_id, $qtd, $category, $country, $foto, $log, $level);
                    } else {
                        if ($foto == 1) {
                            #Cada 4 preguntas, si la ultima respondida es correcta enviamos pregunta con foto
                            #Si la ultima respondida es correcta le enviamos foto si o si.
                            if (($count_answered % 3) == 0 and (Questions::iscorrectquestion($user_id))) {
                                $foto = 2;
                            }
                            $data = Questions::get($lang, $user_id, $qtd, $category, $country, $foto, $log);
                        } else {
                            $data = Questions::get($lang, $user_id, $qtd, $category, $country, $foto, $log);
                        }
                    }
                    #                    
                    $return['questions'] = Array();
                    $preguntas = 0;

                    $coronas = 0;
                    if ($data) {
                        $preguntas = 0;
                        $coronas = 0;
                        foreach ($data as $answer) {
                            $respuesta = Array();

                            $respuesta['answer'] = $answer['answer'];
                            $respuesta['answer_id'] = $answer['answer_id'];
                            $respuesta['is_correct'] = $answer['is_correct'];
                            $pregunta = $answer;

                            unset($pregunta['answer']);
                            unset($pregunta['is_correct']);
                            unset($pregunta['answer_id']);
                            if (isset($return['questions'][$answer['question_id']])) {
                                $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                                if ($respuesta['is_correct'] == "1") {
                                    $return['questions'][$answer['question_id']]['answer_correct'] = $respuesta['answer_id'];
                                }
                                if (is_null($answer['user_id'])) {
                                    $return['questions'][$answer['question_id']]['author_name'] = "";
                                    $return['questions'][$answer['question_id']]['author_fbtoken'] = "";
                                } else {
                                    $data_author = Users::getProfileData($answer['user_id']);
                                    $parts_data_author = explode(",", $data_author['fb_data']);
                                    $author_name = $parts_data_author [2];
                                    $author_fbtoken = $parts_data_author [0];
                                    $return['questions'][$answer['question_id']]['author_name'] = $author_name;
                                    $return['questions'][$answer['question_id']]['author_fbtoken'] = $author_fbtoken;
                                }
                            } else {
                                $preguntas++;
                                # guardamos la pregunta que enviamos para no repetir
                                $infoAnswer = Array(
                                    'match_id' => 0,
                                    'user_id' => $user_id,
                                    'result_type' => 9,
                                    'question_id' => intval($pregunta['question_id']),
                                    'answer_id' => 0,
                                    'category_id' => 0,
                                    'is_correct' => 0,
                                    'time_in_seconds' => 0,
                                    'duel_id' => 0
                                );

                                @$this->uploadAnswers(array($infoAnswer));
                                $this->log->info("Pregunta para {$user_id}:{$count_answered}:{$pregunta['question_id']}:{$pregunta['question']}");
                                $mediaConveterUrl = $this->di->get('mediaConverterUrl');
                                if ($pregunta['squared_image_id']) {
                                    $pregunta['squared_image'] = $mediaConveterUrl->get() . "media/contdin/fixo/questions/$pregunta[question_id]/squared/##RES##/$pregunta[squared_image_id].##EXT##";
                                }
                                if ($pregunta['rectangular_image_id']) {
                                    $pregunta['rectangular_image'] = $mediaConveterUrl->get() . "media/contdin/fixo/questions/$pregunta[question_id]/rectangular/##RES##/$pregunta[rectangular_image_id].##EXT##";
                                    unset($pregunta['rectangular_image_id']);
                                }
                                unset($pregunta['squared_image_id']);
                                unset($pregunta['rectangular_image_id']);
                                $return['questions'][$answer['question_id']] = $pregunta;
                                $return['questions'][$answer['question_id']]['answers'] = Array();
                                $return['questions'][$answer['question_id']]['answers'][] = $respuesta;

                                if ($respuesta['is_correct'] == "1") {
                                    $return['questions'][$answer['question_id']]['answer_correct'] = $respuesta['answer_id'];
                                }
                            }
                        }
                        $return['questions'] = array_values($return['questions']);

                        if (isset($return['questions'][0]['answers'])) {
                            shuffle($return['questions'][0]['answers']);
                        }
                    }
                }
            }

            /*
             *  HACER EL COBRO DE POWER UP
             */

            $user_info = $this->getUserCoinInfo($user_id);
            $coins = intval($user_info['coins']);
            if (!empty($powerup) && $powerup != 'X') {

                if ($coins) {

                    $coins_nuevas = $this->cobrarPowerUp($powerup, $user_id, $coins);
                    if ($coins_nuevas !== false) {
                        $user_info['coins'] = $coins_nuevas;
                        Users::putCoinHistory($user_id, $powerup, 1, intval($user_info['power_ups']['PRICE_' . $powerup]));
                        ### ADJUNTAR INFO DE COINS ##
                        $user_info = $this->getUserCoinInfo($user_id);
                    } else {
                        $this->log->error("No se cobro power up {$user_id}:{$coins}");
                        #      throw new \Phalcon\Exception("User haven't enough coins", 101);
                    }
                } else {
                    $this->log->error("No tiene coins {$user_id}:{$coins}");
                    #      throw new \Phalcon\Exception("User haven't enough coins", 101);
                }
            }

            $json = array('status' => 'OK', 'data' => $return, 'user_data' => $user_info, 'splash_info' => $splash_info);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function getUserCoinInfo($user_id) {
        try {
            $user_info = Users::getCoinInfo($user_id);
            if ($user_info) {
                $vidas = intval($user_info['whistle_items']);
                $seconds = intval($user_info['whistle_time']);
                $coins = intval($user_info['coins']);

                # Mirar si debe recuperar vidas
                if ($vidas < 3) {
                    if ($seconds >= MINUTOS_VIDA * 60) {

                        if (intval($seconds / intval(MINUTOS_VIDA * 60)) < 4) {
                            $sumar_vidas = intval($seconds / intval(MINUTOS_VIDA * 60));
                        } else {
                            $sumar_vidas = 3;
                        }

                        $vidas = $vidas + $sumar_vidas;
                        if ($vidas > 3) {
                            $vidas = 3;
                        } elseif ($vidas == 0) {
                            $vidas = 0;
                        }
                        if ($vidas != $user_info['whistle_items']) {
                            Users::updateWhistle($user_id, $vidas);
                        }
                        $seconds = intval($seconds % (MINUTOS_VIDA * 60));
                    }
                    $user_info['whistle_time'] = intval((MINUTOS_VIDA * 60) - $seconds);
                    $user_info['whistle_items'] = $vidas;
                } else {
                    $user_info['whistle_time'] = MINUTOS_VIDA * 60;
                }

                $result = Users::getPrices();


                $powerups = array();
                foreach ($result as $t) {
                    $powerups[$t['id']] = (intval($t['cant_coins']) <= $coins) ? 1 : 0;
                    $powerups['PRICE_' . $t['id']] = intval($t['cant_coins']);
                }
                $user_info['power_ups'] = $powerups;
                ksort($user_info['power_ups']);
            }


            return $user_info;
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function cobrarPowerUp($powerup, $user_id, $coins) {

        $price = Questions::getPowerUpPrice($powerup);
        if ($price) {
            if ($coins >= $price) {
                $resultCoins = intval($coins - $price);
                Users::updateCoins($user_id, $resultCoins);
                return $resultCoins;
            } else {
                return false;
            }
        }
        return false;
    }

    public function urlAction() {
        try {

            $this->load();
            #         echo $this->request->getServer('HTTP_USER_AGENT');
            $default = 'http://www.futboleando.com/';
            $code = $this->dispatcher->getParam('code');
            if (strlen($code)) {
                $ua = $this->request->getServer('HTTP_USER_AGENT');
                $urls = Redir::getUrlsByCode($code);
                if (!count($urls)) {
                    Redir::saveHistory($ua, $code, "NOT_FOUND", $default);
                    throw new \Phalcon\Exception("Url not found $code");
                } else {
                    # var_dump($urls);
                    if ($this->is('WindowsMobileOS')) {
                        $ua_result = 'windows';
                        $go = $urls['windows_redir'];
                    } elseif ($this->is('AndroidOS')) {
                        $ua_result = 'android';
                        $go = $urls['android_redir'];
                    } elseif ($this->is('iOS')) {
                        $ua_result = 'ios';
                        $go = $urls['ios_redir'];
                    } else {
                        $ua_result = 'default';
                        $go = $urls['mobile_redir'];
                    }
                    Redir::saveHistory($ua, $code, $ua_result, $go);
                    return $this->response->redirect($go);
                }
            } else {
                throw new \Phalcon\Exception("Code or User missing");
            }
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
