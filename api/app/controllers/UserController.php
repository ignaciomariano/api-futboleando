<?php

define('URL_BILLING_CELCO', 'http://api.futboleando.com/user/startbuying');

class UserController extends ControllerBase {

    /**
     * @var Logger
     */
    private $log;

    public function initialize() {
        $this->log = Logger::getLogger('ControllerBase');
    }

    public function indexAction() {
        
    }

    public function profileAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            #if (empty($user_id)) {
            #    throw new \Phalcon\Exception('user_id missing', 101);
            #}
            $user_data = Users::getProfileData($user_id);
            if (!$user_data) {
                throw new \Phalcon\Exception('User not found', 101);
            }
            $this->setShowname($user_data);
            # Datos personales OK.

            $perf = array(
                'user_data' => $user_data,
                'category' => array(
                    '1' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                    '2' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                    '3' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                    '4' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                    '5' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                    '6' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
            ));


            # Desempeño por categoria:
            $performance = Users::getCategoryPerformance($user_id);


            if ($performance) {
                foreach ($performance as $i) {
                    if (intval($i['is_correct']) == 0) {
                        $perf['category'][$i['category_id']]['incorrect'] = intval($i['cant']);
                    } else {
                        $perf['category'][$i['category_id']]['correct'] = intval($i['cant']);
                    }
                }

                foreach ($perf['category'] as &$cat) {
                    $avg = ($cat['correct'] * 100) / ($cat['correct'] + $cat['incorrect']);
                    $cat['average'] = round($avg, 0);
                }
            }

            # Desempeño de partidas
            $match_perf = array(
                'win' => 0,
                'lost' => 0,
                'average' => 0);
            $abandonadas = $win = $lost = $abandonadas_avg = $average = $total = 0;
            $matchs = Users::getMatchPerformance($user_id);
            foreach ($matchs as $match) {
                if ($user_id == $match['owner_user_id']) {
                    $user_type = 'owner';
                } else {
                    $user_type = 'guest';
                }
                # totales
                if ($match['status'] > 1) {
                    $total++;
                }
                # partidas terminadas
                if ($match['status'] == 2) {

                    if ($match['winner'] == $user_id) {
                        $win++;
                    } elseif ($user_type == 'owner' && $match['owner_points'] >= $match['guest_points']) {
                        $win++;
                    } elseif ($user_type == 'guest' && $match['guest_points'] >= $match['owner_points']) {
                        $win++;
                    } else {
                        $lost++;
                    }
                    #partidas rechazadas de invitacion
                } elseif ($match['status'] == 3) {
                    if ($match['turn'] == $user_id) {
                        $abandonadas++;
                    } else {
                        $win++;
                    }
                } elseif ($match['status'] == 4) {
                    if ($match['turn'] == $user_id) {
                        $abandonadas++;
                    } else {
                        $win++;
                    }
                }
            }
            $match_perf['win'] = $win;
            $match_perf['lost'] = $lost;
            $match_perf['abandoned'] = $abandonadas;
            $match_perf['abandoned_avg'] = ($total > 0) ? round($abandonadas * 100 / $total, 0) : 0;
            $match_perf['total'] = $total;
            if ($total > 0) {
                $avg = $win * 100 / $total;
            }
            $match_perf['average'] = round($avg, 0);

            $perf['matchs'] = $match_perf;
            # duel performance
            $win = $lost = $total = $average = 0;
            $perf['duels'] = array('win' => 0, 'lost' => 0, 'average' => 0);
            $duels = Users::getDuelPerformance($user_id);
            if ($duels) {
                foreach ($duels as $duel) {
                    if (intval($duel['is_correct']) == 0) {
                        $lost = intval($duel['cant']);
                    } else {
                        $win = intval($duel['cant']);
                    }
                }
                $total = $win + $lost;
                if ($total > 0) {
                    $average = $win * 100 / $total;
                }
            }
            $perf['duels']['win'] = $win;
            $perf['duels']['lost'] = $lost;
            $perf['duels']['average'] = round($average, 0);
            $json = array(
                'status' => 'OK',
                'data' => $perf);

            ## end category performance
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    public function startbuyingAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            $country_id = $this->dispatcher->getParam('country');
            $billing_code = $this->dispatcher->getParam('billing_code');
            $coins = intval($this->dispatcher->getParam('coins'));
            $url = "http://mpp.srv.pmovil.net/?action=startBuying&cobranding=##COBRANDING##&country=##COUNTRY##&urlCallback=##URL_CALLBACK##&billingCode=##BILLING_CODE##&celco=##CELCO##&buyingType=ONDEMAND&billingCode=##BILLING_CODE##";
            $url = str_replace("##COUNTRY##", $country_id, $url);
            $url = str_replace("##COBRANDING##", "FUTBOLEANDO_" . $country_id, $url);
            $url = str_replace("##BILLING_CODE##", $billing_code, $url);
            $url = str_replace("##CELCO##", 'VIVO', $url);
            $url = str_replace("##URL_CALLBACK##", urlencode("http://api.futboleando.com/user/checkbuying/user_id/{$user_id}/coins/{$coins}"), $url);
            $this->log->info('BUYINGCELCO-User ' . $user_id . ' startingBuying: ' . $url);
            $results_json = file_get_contents($url);
            $results = json_decode($results_json);
            if ((isset($results->sessionId)) && (!empty($results->sessionId))) {
                $sessionId = $results->sessionId;
                Users::addBillingCelco($sessionId, $user_id, $country_id, $billing_code, $coins);
                $url_go = $results->urlRedirect;
                $this->log->info('BUYINGCELCO-User ' . $user_id . ' redirecting: ' . $url_go);
                return $this->response->redirect($url_go);
            } else {
                $this->log->info('BUYINGCELCO-User ' . $user_id . ' error sessionId: ' . var_export($results, true));
                return $this->response->redirect("HTTP://ERROR");
            }
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    public function checkbuyingAction() {
        $this->load();
        $user_id = $this->dispatcher->getParam('user_id');
        $coins = $this->dispatcher->getParam('coins');
        if (empty($user_id)) {
            return $this->response->redirect("HTTP://ERROR");
        }
        $sessionId = Users::getBillingSession($user_id);

        $url = "http://mpp.srv.pmovil.net/?action=checkBuyingProcess&sessionId=" . $sessionId;
        $this->log->info('BUYINGCELCO-User ' . $user_id . ' CHECKING URL: ' . $url);
        $results_json = file_get_contents($url);
        $results = json_decode($results_json);
        #print_r($results);exit;

        if (isset($results->status)) {
            if ($results->status == 'PURCHASED') {
                #Users::purchasedOKCelco($user_id,$sessionId, $coins);
                Users::putCoins($user_id, $coins);
                $this->log->info('BUYINGCELCO-User ' . $user_id . ' PURCHASED OK: ' . $sessionId);
                $url_go = 'http://PURCHASED';
                return $this->response->redirect($url_go);
            } else {
                $this->log->info('BUYINGCELCO-User ' . $user_id . ' PURCHASED NO SUCCESS: ' . $results->status);
                $url_go = 'http://ERROR';
                return $this->response->redirect($url_go);
            }
        } else {
            $this->log->info('BUYINGCELCO-User ' . $user_id . ' checkBuying: NO RESULT');
        }
    }

    /*
     * Pantalla de compra
     */

    public function getbillingAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($user_id)) {
                #  throw new Exception('UserId missing', 101);
            }

            $ip = $this->request->getServer('HTTP_X_FORWARDED_FOR');

            ## determinate country to use
            $country_user = $this->dispatcher->getParam('country_id');
            #print_r(get_loaded_extensions());exit;
            $country_geo = null;
            if (function_exists("geoip_country_code_by_name")) {
                #$country_geo = @geoip_country_code_by_name($ip);
                #$this->log->log("Using GEOIP_COUNTRY: {$country_geo}");
                $country_geo = $country_user;
            } else {
                $this->log->error("No existe GEOIP");
            }
            # if no detect by IP - detect by REQUEST
            if (empty($country_geo) && !empty($country_user)) {
                $country_geo = $country_user;
                $this->log->info("Using USER_COUNTRY, NO DETECTED IP {$ip}: {$country_geo}");
            }
            $country_id = $country_geo;

            $platform = strtoupper($this->dispatcher->getParam('platform'));
            if (!in_array($platform, array('ANDROID', 'IOS', 'WINDOWS', 'WINDOWS8P'))) {
                $platform = 'ANDROID';
            } else {
                $platform = str_replace('WINDOWS8P', 'WINDOWS', $platform);
            }
            # die($platform);
            $data = Users::getBillingOptions($country_id, $platform);
            $lang = $this->dispatcher->getParam('lang');
            if (empty($lang)) {
                $lang = 'ES';
            }
            foreach ($data as &$compra) {
                if ($compra['type'] == 'celco' && !empty($compra['billing_code'])) {
                    $coins = intval($compra['coins']);
                    $compra['url'] = URL_BILLING_CELCO . "/user_id/{$user_id}/country/{$country_id}/coins/{$coins}/billing_code/" . $compra['celcoBillingCode'];
                } else {
                    $compra['url'] = "";
                }
            }

            $json = array(
                'status' => 'OK',
                'data' => $data,
                'platform' => $platform,
                'country_id' => $country_id,
                'descriptions' => array(
                    'title' => $this->lang('No te quedes sin monedas!', $lang),
                    'subtitle' => $this->lang('Mira las ofertas que tenemos para ti', $lang),
                    'description' => $this->lang('', $lang)
                )
            );
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /*
     * Invite friend to get coins / lifes
     */

    public function invitefriendsAction() {
        try {
            $this->load();
            $list_codes = array('FB_INVITE', 'TW_INVITE', 'WA_INVITE', 'FB_1_LIFE', '1LIFE', 'COINS', 'TW_INVITE_LIFE', 'FB_INVITE_LIFE', 'WA_INVITE_LIFE');
            $code = $this->dispatcher->getParam('code');
            if (!in_array($code, $list_codes)) {
                $code = "COINS";
            }
            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 105);
            }

            $give_coins = 20;
            switch ($code) {
                case 'FB_1_LIFE': case '1LIFE': case 'OneLIFE':
                    $lifes = intval($this->dispatcher->getParam('lifes')) ? $lifes : 1;
                    Users::putLifes($user_id, $lifes);
                    break;
                case 'TW_INVITE_LIFE':
                case 'FB_INVITE_LIFE':
                case 'WA_INVITE_LIFE':
                    $lifes = intval($this->dispatcher->getParam('lifes')) ? $lifes : 1;
                    Users::putLifes($user_id, $lifes);
                    break;
                default:
                    if ($code == 'FB_INVITE') {
                        $give_coins = 10;
                    }
                    $selected = intval($this->dispatcher->getParam('selected'));
                    $friends = abs($selected);
                    if ($friends < 1) {
                        $friends = 1;
                    }
                    Users::putCoins($user_id, $friends * $give_coins);
                    break;
            }
            $userCoinLifes = Users::getCoinsLifes($user_id); #var_dump($userCoinLifes);exit;
            $totalCoins = (is_array($userCoinLifes)) ? $userCoinLifes['coins'] : 0;
            $totalLifes = (is_array($userCoinLifes)) ? $userCoinLifes['whistle_items'] : 0;

            $json = array('status' => 'OK', 'data' => array('code' => $code, 'coins' => $totalCoins, 'lifes' => $totalLifes));
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /*
     * Update Account to FB
     */

    public function fbupdateAction() {
        try {
            $this->load();
            $fb_token = $this->dispatcher->getParam('fb_token');
            $fb_data = $this->dispatcher->getParam('fb_data');

            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 105);
            }
            if (empty($fb_token)) {
                throw new \Phalcon\Exception('Missing token', 105);
            }
            if (empty($fb_data)) {
                throw new \Phalcon\Exception('Missing data', 105);
            }
            if (strlen($fb_token) > strlen($fb_data)) {
                $aux = $fb_token;
                $fb_token = $fb_data;
                $fb_data = $aux;
            }
            $success = Users::upgradeToFBAccount($user_id, $fb_token, $fb_data);
            if ($success) {
                $data = Users::getById($user_id);

                $this->setShowname($data);
            } else {
                throw new \Phalcon\Exception('Upgrade not succeded.', 106);
            }
            $json = array('status' => 'OK', 'data' => array($data), 'result' => $success);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 
     * Get User Info
     */
    public function getuserinfoAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 105);
            }

            $data = Users::getById($user_id);
            $json = array('status' => 'OK', 'data' => array($data));
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    public function pushregisterAction() {
        $this->load();
        $platform = strtoupper($this->dispatcher->getParam('platform'));
        $user_id = $this->dispatcher->getParam('user_id');
        $device_id = $this->dispatcher->getParam('device_id');
        $result = $this->pushregister($user_id, $platform, $device_id);
        $json = array('status' => 'OK', 'data' => $result);
        return $this->response($json);
    }

    /*
     * Update push notification id
     */

    public function pushregister($user_id, $platform, $device_id) {

        if (empty($platform)) {
            $platform = 'ANDROID';
        }

        $parse = false;

        if ($platform == 'ANDROIDP') {
            $did = UserDevice::findFirst("parse_device_id = '$device_id'");
            $native_platform = 'ANDROID';
            $parse = true;
        } elseif ($platform == 'WINDOWS8P') {
            $did = UserDevice::findFirst("parse_device_id = '$device_id'");
            $native_platform = 'WINDOWS8';
            $parse = true;
        } elseif ($platform == 'IOS_PARSE') {
            $did = UserDevice::findFirst("parse_device_id = '$device_id'");
            $native_platform = 'IOS';
            $parse = true;
        } else {
            $native_platform = $platform;
            $did = UserDevice::findFirst("native_device_id = '$device_id'");
        }


        if ($did) {
            $did->user_id = $user_id;
        } else {
            $did = new UserDevice();
            $did->user_id = $user_id;
        }

        $did->platform = $native_platform;

        if ($parse) {
            $did->parse_device_id = $device_id;
            if (empty($did->native_device_id)) {
                $pusher = new Push($this->di['config']->pushs->toArray());
                $did->native_device_id = $pusher->nativeDeviceId($device_id);
            }
        } else {
            $did->native_device_id = $device_id;
        }

        return $did->save();
    }

    /*
     * Login / Register
     */

    public function registerAction() {
        try {
            $this->load();
            $existe = 0;
            $fb_token = urldecode($this->dispatcher->getParam('fb_token'));
            $fb_data = urldecode($this->dispatcher->getParam('fb_data'));
            $device_id = urldecode($this->dispatcher->getParam('device_id'));
            $token = urldecode($this->dispatcher->getParam('token'));
            $store = urldecode($this->dispatcher->getParam('store'));
            $user_info = array();

            ##################
            # country recognize
            $country_geo = null;
            $ip = null;
            $country_request = urldecode($this->dispatcher->getParam('country_id'));
            if (function_exists("geoip_country_code_by_name")) {
                $ip = $this->request->getServer('HTTP_X_FORWARDED_FOR');
                if (empty($ip)) {
                    $ip = $this->request->getServer('REMOTE_ADDR');
                }
                $country_geo = @geoip_country_code_by_name($ip);
                $this->log->info("GEOIP:Using register by GEO_COUNTRY: {$country_geo} {$ip}");
            } else {
                $this->log->info("GEOIP:Not geoip");
            }

            if (strlen($country_geo) == 2) {
                $user_info['country_id'] = $country_geo;
                $this->log->info("GEOIP:Applying register by GEO_COUNTRY: {$country_geo} {$ip}");
            } elseif (strlen($country_request) == 2) {
                $user_info['country_id'] = $country_request;
                $this->log->info("GEOIP:Applying register by request");
            } else {
                $this->log->info("GEOIP:Applying default{$ip}");
                $user_info['country_id'] = 'AR';
            }


            ##############
            # Registro por Facebook
            if (!empty($fb_token)) {
                $this->log->info('LOGIN FB_TOKEN: ' . $fb_token . ' / FB_DATA: ' . $fb_data);

                $registro_anonimo = false;
                #Si existe el usuario en la base
                if (strlen($fb_token) > strlen($fb_data)) {
                    $aux = $fb_token;
                    $fb_token = $fb_data;
                    $fb_data = $aux;
                    $this->log->info('EXHANGE OUT FB_TOKEN: ' . $fb_token . ' / FB_DATA: ' . $fb_data);
                }
                $existe = Users::getByFbId($fb_token);

                # Registro anonimo   
            } else {
                $registro_anonimo = true;
                $user_info['username'] = "null";
                # Registro random (Sin llenar campos)  
            }
            $user_info['lang'] = ($user_info['country_id'] == 'BR') ? 'PT' : 'ES';
            $user_info['fb_token'] = $fb_token;
            $user_info['fb_data'] = $fb_data;
            $user_info['phone_data'] = $this->request->getServer('HTTP_USER_AGENT');
            $platform = $this->dispatcher->getParam('platform');
            $user_info['platform'] = (!empty($platform) ) ? urldecode($platform) : 'ANDROID';
            if ($token == 'ios') {
                $user_info['platform'] = 'IOS';
            }

            $user_info['gender'] = "ACTIVE";
            $user_info['device_id'] = $device_id;

            if (strtoupper($store) == "SAMSUNG") {
                $user_info['coins'] = 200;
            } elseif ($user_info['country_id'] == "ES") {
                $ip = $this->request->getServer('REMOTE_ADDR');
                $res = Origin::getByIp($ip);
                $origin_id = $res["id"];
                if ($origin_id) {
                    $user_info['coins'] = 100;
                    $result = Origin::deleteById($origin_id);
                } else {
                    $user_info['coins'] = DEFAULT_COINS;
                }
            } elseif (strtoupper($store) == "OI") {
                $user_info['coins'] = 70;
            } else {
                $user_info['coins'] = DEFAULT_COINS;
            }
            if ($platform == 'windows8p' && $user_info['country_id'] == 'BR') {
                $user_info['coins'] = DEFAULT_WINDOWS_BR_COINS;
            }
            $user_info['avatar'] = rand(0, 9);

            if (!$registro_anonimo && $existe) {
                #existe le usuario y loguea por FB 

                Users::updateFb($existe['user_id'], $fb_data);


                $data = Users::getById($existe['user_id']);
                if ($data) {
                    $this->setShowname($data);
                }
                $json = array('status' => 'OK', 'data' => array($data));
            } else {
                #No existe el usuario, lo creamos.
                $user_id = Users::add($user_info);
                if ($user_id) {
                    # if anonymous user 
                    if (empty($fb_token)) {
                        Users::updateGuest($user_id);
                    }
                    $data = Users::getById($user_id);
                    if ($data) {
                        $this->setShowname($data);
                    }
                } else {
                    throw new \Phalcon\Exception('Cannot create this user', 105);
                }
                $json = array('status' => 'OK', 'data' => array($data));
            }

            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /*
     * Logout - Clear Push Id
     */

    public function logoutAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');

            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 105);
            }

            $success = Users::logout($user_id);
            $json = array('status' => 'OK', 'data' => $success);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /*
     * Buy Lifes with COINS
     */

    public function buyAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            $code = $this->dispatcher->getParam('code');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 105);
            }
            if (empty($code)) {
                throw new \Phalcon\Exception('Missing code', 105);
            }
            $success = Users::buyLifes($user_id, $code);
            $user_data = Users::getCoinInfo($user_id);
            $json = array('status' => 'OK', 'data' => array('user_data' => $user_data, 'purchase' => $success));
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /*
     * Confirm a purchase
     */

    public function walletAction() {
        try {
            $this->load();

            $user_id = $this->dispatcher->getParam('user_id');
            $code = $this->dispatcher->getParam('code');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id', 105);
            }
            if (empty($code)) {
                throw new \Phalcon\Exception('Missing code', 105);
            }
            $purchase = Users::purchaseOk($user_id, $code);
            $user_data = Users::getCoinInfo($user_id);
            $json = array('status' => 'OK', 'data' => array('user_data' => $user_data, 'purchase' => $purchase));
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /*
     * Show random list from users
     * @params user_id who call it
     */

    public function getfriendsAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('user_id missing', 101);
            }
            $search = $this->dispatcher->getParam('search');
            $token = $this->dispatcher->getParam('token');
            $country_id = $this->dispatcher->getParam('country_id');
            if ($token == 'ios') {
                $data = Users::searchFbUsers($user_id, $country_id, $search);
            } else {
                $data = Users::search($user_id, $country_id, $search);
            }
            foreach ($data as &$user) {
                $this->setShowname($user);
                $this->removeUnusedFields($user);
            }
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
