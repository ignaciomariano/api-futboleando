<?php

#use Models\Users;

class MatchController extends ControllerBase {

    /**
     * @var Logger
     */
    private $log;
    
    public function onConstruct(){
        parent::onConstruct();
        $this->log = Logger::getLogger('MatchController');
    }
    
    
    /**
     * Get Matchs
     *
     * Show the homepage with the list of games.
     *
     * @access public
     */
    public function getAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            $langParam = $this->dispatcher->getParam('lang');
            $lang = (!empty($langParam)) ? $langParam : DEFAULT_LANG;

            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user id', 0);
            }
            $user_info = $this->getUserCoinInfo($user_id);
            #$p = $this->updateWhistle(3, 4);
            if ($user_info !== NULL) {
                $matchs = $this->getMatchs($user_id);
                $matchs_ret = array(
                    'invitations' => array(),
                    'waiting' => array(),
                    'myturn' => array(),
                    'yourturn' => array(),
                    'ended' => array()
                );

                $end_limit = 0;
                $show_ended = (intval($user_info['show_ended']) > 0) ? true : false;
                foreach ($matchs as $match) {
                    $groupOfMatchs = '';
                    $time_desc = "";
                    #### change time_desc
                    $time_arr = explode(":", $match['time_desc']);
                    if (count($time_arr) == 3) {
                        $time_h = intval($time_arr[0]);
                        $time_m = intval($time_arr[1]);
                        if ($time_h < 120) {
                            if ($time_h >= 119) {
                                $time_end = (60 - $time_m) . " " . $this->lang("minutos", $lang);
                            } elseif ($time_h > 24) {
                                 $time_end = "2 " . $this->lang("dias", $lang);
                            } elseif ($time_h > 48) {
                                 $time_end = "3 " . $this->lang("dias", $lang);
                            } elseif ($time_h > 72) {
                                 $time_end = "4 " . $this->lang("dias", $lang);
                            } elseif ($time_h > 96) {
                                $time_end = "5 " . $this->lang("dias", $lang);
                            } else {
                                $time_end = "5 " . $this->lang("dias", $lang);
                            }
                            if ($lang != 'EN') {
                                $time_desc = $this->lang("Restan ", $lang) . $time_end;
                            } else {
                                $time_desc = $time_end . " remaining";
                            }
                        } else {
                            if ($match['status'] != STATUS_ENDED) {
                                # update match set status = 2

                                $this->updateMatchStatus(STATUS_ENDED, $match['match_id']);
                                $match['status'] = STATUS_ENDED;
                                $this->showEndedMatchs($user_id, 1);
                                $show_ended = true;
                            }
                            $time_desc = $this->lang("Finalizada", $lang);
                        }
                    }
                    $match['time_desc'] = $time_desc;

                    switch ($match['status']) {
                        case '0':
                            # $this->logger->debug("INVITATION");
                            if ($match['guest_user_id'] == $user_id && $match['turn'] == $user_id) {
                                $groupOfMatchs = 'invitations';
                            } elseif ($match['owner_user_id'] == $user_id && $match['turn'] != $user_id) {
                                $groupOfMatchs = 'waiting';
                            } elseif ($match['turn'] == $user_id) {
                                $groupOfMatchs = 'myturn';
                            }
                            break;
                        case '1':
                            if ($match['turn'] == $user_id) {
                                $groupOfMatchs = 'myturn';
                            } else {
                                $groupOfMatchs = 'yourturn';
                            }
                            break;
                        default:
                            if ($show_ended) {
                                if ($end_limit++ < 3) {
                                    $groupOfMatchs = 'ended';
                                    #AGREGAR HAS PERDIDO O HAS GANADO
                                    $match['time_desc'] = $this->lang("Finalizada", $lang);
                                }
                            }
                            break;
                    }


                    if ($groupOfMatchs) {
                        if ($match['owner_user_id'] == $user_id) {
                            $rival_user_id = $match['guest_user_id'];
                            $isOwner = true;
                        } else {
                            $rival_user_id = $match['owner_user_id'];
                            $isOwner = false;
                        }

                        $this->setShowname($match);
                        $this->removeUnusedFields($match);

                        $matchs_ret[$groupOfMatchs][] = $match;
                    }
                }
                $json = array('status' => 'OK', 'data' => $matchs_ret, 'user_data' => $user_info);
            } else {
                $this->responseError('Cannot get user_info');
            }
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Add Match
     *
     * Create a game with request params
     *
     * @access public
     */
    public function addAction() {
        try {
            $this->load();
            $owner_user_id = $turn = intval($this->dispatcher->getParam('owner_user_id'));
            $guest_user_id = $this->dispatcher->getParam('guest_user_id');

            if (empty($owner_user_id) || empty($guest_user_id)) {
                throw new \Phalcon\Exception('Missing an userId', 0);
            }
            # Usuario anónimo
            if ($guest_user_id == 'null') {
                $ahora = getdate();
                $bool = $ahora['hours'] % 2;
                $this->log->info('bool:  ' . $bool);
                $match_info = Matchs::getMatchPending($owner_user_id);
                if ($bool == 1) {
                    $guest_user_id = $this->getRandomUser($owner_user_id);
                    $this->log->info('user_id devuelto por /match/add -> getRandomUser  ' . $guest_user_id);
                    $match_id = Matchs::addinvite($owner_user_id, $guest_user_id);
                    $data = Matchs::getToGuest($match_id);
                } else {
                    if (isset($match_info['match_id'])) {
                        Matchs::updateMatchPending($match_info['match_id'], $owner_user_id);
                        $match_id = $match_info['match_id'];
                        $data = Matchs::getToOwner($match_id);
                    } else {
                        $match_id = Matchs::add($owner_user_id, $guest_user_id);
                        $data = Matchs::getToGuest($match_id);
                    }
                }
            } elseif (substr(strtolower($guest_user_id), 0, 3) != "fb_") {# Usuario invitado desde el buscador
                $match_id = Matchs::addinvite($owner_user_id, $guest_user_id);
                $data = Matchs::getToGuest($match_id);
            } else {
                $this->log->info('guest have facebook?' . substr(strtolower($guest_user_id), 0, 3) . "/" . $guest_user_id . "/"
                        . substr(strtolower($guest_user_id), 3, 30));
                if (substr(strtolower($guest_user_id), 0, 3) == "fb_") {
                    $guest_user_id = $this->getUserByFacebook(substr($guest_user_id, 3, 30));
                    $this->log->info('guest have facebook result: ' . $guest_user_id);
                    $match_id = Matchs::addinvite($owner_user_id, $guest_user_id);
                    $data = Matchs::getToGuest($match_id);
                }
            }
            #if ($guest_user_id < 1) {
            #  throw new \Phalcon\Exception('Guest user missing', 103);
            #}

            if ($match_id) {
                if (!empty($data)) {
                    $this->setShowname($data);
                    $this->restLife($owner_user_id);
                    $json = array('status' => 'OK', 'data' => array($data));
                    return $this->response($json);
                } else {
                    return $this->deleteMatch($match_id);
                    throw new \Phalcon\Exception('Match data request failed', 103);
                }
            } else {
                throw new \Phalcon\Exception('Match creation failed', 102);
            }
        } catch (Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Drop ended matchs
     *
     * Remove Ended Matchs from Home Screen
     *
     * @access public
     */

    /**
     * Drop ended matchs
     *
     * Remove Ended Matchs from Home Screen
     *
     * @access public
     */
    public function dropAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id');
            }
            $this->showEndedMatchs($user_id, 0);
            $json = array('status' => 'OK', 'data' => '1');
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    public function getLog($match_id, $owner_id, $guest_id) {
        $data = Matchs::getLog($match_id);
        $detail = Array(
            'status' => 0,
            'duel_id' => 0,
            'ronda' => 1,
            'owner' => array(
                'balls' => 0,
                'id' => intval($owner_id),
                'points' => 0,
                'avatarstr' => '000000',
                'avatars' => array(
                    'avatar1' => 0, 'avatar2' => 0, 'avatar3' => 0, 'avatar4' => 0, 'avatar5' => 0, 'avatar6' => 0
                )
            ),
            'guest' => array(
                'balls' => 0,
                'id' => intval($guest_id),
                'points' => 0,
                'avatarstr' => '000000',
                'avatars' => array(
                    'avatar1' => 0, 'avatar2' => 0, 'avatar3' => 0, 'avatar4' => 0, 'avatar5' => 0, 'avatar6' => 0
                )
            )
        );
        $duelos_owner_win = 0;
        $duelos_owner_lost = 0;
        $duelos_guest_win = 0;
        $duelos_guest_lost = 0;
        $copas_owner = 0;
        $copas_guest = 0;
        $ronda = 1;
        if (!empty($data)) {
            foreach ($data as $row) {
                if (intval($row['duel_id']) == 0) {
                    # si es owner
                    if ($row['user_id'] == $row['owner_user_id']) {
                        #if pregunta normal
                        if ($row['category_id'] == 0) {
                            if (intval($row['is_correct']) == 1) {
                                $detail['owner']['balls'] = ($detail['owner']['balls'] == 3) ? 1 : $detail['owner']['balls'] + 1;
                            }
                        } else {
                            #pregunta por corona
                            if (intval($row['is_correct']) == 1) {
                                if ($detail['owner']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['owner']['points'] = intval($detail['owner']['points']) + 1;
                                }
                                $detail['owner']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['owner']['avatarstr'][$row['category_id'] - 1] = "1";
                            }
                            $detail['owner']['balls'] = 0;
                        }
                    } else {
                        #if pregunta normal

                        if (intval($row['is_correct'] == 0)) {
                            $ronda++;
                        }
                        if ($row['category_id'] == 0) {
                            if (intval($row['is_correct']) == 1) {
                                $detail['guest']['balls'] = ($detail['guest']['balls'] == 3) ? 1 : $detail['guest']['balls'] + 1;
                            } else {
                                #  $ronda++;
                            }
                        } else {
                            #pregunta por corona 
                            if (intval($row['is_correct']) == 1) {
                                if ($detail['guest']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['guest']['points'] = intval($detail['guest']['points']) + 1;
                                }

                                $detail['guest']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['guest']['avatarstr'][$row['category_id'] - 1] = "1";
                            }
                            $detail['guest']['balls'] = 0;
                        }
                    }
                } else {
                    #es duelo
                    #y esta ganando o perdiendo avatar
                    if ($row['result_type'] == "4") {
                        if ($row['user_id'] == $row['owner_user_id']) {
                            $detail['owner']['balls'] = 0;

                            if (intval($row['is_correct']) == 1) {
                                if ($detail['owner']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['owner']['points'] = intval($detail['owner']['points']) + 1;
                                }
                                $duelos_owner_win++;
                                $duelos_guest_lost++;
                                $detail['owner']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['owner']['avatarstr'][$row['category_id'] - 1] = "1";
                            } else {
                                if ($detail['owner']['avatars']['avatar' . $row['category_id']] > 0) {
                                    $detail['owner']['points'] = intval($detail['owner']['points']) - 1;
                                }
                                $detail['owner']['avatars']['avatar' . $row['category_id']] = 0;
                                $detail['owner']['avatarstr'][$row['category_id'] - 1] = "0";
                            }
                        } else {
                            #is guest
                            $detail['guest']['balls'] = 0;
                            $ronda++;
                            if (intval($row['is_correct']) == 1) {

                                if ($detail['guest']['avatars']['avatar' . $row['category_id']] == 0) {
                                    $detail['guest']['points'] = intval($detail['guest']['points']) + 1;
                                }
                                $duelos_guest_win++;
                                $duelos_owner_lost++;
                                $detail['guest']['avatars']['avatar' . $row['category_id']] = 1;
                                $detail['guest']['avatarstr'][$row['category_id'] - 1] = "1";
                            } else {
                                if ($detail['guest']['avatars']['avatar' . $row['category_id']] > 0) {
                                    $detail['guest']['points'] = intval($detail['guest']['points']) - 1;
                                }
                                $detail['guest']['avatars']['avatar' . $row['category_id']] = 0;
                                $detail['guest']['avatarstr'][$row['category_id'] - 1] = "0";
                            }
                        }
                    }
                }
            }
        }

        $detail['ronda'] = $ronda;
        $detail['owner']['duelos'] = array(
            'win' => $duelos_owner_win,
            'lost' => $duelos_owner_lost);
        $detail['guest']['duelos'] = array(
            'win' => $duelos_guest_win,
            'lost' => $duelos_guest_lost);
        $str = $detail['owner']['avatarstr'];
        for ($i = 0; $i < strlen($str); $i++) {
            if (intval(substr($str, $i, 1)) == 1) {
                $copas_owner++;
            }
        }
        $str = $detail['guest']['avatarstr'];
        for ($i = 0; $i < strlen($str); $i++) {
            if (intval(substr($str, $i, 1)) == 1) {
                $copas_guest++;
            }
        }
        $detail['owner']['copas'] = $copas_owner;
        $detail['guest']['copas'] = $copas_guest;

        return $detail;
    }

    /**
     * get matchs by id
     *
     * @access public
     */
    public function summaryAction() {


        try {
            $this->load();
            $match_id = $this->dispatcher->getParam('match_id');
            $user_id = $this->dispatcher->getParam('user_id');
            if (empty($match_id)) {
                throw new \Phalcon\Exception('Missing match_id');
            }
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id');
            }
            $match_info = Matchs::getInfo($match_id);
            if (empty($match_info)) {
                throw new \Phalcon\Exception('Match not found');
            }
            # check if user have permission
            if ($match_info['guest_user_id'] == $user_id) {
                $me = 'guest';
                $enemy = $match_info['owner_user_id'];
                $guest_id = $user_id;
                $owner_id = $enemy;
            } else {
                $me = 'owner';
                $enemy = $match_info['guest_user_id'];
                $guest_id = $enemy;
                $owner_id = $user_id;
            }

            $user = array(
                'owner' => array(
                    'user_id' => $owner_id,
                    'category' => array(
                        '1' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '2' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '3' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '4' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '5' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '6' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                    )),
                'guest' => array(
                    'user_id' => $guest_id,
                    'category' => array(
                        '1' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '2' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '3' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '4' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '5' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
                        '6' => array('incorrect' => 0, 'correct' => 0, 'average' => 0),
            )));

            # get category performance
            $performance = Matchs::getPerformance($owner_id, $match_id);
            $perf = &$user['owner'];
            if ($performance) {
                foreach ($performance as $i) {
                    if (intval($i['is_correct']) == 0) {
                        $perf['category'][$i['category_id']]['incorrect'] = intval($i['cant']);
                    } else {
                        $perf['category'][$i['category_id']]['correct'] = intval($i['cant']);
                    }
                }

                foreach ($perf['category'] as &$cat) {
                    $avg = ($cat['correct'] * 100) / ($cat['correct'] + $cat['incorrect']);
                    $cat['average'] = round($avg, 0);
                }
            }
            # get category performance by enemy
            $performance = Matchs::getPerformance($guest_id, $match_id);
            $perf = &$user['guest'];
            if ($performance) {
                foreach ($performance as $i) {
                    if (intval($i['is_correct']) == 0) {
                        $perf['category'][$i['category_id']]['incorrect'] = intval($i['cant']);
                    } else {
                        $perf['category'][$i['category_id']]['correct'] = intval($i['cant']);
                    }
                }

                foreach ($perf['category'] as &$cat) {
                    $avg = ($cat['correct'] * 100) / ($cat['correct'] + $cat['incorrect']);
                    $cat['average'] = round($avg, 0);
                }
            }
            $details = $this->getLog($match_id, $owner_id, $guest_id);
            if ($details !== null) {
                $details['owner']['stats'] = $user['owner'];
                $details['guest']['stats'] = $user['guest'];
            }
            unset($details['status']);
            unset($details['duel_id']);
            unset($details['ronda']);
            
            $points = Ranking::getPointsMatch( $match_info );
            
            $details['owner']['ranking_points'] = ( ! is_null ( $points['owner'] ) ) ? $points['owner'] : array();
            $details['guest']['ranking_points'] = ( ! is_null ( $points['guest'] ) ) ? $points['guest'] : array();
            
            $json = array('status' => 'OK', 'data' => $details);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * get matchs by id
     *
     * @access public
     */
    public function getbyidAction() {


        try {
            $this->load();
            $match_id = $this->dispatcher->getParam('match_id');
            $user_id = $this->dispatcher->getParam('user_id');

            if (empty($match_id)) {
                throw new \Phalcon\Exception('Missing match_id');
            }
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id');
            }
            $count_answered = Questions::getCountByUser($user_id);
            $splash_info = "";
            if ($count_answered) {
                $splash_info = $this->getSplash($count_answered);
            }
            $data = Matchs::getToUser($match_id, $user_id);
            $json = array('status' => 'OK', 'data' => $data, 'splash_info' => $splash_info);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Reply Game-Invitation
     *
     * Contest an invitation with (accept-reject-lost)
     *
     * @access public
     */
    public function invitationAction() {
        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            $match_id = $this->dispatcher->getParam('match_id');
            $result = $this->dispatcher->getParam('result');
            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id');
            }
            $data = $this->matchReplyInvitation($match_id, $user_id, $result, 'guest');
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Quit Game
     *
     * Give Win to enemy
     *
     * @access public
     */
    public function lostAction() {


        try {
            $this->load();
            $user_id = $this->dispatcher->getParam('user_id');
            $match_id = $this->dispatcher->getParam('match_id');
            $user_type = $this->dispatcher->getParam('user_type');

            if (empty($user_id)) {
                throw new \Phalcon\Exception('Missing user_id');
            }
            if (empty($match_id)) {
                throw new \Phalcon\Exception('Missing match_id');
            }
            $status = STATUS_QUIT;
            $r = Matchs::reply($match_id, $status, $user_type, $user_id);
            if ($r) {
                $this->showEndedByMatch($match_id);
            }
            $json = array('status' => 'OK', 'data' => $r);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Create Duel
     *
     * Create Duel Info for game
     *
     * @access public
     */
    public function createduelAction() {

        try {
            $this->load();
            $lang = $this->dispatcher->getParam('lang');
            $user_id = $this->dispatcher->getParam('user_id');
            $enemy_id = $this->dispatcher->getParam('enemy_id');
            $match_id = $this->dispatcher->getParam('match_id');
            $win_category = $this->dispatcher->getParam('win_category');
            $lost_category = $this->dispatcher->getParam('lost_category');

            if (empty($user_id) || empty($enemy_id)) {
                throw new \Phalcon\Exception('Missing users');
            }
            if (empty($match_id)) {
                throw new \Phalcon\Exception('Missing match_id');
            }
            if (!empty($lost_category) && !empty($win_category)) {
                $duelosPendientes = Matchs::duelsNoAnswered($match_id);
                if (!$duelosPendientes) {
                    $duel = $this->createDuel($match_id, $user_id, $enemy_id, $win_category, $lost_category, $lang);
                    if ($duel) {
                        $json = array('status' => 'OK', 'data' => $duel);
                        return $this->response($json);
                    } else {
                        throw new \Phalcon\Exception('Error in creation duel', 101);
                    }
                } else {
                    throw new \Phalcon\Exception('There is a pending duel', 101);
                }
            } else {
                throw new \Phalcon\Exception('Missing win and lost categories', 101);
            }
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function createDuelQuestions($duelId, $lang, $user_id, $enemy_id) {
        $data = Questions::getEachCategory($lang, $user_id, $enemy_id);
        $this->log->info('getEachCategory:  ' . $data);

        $first_qid = null;
        $return['questions'] = Array();
        if ($data) {
            foreach ($data as $answer) {

                $respuesta = Array();
                $respuesta['answer'] = $answer['answer'];
                $respuesta['answer_id'] = $answer['answer_id'];
                $respuesta['is_correct'] = $answer['is_correct'];
                $pregunta = $answer;
                unset($pregunta['answer']);
                unset($pregunta['is_correct']);
                unset($pregunta['answer_id']);
                if (isset($return['questions'][$answer['question_id']])) {
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                    if ($respuesta['is_correct'] == "1") {
                        $return['questions'][$answer['question_id']]['answer_correct'] = $respuesta['answer_id'];
                    }
                } else {
                    if (!$first_qid) {
                        $first_qid = $answer['question_id'];
                    }
                    $return['questions'][$answer['question_id']] = $pregunta;

                    $return['questions'][$answer['question_id']]['answers'] = Array();
                    $return['questions'][$answer['question_id']]['answers'][] = $respuesta;
                    if ($respuesta['is_correct'] == "1") {
                        $return['questions'][$answer['question_id']]['answer_correct'] = $respuesta['answer_id'];
                    }
                }
            }

            Duels::addQuestions($return['questions'], $duelId);
            $devolver = $return['questions']["$first_qid"];
            $this->log->info('createDuelQuestions Devolver:  ' . $devolver);
            return $devolver;
        }
    }

    private function createDuel($match_id, $user_id, $enemy_id, $win_category, $lost_category, $lang) {

        $info = array(
            'match_id' => $match_id,
            'created_by' => $user_id,
            'win_category' => $win_category,
            'lost_category' => $lost_category
        );

        $duelId = Duels::add($info);
        $this->log->info('Duelo creado:  ' . $duelId);
        if (!empty($duelId)) {
            $questions = $this->createDuelQuestions($duelId, $lang, $user_id, $enemy_id);
            $this->log->info('createDuelQuestions:  ' . $questions);
            if ($questions) {
                return
                        array(
                            'details' => array('duel_id' => $duelId, 'question_number_duel' => "1/5"),
                            'questions' => $questions
                );
            } else {
                return null;
            }
        }
        return null;
    }

    #private methods

    private function showEndedByMatch($match_id) {
        $data = Matchs::getMatch($match_id);
        if ($data) {
            $this->showEnded1($data['owner_user_id'], $data['guest_user_id']);
        } else {
            return 0;
        }
    }

    private function showEnded1($u1, $u2 = null) {
        Users::showEnded(intval($u1), 1);
        if (intval($u2)) {
            Users::showEnded(intval($u2), 1);
        }
    }

    private function matchReplyInvitation($match_id, $user_id, $result) {
        try {

            $status = ($result == 'reject') ? STATUS_INVITATION_REJECTED : STATUS_PLAYING;
            $user_type = 'guest';
            $r = Matchs::reply($match_id, $status, $user_type, $user_id);
            return $r;
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function getUserCoinInfo($user_id) {
        try {
            $user_info = Users::getCoinInfo($user_id);
            if ($user_info) {
                $vidas = intval($user_info['whistle_items']);
                $seconds = intval($user_info['whistle_time']);
                $coins = intval($user_info['coins']);

                # Mirar si debe recuperar vidas
                if ($vidas < 3) {
                    if ($seconds >= MINUTOS_VIDA * 60) {

                        if (intval($seconds / intval(MINUTOS_VIDA * 60)) < 4) {
                            $sumar_vidas = intval($seconds / intval(MINUTOS_VIDA * 60));
                        } else {
                            $sumar_vidas = 3;
                        }

                        $vidas = $vidas + $sumar_vidas;
                        if ($vidas > 3) {
                            $vidas = 3;
                        } elseif ($vidas == 0) {
                            $vidas = 0;
                        }
                        if ($vidas != $user_info['whistle_items']) {
                            $this->updateWhistle($user_id, $vidas);
                        }
                        $seconds = intval($seconds % (MINUTOS_VIDA * 60));
                    }
                    $user_info['whistle_time'] = intval((MINUTOS_VIDA * 60) - $seconds);
                    $user_info['whistle_items'] = $vidas;
                } else {
                    $user_info['whistle_time'] = MINUTOS_VIDA * 60;
                }

                $result = Users::getPrices();


                $powerups = array();
                foreach ($result as $t) {
                    $powerups[$t['id']] = (intval($t['cant_coins']) <= $coins) ? 1 : 0;
                    $powerups['PRICE_' . $t['id']] = intval($t['cant_coins']);
                }
                $user_info['power_ups'] = $powerups;
                ksort($user_info['power_ups']);
            }


            return $user_info;
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    private function showEndedMatchs($user_id, $show_ended) {
        $r = Users::showEnded($user_id, $show_ended);
        return $r;
    }

    private function updateWhistle($user_id, $whistle_items) {
        $r = Users::updateWhistle($user_id, $whistle_items);
        return $r;
    }

    private function getMatchs($user_id) {

        $m = Matchs::getAll($user_id);
        return $m;
    }

    private function updateMatchStatus($status, $match_id) {
        $r = Matchs::updateStatus($status, $match_id);
        return $r;
    }

    private function getRandomUser($user_id) {
        $r = Users::getOneRandom($user_id);
        return $r;
    }

    private function getUserByFacebook($fb_id) {
        $r = Users::getByFacebookId($fb_id);
        return $r;
    }

    private function deleteMatch($match_id) {
        $r = Matchs::deleteMatch($match_id);
        return $r;
    }

    private function restLife($user_id) {
        $r = Users::restLife($user_id);
        return $r;
    }

}
