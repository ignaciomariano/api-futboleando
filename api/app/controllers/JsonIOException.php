<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InvalidParametersException
 *
 * @author ivan
 */
class JsonIOException extends \Exception {

    private $errorMessages;

    public function __construct($message = "", $code = 0, \Exception $previous = NULL, array $errorMessages = array()) {
        parent::__construct($message, $code, $previous);
        $this->errorMessages = $errorMessages;
    }
    
    public function getErrorMessages(){
        return $this->errorMessages;
    }

}
