<?php

class ReportedreasonsController extends ControllerBase {

    public function getallAction() {
        try {
            $this->load();
            $lang = $this->dispatcher->getParam('lang');
            $data = Reportedreasons::getAll($lang);
            $json = array('status' => 'OK', 'data' => $data);
            return $this->response($json);
        } catch (\Phalcon\Exception $e) {
            return $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
