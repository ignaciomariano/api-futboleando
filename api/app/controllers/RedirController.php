<?php

#use Models\Users;

class RedirController extends ControllerBase {

    private function is($type) {
        try {
            $operatingSystems = array(
                'AndroidOS' => 'Android',
                #     'BlackBerryOS'      => 'blackberry|\bBB10\b|rim tablet os',
                #       'PalmOS'            => 'PalmOS|avantgo|blazer|elaine|hiptop|palm|plucker|xiino',
                #  'SymbianOS'         => 'Symbian|SymbOS|Series60|Series40|SYB-[0-9]+|\bS60\b',
                // @reference: http://en.wikipedia.org/wiki/Windows_Mobile
                'WindowsMobileOS' => 'Windows CE.*(Windows|PPC|Smartphone|Mobile|[0-9]{3}x[0-9]{3})|Window Mobile|Windows Phone [0-9.]+|WCE|Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7;',
                // @reference: http://en.wikipedia.org/wiki/Windows_Phone
                // http://wifeng.cn/?r=blog&a=view&id=106
                // http://nicksnettravels.builttoroam.com/post/2011/01/10/Bogus-Windows-Phone-7-User-Agent-String.aspx
                #      'WindowsPhoneOS' => 'Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7',
                'iOS' => '\biPhone.*Mobile|\biPod|\biPad'
                    // http://en.wikipedia.org/wiki/MeeGo
                    // @todo: research MeeGo in UAs
                    #      'MeeGoOS'           => 'MeeGo',
                    // http://en.wikipedia.org/wiki/Maemo
                    // @todo: research Maemo in UAs
                    #   'MaemoOS'           => 'Maemo',
                    #  'JavaOS'            => 'J2ME/|\bMIDP\b|\bCLDC\b', // '|Java/' produces bug #135
                    #   'webOS'             => 'webOS|hpwOS',
                    #   'badaOS'            => '\bBada\b',
                    #   'BREWOS'            => 'BREW',
            );
            $ua = $this->request->getServer('HTTP_USER_AGENT');
            $regex = str_replace('/', '\/', $operatingSystems[$type]);

            return (bool) preg_match('/' . $regex . '/is', $ua);
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

    public function urlAction() {
        try {

            $this->load();
            #         echo $this->request->getServer('HTTP_USER_AGENT');
            $default = 'http://www.futboleando.com/';
            $code = $this->dispatcher->getParam('code');
            if (strlen($code)) {
                $ua = $this->request->getServer('HTTP_USER_AGENT');
                $urls = Redir::getUrlsByCode($code);
                if (!count($urls)) {
                    Redir::saveHistory($ua, $code, "NOT_FOUND", $default);
                    throw new \Phalcon\Exception("Url not found $code");
                } else {
                    # var_dump($urls);
                    if ($this->is('WindowsMobileOS')) {
                        $ua_result = 'windows';
                        $go = $urls['windows_redir'];
                    } elseif ($this->is('AndroidOS')) {
                        $ua_result = 'android';
                        $go = $urls['android_redir'];
                    } elseif ($this->is('iOS')) {
                        $ua_result = 'ios';
                        $go = $urls['ios_redir'];
                    } else {
                        $ua_result = 'default';
                        $go = $urls['mobile_redir'];
                    }
                    Redir::saveHistory($ua, $code, $ua_result, $go);
                    return $this->response->redirect($go);
                }
            } else {
                throw new \Phalcon\Exception("Code or User missing");
            }
        } catch (\Phalcon\Exception $e) {
            $this->responseError($e->getMessage(), $e->getCode());
        }
    }

}
