<?php

use Phalcon\DI,
    Phalcon\DI\FactoryDefault;

ini_set('display_errors',1);
error_reporting(E_ALL);

define('APPLICATION_ENV', 'development');

include __DIR__ . "/../app/config/config.php";

$config->log4php = null;
$_SERVER['REQUEST_URI'] = '';

include __DIR__ . "/../app/config/loader.php";

$logger = Logger::getRootLogger();


$di = new FactoryDefault();
DI::reset();
include __DIR__ . "/../app/config/services.php";

DI::setDefault($di);

