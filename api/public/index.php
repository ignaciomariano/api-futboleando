<?php

$init_time = microtime(true);

error_reporting(E_ALL);
try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../app/config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../app/config/loader.php";


    /**
     * Read services
     */
    include __DIR__ . "/../app/config/services.php";

    /**
     * Handle the request
     */
    $logger = Logger::getLogger('index');


    try {
        $application = new \Phalcon\Mvc\Application($di);
        $application->useImplicitView(false);
        $application->handle()->send();
    } catch (\Phalcon\Mvc\Dispatcher\Exception $e) {


        $uapp = new \Phalcon\Mvc\Micro();

        try {

            $validator = new MicroAppJsonSchemaValidator(new \Phalcon\Cache\Backend\Apc(
                    new \Phalcon\Cache\Frontend\Data(array('lifetime' => $config->application->schemaCacheTimeout))
                    ), $config->application->schemasDir);


            $uapp->before(function() use ($validator, $uapp, $logger) {
                $pattern = $uapp->getRouter()->getMatchedRoute()->getPattern();
                if (preg_match('/^\/docs/', $pattern)) {
                    return true;
                }

                if ($validator->validatePhalconRequest($pattern, $uapp->request, true)) {
                    $logger->info("Valid json request");
                    return true;
                } else {
                    throw new JsonIOException('Invalid Parameters', 400, null, $validator->getLastErrors());
                }
            });

            $uapp->after(function() use ($validator, $uapp, $logger) {
                $method = $uapp->request->getMethod();
                $pattern = $uapp->getRouter()->getMatchedRoute()->getPattern();
                if (preg_match('/^\/docs/', $pattern)) {
                    return true;
                }

                if ($validator->validatePhalconResponse($method, $pattern, $uapp->getReturnedValue())) {
                    $logger->info("Valid json response");
                    return true;
                } else {
                    throw new JsonIOException('Invalid Parameters', 500, null, $validator->getLastErrors());
                }
            });


            $ranking = new Phalcon\Mvc\Micro\Collection();
            $ranking->setHandler(new RankingController());
            $ranking->setPrefix('/ranking');
            $ranking->get('/', 'getRanking');
            $ranking->get('/user', 'getRankingUser');
            $uapp->mount($ranking);


            $uapp->get('/docs', function() use ($uapp, $config) {
                $routes = $uapp->router->getRoutes();
                $view = new \Phalcon\Mvc\View\Simple();
                $view->setViewsDir($config->application->viewsDir);
                array_pop($routes);
                $view->setVar('routes', $routes);
                $view->render('/docs');
                echo $view->getContent();
            });

            $uapp->handle();
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), $e);
            echo $e->getMessage();
        }
    }
} catch (\Exception $e) {
    $logger->error($e->getMessage(), $e);
    echo $e->getMessage();
}

$logger->info('Total time:' . (microtime(true) - $init_time));
